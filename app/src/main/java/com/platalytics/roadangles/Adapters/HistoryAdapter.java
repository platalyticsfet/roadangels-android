package com.platalytics.roadangles.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.platalytics.roadangles.Activities.ServiceDetailActivity;
import com.platalytics.roadangles.Activities.User.ServicesActivity;
import com.platalytics.roadangles.Models.ServicesModel;
import com.platalytics.roadangles.Models.TripModel;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.Utility;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by kamilfaheem on 10/11/2017.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private Activity ctx;
    List<TripModel> tripModels;

    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.cellitem_history, parent, false);
        HistoryAdapter.ViewHolder viewHolder = new HistoryAdapter.ViewHolder(ctx, contactView);
        return viewHolder;
    }

    public HistoryAdapter(Activity context, List<TripModel> tripModels) {
        ctx = context;
        this.tripModels = tripModels;
    }

    @Override
    public void onBindViewHolder(final HistoryAdapter.ViewHolder holder, final int position) {
        try {
            holder.datetime.setText(Utility.getFormatedData_yyyy_MM_dd_HH_mm_ss(Long.parseLong(tripModels.get(position).startTime)));
            if(SharedClass.accountType== Constants.END_USER)
            {
                holder.name.setText(tripModels.get(position).vendorName);
                Picasso.with(ctx).load(tripModels.get(position).vendorProfilePic).error(R.drawable.user_placeholder).placeholder(R.drawable.user_placeholder).resize(100,100).centerCrop().into(holder.profilePic);
            }
            else
            {
                holder.name.setText(tripModels.get(position).userName);
                Picasso.with(ctx).load(tripModels.get(position).userProfilePic).error(R.drawable.user_placeholder).placeholder(R.drawable.user_placeholder).resize(100,100).centerCrop().into(holder.profilePic);
            }
            holder.fromlocation.setText(tripModels.get(position).vendorAddress);
            holder.tolocation.setText(tripModels.get(position).userAddress);
            holder.serviceName.setText(tripModels.get(position).serviceName);
            holder.servicePrice.setText(tripModels.get(position).servicePrice+"$");
            Picasso.with(ctx).load(tripModels.get(position).serviceIcon).resize(100,100).centerCrop().into(holder.servicePic);

            String mapImg = Utility.makeGoogleImageURL(tripModels.get(position));
            Picasso.with(ctx).load((tripModels.get(position).mapImage==null || tripModels.get(position).mapImage.equals("")) ? mapImg : tripModels.get(position).mapImage).into(holder.map);
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ctx.startActivity(new Intent(ctx,ServiceDetailActivity.class).putExtra("trip",tripModels.get(position)));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return tripModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public Context ctx;
        TextView datetime,name,serviceName,servicePrice,fromlocation,tolocation;
        RoundedImageView map;
        CircleImageView profilePic;
        ImageView servicePic;
        View view;
        public ViewHolder(Context context, View itemView) {
            super(itemView);
            ctx = context;
            view = itemView;
            servicePic = (ImageView)itemView.findViewById(R.id.servicePic);
            datetime = (TextView)itemView.findViewById(R.id.datetime);
            name = (TextView)itemView.findViewById(R.id.name);
            fromlocation = (TextView)itemView.findViewById(R.id.fromlocation);
            tolocation = (TextView)itemView.findViewById(R.id.tolocation);
            serviceName = (TextView)itemView.findViewById(R.id.serviceName);
            servicePrice = (TextView)itemView.findViewById(R.id.servicePrice);
            profilePic = (CircleImageView)itemView.findViewById(R.id.profilePic);
            map = (RoundedImageView)itemView.findViewById(R.id.map);
        }

    }
}

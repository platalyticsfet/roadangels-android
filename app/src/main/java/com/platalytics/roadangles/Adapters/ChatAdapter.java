package com.platalytics.roadangles.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.platalytics.roadangles.Models.ChatModel;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.Utility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by kamil on 09/02/2018.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private Activity ctx;
    private final int SENDER_MESSAGE = 1;
    private final int RECIEVER_MESSAGE = 2;
    private ArrayList<ChatModel> chats;

    public ChatAdapter(Activity ctx,ArrayList<ChatModel> chats) {
        this.ctx = ctx;
        this.chats = chats;
    }

    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView;
        if(viewType==SENDER_MESSAGE)
        {
            contactView = inflater.inflate(R.layout.cellitem_chat_sender, parent, false);
        }
        else
        {
            contactView = inflater.inflate(R.layout.cellitem_chat_reciever, parent, false);
        }
        ChatAdapter.ViewHolder viewHolder = new ChatAdapter.ViewHolder(ctx, contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ChatAdapter.ViewHolder holder, final int position) {
        try {
            Picasso.with(ctx).load(chats.get(position).from.pic).error(R.drawable.user_placeholder).placeholder(R.drawable.user_placeholder).resize(100,100).centerCrop().into(holder.profilePic);
            holder.date.setText(Utility.getFormatedData_HH_mm_ss(chats.get(position).date));
            holder.message.setText(chats.get(position).message);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(chats.get(position).from.id.equals(SharedClass.user.getId()))
        {
            return SENDER_MESSAGE;
        }
        else
        {
            return RECIEVER_MESSAGE;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public CircleImageView profilePic;
        public TextView message;
        public TextView date;

        public ViewHolder(Context context, View itemView) {
            super(itemView);
            profilePic = (CircleImageView) itemView.findViewById(R.id.profilePic);
            message = (TextView) itemView.findViewById(R.id.message);
            date = (TextView) itemView.findViewById(R.id.date);
        }
    }
}


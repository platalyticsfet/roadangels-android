package com.platalytics.roadangles.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.ornolfr.ratingview.RatingView;
import com.makeramen.roundedimageview.RoundedImageView;
import com.platalytics.roadangles.Activities.ServiceDetailActivity;
import com.platalytics.roadangles.Models.ReviewRatingModel;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.Utility;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by kamil on 05/03/2018.
 */

public class RatingReviewAdapter extends RecyclerView.Adapter<RatingReviewAdapter.ViewHolder> {
    private Activity ctx;
    List<ReviewRatingModel> review_ratingModels;

    @Override
    public RatingReviewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.cellitem_review, parent, false);
        RatingReviewAdapter.ViewHolder viewHolder = new RatingReviewAdapter.ViewHolder(ctx, contactView);
        return viewHolder;
    }

    public RatingReviewAdapter(Activity context, List<ReviewRatingModel> review_ratingModels) {
        ctx = context;
        this.review_ratingModels = review_ratingModels;
    }

    @Override
    public void onBindViewHolder(final RatingReviewAdapter.ViewHolder holder, final int position) {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        //return review_ratingModels.size();
        return 10;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public Context ctx;
        TextView userName,reviewDate,reviewText;
        RatingView ratingbar;
        CircleImageView profilePic;
        View view;
        public ViewHolder(Context context, View itemView) {
            super(itemView);
            ctx = context;
            view = itemView;
            ratingbar = (RatingView)itemView.findViewById(R.id.ratingbar);
            userName = (TextView)itemView.findViewById(R.id.userName);
            reviewDate = (TextView)itemView.findViewById(R.id.reviewDate);
            reviewText = (TextView)itemView.findViewById(R.id.reviewText);
            profilePic = (CircleImageView)itemView.findViewById(R.id.profilePic);
        }

    }
}

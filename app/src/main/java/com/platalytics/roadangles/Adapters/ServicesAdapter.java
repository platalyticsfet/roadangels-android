package com.platalytics.roadangles.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.platalytics.roadangles.Activities.User.ServicesActivity;
import com.platalytics.roadangles.Models.ServicesModel;
import com.platalytics.roadangles.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kamilfaheem on 10/11/2017.
 */

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {
    private Activity ctx;
    List<ServicesModel> servicesModelList;

    @Override
    public ServicesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.cellitem_services, parent, false);
        ServicesAdapter.ViewHolder viewHolder = new ServicesAdapter.ViewHolder(ctx, contactView);
        return viewHolder;
    }

    public ServicesAdapter(Activity context,List<ServicesModel> servicesModelList) {
        ctx = context;
        this.servicesModelList = servicesModelList;
    }

    @Override
    public void onBindViewHolder(final ServicesAdapter.ViewHolder holder, final int position) {
        try {
            holder.serviceDetail.setText(servicesModelList.get(position).serviceDetail);
            holder.serviceName.setText(servicesModelList.get(position).serviceName);
            Picasso.with(ctx).load(servicesModelList.get(position).serviceImage).into(holder.serviceIcon);
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ServicesActivity)ctx).ShowConfirmDialog(servicesModelList.get(position),servicesModelList.get(position).serviceImage,servicesModelList.get(position).serviceName,servicesModelList.get(position).serviceDetail,servicesModelList.get(position).servicePrice);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return servicesModelList.size()-2;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public Context ctx;
        TextView serviceName,serviceDetail;
        ImageView serviceIcon;
        View view;
        public ViewHolder(Context context, View itemView) {
            super(itemView);
            ctx = context;
            view = itemView;
            serviceIcon = (ImageView)itemView.findViewById(R.id.serviceIcon);
            serviceName = (TextView)itemView.findViewById(R.id.serviceName);
            serviceDetail = (TextView)itemView.findViewById(R.id.serviceDetail);
        }

    }
}

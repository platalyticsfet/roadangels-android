package com.platalytics.roadangles.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.share.Share;
import com.makeramen.roundedimageview.RoundedImageView;
import com.platalytics.roadangles.Activities.ServiceDetailActivity;
import com.platalytics.roadangles.Activities.Vendor.HomeActivity;
import com.platalytics.roadangles.Models.RequestModel;
import com.platalytics.roadangles.Models.TripModel;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.Utility;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by kamilfaheem on 10/11/2017.
 */

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {
    private Activity ctx;
    List<RequestModel> requestsList;

    @Override
    public RequestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.cellitem_request, parent, false);
        RequestAdapter.ViewHolder viewHolder = new RequestAdapter.ViewHolder(ctx, contactView);
        return viewHolder;
    }

    public RequestAdapter(Activity context, List<RequestModel> requestsList) {
        ctx = context;
        this.requestsList = requestsList;
    }

    @Override
    public void onBindViewHolder(final RequestAdapter.ViewHolder holder, final int position) {
        try {
            if(requestsList.get(position).serviceNote!=null && requestsList.get(position).serviceNote.length()>0 )
            {
                holder.noteIcon.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.noteIcon.setVisibility(View.GONE);
            }
            holder.headerView.setBackgroundColor(Color.parseColor(SharedClass.colors.get(requestsList.get(position).colorIndex).hexString));
            holder.serviceName.setText(requestsList.get(position).service.serviceName);
            holder.distanceTime.setText(requestsList.get(position).distanceTime);
            holder.address.setText(requestsList.get(position).user_address);
            Picasso.with(ctx).load(requestsList.get(position).service.serviceImage).into(holder.serviceIcon);
            holder.acceptBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ctx instanceof HomeActivity)
                    {
                        try
                        {
                            ((HomeActivity)ctx).onAcceptRequest(requestsList.get(position).requestId);
                        }
                        catch (Exception exp)
                        {
                            exp.printStackTrace();
                        }
                    }
                }
            });
            holder.noteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showNoteDialog(requestsList.get(position).serviceNote);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showNoteDialog(String note) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Service Description").setMessage(note)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public int getItemCount() {
        return requestsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView serviceName,address,acceptBtn,distanceTime;
        ImageView serviceIcon,noteIcon;
        RelativeLayout headerView;
        View view;
        public ViewHolder(Context context, View itemView) {
            super(itemView);
            view = itemView;
            headerView = (RelativeLayout)itemView.findViewById(R.id.headerView);
            serviceIcon = (ImageView)itemView.findViewById(R.id.serviceIcon);
            noteIcon = (ImageView)itemView.findViewById(R.id.noteIcon);
            distanceTime = (TextView)itemView.findViewById(R.id.distanceTime);
            acceptBtn = (TextView)itemView.findViewById(R.id.acceptBtn);
            address = (TextView)itemView.findViewById(R.id.address);
            serviceName = (TextView)itemView.findViewById(R.id.serviceName);
        }
    }

    public void scaleView(View v, float startScale, float endScale) {
        Animation anim = new ScaleAnimation(
                startScale, endScale, // Start and end values for the X axis scaling
                startScale, endScale, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
        anim.setFillAfter(true); // Needed to keep the result of the animation
        anim.setDuration(1000);
        v.startAnimation(anim);
    }

    public void updateDisTime(String reqId,String disTime)
    {
        for(int i=0;i<requestsList.size();i++)
        {
            if(requestsList.get(i).requestId.equals(reqId))
            {
                requestsList.get(i).distanceTime = disTime;
                break;
            }
        }
        notifyDataSetChanged();
    }
}

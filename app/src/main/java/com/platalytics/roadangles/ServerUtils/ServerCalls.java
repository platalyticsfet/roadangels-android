package com.platalytics.roadangles.ServerUtils;

import android.app.Activity;
import android.app.ProgressDialog;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.platalytics.roadangles.Interfaces.ApiInterface;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.Models.GeneralModel;
import com.platalytics.roadangles.Models.TripModel;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.Utility;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by kamilfaheem on 08/11/2017.
 */

public class ServerCalls {

    public static Activity context;

    public static void initServerCall(Activity myContext)
    {
        context = myContext;
    }

    public static void signIn(final String email, final String password,String loadingMsg, final ResponceCallback responceCallback) {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Utility.ShowProgressDialog(context,loadingMsg);
                Call<GeneralModel> loginCall = api.login(email,password,SharedClass.accountType+"");
                loginCall.enqueue(new Callback<GeneralModel>() {

                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }

    }

    public static void setVendorLocation(String vendorId,String lat,String lng,String address,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> loginCall = api.setVendorLocation(vendorId,lat,lng,address,SharedClass.user.getAuthKey());
                loginCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void createTrip(TripModel tripModel,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                HashMap<String, JSONObject> trip = new HashMap<>();
                JSONObject jsonObject = new JSONObject(new Gson().toJson(tripModel));
                jsonObject.put("auth_key",SharedClass.user.getAuthKey());
                trip.put("trip",jsonObject);
                Call<GeneralModel> loginCall = api.createTrip(trip);
                loginCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void finishTrip(String tripId,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                String endTime = System.currentTimeMillis()+"";
                Call<GeneralModel> loginCall = api.finishTrip(tripId,endTime,SharedClass.user.getId(),SharedClass.user.getAuthKey());
                loginCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void getNearByVendors(String lat,String lng,String range,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> loginCall = api.getNearByVendors(lat,lng,range,SharedClass.user.getId(),SharedClass.user.getAuthKey());
                loginCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void sendRequest(String serviceId,String serviceNote,String lat,String lng,String address,String requestTime,String range,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);

                HashMap<String, JsonObject> request = new HashMap<>();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("service_id", serviceId);
                jsonObject.addProperty("service_note", serviceNote);
                jsonObject.addProperty("user_id", SharedClass.user.getId());
                jsonObject.addProperty("lat", lat);
                jsonObject.addProperty("lng", lng);
                jsonObject.addProperty("address", address);
                jsonObject.addProperty("request_time", requestTime);
                jsonObject.addProperty("range", range);
                jsonObject.addProperty("auth_key", SharedClass.user.getAuthKey());
                request.put("request", jsonObject);

                Call<GeneralModel> requestCall = api.sendRequest(request);
                requestCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void updateRequestStatus(String requestId,String vendorId,String requestStatus,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                String requestTime = System.currentTimeMillis()+"";
                Call<GeneralModel> requestCall = api.updateRequestStatus(requestId,vendorId,requestStatus,SharedClass.user.getAuthKey());
                requestCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void cancelRequest(String requestId,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> requestCall = api.cancelRequest(requestId,SharedClass.user.getId(),SharedClass.user.getAuthKey());
                requestCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void setTripRoute(String tripId,JsonObject routeObj,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                HashMap<String, Object> path = new HashMap<>();
                path.put("path",routeObj);
                path.put("tripId",tripId);
                path.put("id",SharedClass.user.getId());
                path.put("auth_key",SharedClass.user.getAuthKey());
                Call<GeneralModel> loginCall = api.setTripRoute(path);
                loginCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void getAllServices(final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> allServices = api.getAllServices(SharedClass.user.getId(),SharedClass.user.getAuthKey());
                allServices.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void changeVendorStatus(String vendorId,boolean status,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> loginCall = api.changeVendorStatus(vendorId,status,SharedClass.user.getAuthKey());
                loginCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void getPreviousTrips(String type,String id,String authKey,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                Utility.ShowProgressDialog(context,context.getString(R.string.please_wait));
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> loginCall = api.getPreviousTrips(type,id,authKey);
                loginCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void Register(HashMap<String, JsonObject> path,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                Utility.ShowProgressDialog(context,context.getString(R.string.please_wait));
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> loginCall = api.Register(path);
                loginCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().message);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void getVendor(String vendorId, final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> loginCall = api.getVendor(vendorId,SharedClass.user.getId(),SharedClass.user.getAuthKey());
                loginCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void getUser(String userId, final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> loginCall = api.getUser(userId,SharedClass.user.getId(),SharedClass.user.getAuthKey());
                loginCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void logout(String type,String id,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> requestCall = api.logout(id,type,SharedClass.user.getAuthKey());
                requestCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void editProfile(String firstName,String lastname,String phone,String profileLink,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                HashMap<String, JsonObject> profile = new HashMap<>();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("first_name",firstName);
                jsonObject.addProperty("last_name",lastname);
                jsonObject.addProperty("phone",phone);
                jsonObject.addProperty("profile_pic",profileLink);
                jsonObject.addProperty("id",SharedClass.user.getId());
                jsonObject.addProperty("auth_key",SharedClass.user.getAuthKey());
                jsonObject.addProperty("type",SharedClass.accountType+"");
                profile.put("user",jsonObject);
                Call<GeneralModel> loginCall = api.editProfile(profile);
                loginCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void uploadImage(String id, File file,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                RequestBody _id = toRequestBody(id);
                final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("img", file.getName(), requestFile);

                final Call<GeneralModel> call_uploader = api.uploadImage(_id,body);
                call_uploader.enqueue(new Callback<GeneralModel>()
                {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Uploading Error",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void changePassword(String oldpassword,String newpassword,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> passswordCall = api.changePassword(SharedClass.user.getId(),oldpassword,newpassword,SharedClass.accountType+"",SharedClass.user.getAuthKey());
                passswordCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void setRating(HashMap<String, JsonObject> information, final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> passswordCall = api.setRating(information);
                passswordCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void getNearestVendor(String lat,String lng,String range,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> loginCall = api.getNearestVendor(lat,lng,range,SharedClass.user.getId(),SharedClass.user.getAuthKey());
                loginCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void forgetPassword(String email,String type,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                Utility.ShowProgressDialog(context,"Email Varification...");
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> forgetPassCall = api.forgetPassword(email,type);
                forgetPassCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void verifyNumber(String type,String mobile,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                Utility.ShowProgressDialog(context,"Sending SMS....");
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> forgetPassCall = api.verifyPhone(type,mobile);
                forgetPassCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().message);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void verifyCode(String type,String mobile,String code,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                Utility.ShowProgressDialog(context,"Code Varification...");
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> forgetPassCall = api.verifyCode(type,mobile,code);
                forgetPassCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void codeValidation(String email,String type,String code,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                Utility.ShowProgressDialog(context,"Code Validation...");
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> codeValidationCall = api.codeValidation(email,type,code);
                codeValidationCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void updatePassword(String email,String type,String password,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                Utility.ShowProgressDialog(context,"Updating Password..");
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> codeValidationCall = api.updatePassword(email,type,password);
                codeValidationCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static  void updateService(String tripId,String url, final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                HashMap<String,JSONObject> parentObj = new HashMap<>();
                JSONObject childObj = new JSONObject();
                childObj.put("id",tripId);
                childObj.put("long_url",url);
                parentObj.put("url",childObj);
                Utility.ShowProgressDialog(context,context.getString(R.string.please_wait));
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> loginCall = api.updateService(parentObj);
                loginCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void getChatMessages(String tripId,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetCloudServer().create(ApiInterface.class);
                Call<GeneralModel> chatCall = api.getChatMessages(tripId);
                chatCall.enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, retrofit2.Response<GeneralModel> response)
                    {
                        Utility.HideProgressDialog();
                        if(response.isSuccessful())
                        {
                            if (response.body().status==Constants.STATUS_200)
                            {
                                responceCallback.onSuccessResponce(response.body().data);
                            }
                            else if(response.body().status==Constants.STATUS_300)
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.AUTHENTICATION);
                            }
                            else
                            {
                                responceCallback.onFailureResponce(response.body().message,Constants.STRING);
                            }
                        }
                        else
                        {
                            responceCallback.onFailureResponce("Server error found.",Constants.STRING);
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static RequestBody toRequestBody (String value)
    {
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), value);
        return body ;
    }

    /********************************************* GOOGLE API CALLS *********************************************/

    public static  void shortenUrl(String url, final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                Utility.ShowProgressDialog(context,context.getString(R.string.please_wait));
                ApiInterface api = new ServerConfiguration().GetGoogleServer_SU().create(ApiInterface.class);
                JsonObject json = new JsonObject();
                json.addProperty("longUrl",url);
                Call<JsonObject> loginCall = api.shortenUrl(json);
                loginCall.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response)
                    {
                        Utility.HideProgressDialog();
                        if (response.isSuccessful())
                        {
                            responceCallback.onSuccessResponce(response);

                        } else {
                            responceCallback.onFailureResponce(response,Constants.OBJECT);
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Utility.HideProgressDialog();
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Utility.HideProgressDialog();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void getAddressFromGoogle(String lat,String lng,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetGoogleServer().create(ApiInterface.class);
                Call<JsonObject> addressCall = api.getAddress(lat+","+lng,"true");
                addressCall.enqueue(new Callback<JsonObject>() {

                    @Override
                    public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response)
                    {
                        if (response.isSuccessful())
                        {
                            responceCallback.onSuccessResponce(response);

                        } else {
                            responceCallback.onFailureResponce(response,Constants.OBJECT);
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void getPathFromGoogle(String origion,String destination,boolean showAlternativePath,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetGoogleServer().create(ApiInterface.class);
                Call<JsonObject> addressCall = api.getPath(origion,destination,false,"driving",showAlternativePath,context.getString(R.string.google_maps_key));
                addressCall.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response)
                    {
                        if (response.isSuccessful())
                        {
                            responceCallback.onSuccessResponce(response);

                        } else {
                            responceCallback.onFailureResponce(response,Constants.OBJECT);
                        }
                    }
                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

    public static void getTimeDuration(String origion,String destination,final ResponceCallback responceCallback)
    {
        if(SharedClass.isNetConnected)
        {
            try {
                ApiInterface api = new ServerConfiguration().GetGoogleServer().create(ApiInterface.class);
                Call<JsonObject> addressCall = api.getTimeDuration(origion,destination,false,"driving",context.getString(R.string.google_maps_key));
                addressCall.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response)
                    {
                        if (response.isSuccessful())
                        {
                            responceCallback.onSuccessResponce(response);

                        } else {
                            responceCallback.onFailureResponce(response,Constants.OBJECT);
                        }
                    }
                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        responceCallback.onFailureResponce(t, Constants.THROWABLE);
                    }

                });
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            responceCallback.onFailureResponce("Net Connection Not Found", Constants.STRING);
        }
    }

}

package com.platalytics.roadangles.ServerUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by kamilfaheem on 09/11/2017.
 */

public class ServerConfiguration {

    private static final int TIME_OUT = 15;
    public ServerConfiguration() { }

    public static Retrofit GetCloudServer()
    {
        try
        {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            Retrofit.Builder builder = new Retrofit.Builder().baseUrl(Urls.CLOUD_BASE_URL).addConverterFactory(GsonConverterFactory.create(gson));
            OkHttpClient httpClient = new OkHttpClient().newBuilder().readTimeout(TIME_OUT, TimeUnit.SECONDS).connectTimeout(TIME_OUT, TimeUnit.SECONDS).addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build();
            Retrofit retrofit  = builder.client(httpClient).build();
            return retrofit;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    public static Retrofit GetGoogleServer()
    {
        try
        {
            Retrofit.Builder  builder = new Retrofit.Builder().baseUrl(Urls.GOOGLE_BASE_URL).addConverterFactory(GsonConverterFactory.create());
            OkHttpClient httpClient = new OkHttpClient().newBuilder().readTimeout(TIME_OUT, TimeUnit.SECONDS).connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build();
            Retrofit retrofit  = builder.client(httpClient).build();
            return retrofit;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    public static Retrofit GetGoogleServer_SU()
    {
        try
        {
            Retrofit.Builder  builder = new Retrofit.Builder().baseUrl(Urls.SHORTEN_URL_BASE_URL).addConverterFactory(GsonConverterFactory.create());
            OkHttpClient httpClient = new OkHttpClient().newBuilder().readTimeout(TIME_OUT, TimeUnit.SECONDS).connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build();
            Retrofit retrofit  = builder.client(httpClient).build();
            return retrofit;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
}

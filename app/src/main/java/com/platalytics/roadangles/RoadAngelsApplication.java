package com.platalytics.roadangles;

import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.MapsInitializer;
import com.platalytics.roadangles.Bus.GlobalBus;
import com.platalytics.roadangles.SocketsUtil.SocketManager;
import com.platalytics.roadangles.Utilities.SharedClass;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by kamilfaheem on 08/11/2017.
 */

public class RoadAngelsApplication extends MultiDexApplication {
    private SocketManager socketManager;
    @Override
    public void onCreate() {
        super.onCreate();
        SharedClass.isInTestingMode = false;
        MapsInitializer.initialize(this);
        if(!SharedClass.isInTestingMode)
        {
            Fabric.with(this, new Crashlytics());
        }
        socketManager = new SocketManager(RoadAngelsApplication.this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Calibri_Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    public SocketManager getSocketManager() {
        return socketManager;
    }

}

package com.platalytics.roadangles.Services;

/**
 * Created by kamil on 16/01/2018.
 */

import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.platalytics.roadangles.Utilities.SharedPrefrences;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        SharedPrefrences.saveDeviceToken(refreshedToken);
    }
    // [END refresh_token]

}

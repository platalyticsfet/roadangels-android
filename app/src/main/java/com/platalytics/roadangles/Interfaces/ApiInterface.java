package com.platalytics.roadangles.Interfaces;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.platalytics.roadangles.Models.GeneralModel;
import com.platalytics.roadangles.ServerUtils.Urls;
import com.platalytics.roadangles.Utilities.Constants;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ufraj on 11/15/2016.
 */
public interface ApiInterface {

    /* ****************************************** SERVER : CLOUD-1 APIs ****************************************** */

    @GET("/login/{email}/{password}/{type}")
    Call<GeneralModel>login(@Path("email") String email, @Path("password") String password, @Path("type") String type);

    @GET("/getAllVendors")
    Call<GeneralModel>getAllVendors();      // Not using anywhere

    @GET("/setVendorLocation/{vendor_id}/{lat}/{lng}/{address}/{auth_key}")
    Call<GeneralModel>setVendorLocation(@Path("vendor_id") String vendor_id, @Path("lat") String lat,@Path("lng") String lng,@Path("address") String address,@Path("auth_key") String auth_key);

    @GET("/getVendorLocation/{vendor_id}")
    Call<GeneralModel>getVendorLocation(@Path("vendor_id") String vendor_id);      // Not using anywhere

    @Headers({"Accept: application/json","Content-Type: application/json"})
    @POST("/createTrip")
    Call<GeneralModel>createTrip(@Body HashMap<String, JSONObject> path);

    @GET("/finishTrip/{trip_id}/{end_time}/{id}/{auth_key}")
    Call<GeneralModel>finishTrip(@Path("trip_id") String trip_id, @Path("end_time") String end_time,@Path("id") String id,@Path("auth_key") String auth_key);

    @GET("/getNearbyVendors/{lat}/{lng}/{range}/{id}/{auth_key}")
    Call<GeneralModel>getNearByVendors(@Path("lat") String lat, @Path("lng") String lng,@Path("range") String range,@Path("id") String id,@Path("auth_key") String auth_key);


    @GET("/updateRequestStatus/{req_id}/{vendor_id}/{request_status}/{auth_key}")
    Call<GeneralModel>updateRequestStatus(@Path("req_id") String req_id, @Path("vendor_id") String vendor_id,@Path("request_status") String request_status,@Path("auth_key") String auth_key);

    @GET("/cancelRequest/{requestId}/{id}/{auth_key}")
    Call<GeneralModel>cancelRequest(@Path("requestId") String requestId,@Path("id") String id,@Path("auth_key") String auth_key);

    @Headers({"Accept: application/json","Content-Type: application/json"})
    @POST("/setTripRoute")
    Call<GeneralModel>setTripRoute(@Body HashMap<String, Object> path);

    @GET("/getServices/{id}/{auth_key}")
    Call<GeneralModel>getAllServices(@Path("id") String id,@Path("auth_key") String auth_key);

    @GET("/setVendorStatus/{vendorId}/{status}/{auth_key}")
    Call<GeneralModel>changeVendorStatus(@Path("vendorId") String vendorId,@Path("status") boolean status,@Path("auth_key") String auth_key);

    @GET("/getTrips/{type}/{id}/{auth_key}")
    Call<GeneralModel>getPreviousTrips(@Path("type") String type,@Path("id") String id,@Path("auth_key") String auth_key);

    @Headers({"Accept: application/json","Content-Type: application/json"})
    @POST("/register")
    Call<GeneralModel>Register(@Body HashMap<String, JsonObject> path);

    @Headers({"Accept: application/json","Content-Type: application/json"})
    @POST("/requestVendor")
    Call<GeneralModel>sendRequest(@Body HashMap<String, JsonObject> path);

    @GET("/getVendor/{vendor_id}/{id}/{auth_key}")
    Call<GeneralModel>getVendor(@Path("vendor_id") String vendor_id,@Path("id") String id,@Path("auth_key") String auth_key);

    @GET("/getUser/{user_id}/{id}/{auth_key}")
    Call<GeneralModel>getUser(@Path("user_id") String user_id,@Path("id") String id,@Path("auth_key") String auth_key);

    @GET("/logout/{id}/{type}/{auth_key}")
    Call<GeneralModel>logout(@Path("id") String id,@Path("type") String type,@Path("auth_key") String auth_key);

    @Headers({"Accept: application/json","Content-Type: application/json"})
    @POST("/editProfile")
    Call<GeneralModel>editProfile(@Body HashMap<String, JsonObject> profile);

    @Multipart
    @POST("/uploadImage")
    Call<GeneralModel> uploadImage(@Part("id") RequestBody id, @Part MultipartBody.Part image);

    @GET("/changePassword/{id}/{oldPassword}/{newPassword}/{type}/{auth_key}")
    Call<GeneralModel>changePassword(@Path("id") String id,@Path("oldPassword") String oldPassword,@Path("newPassword") String newPassword,@Path("type") String type,@Path("auth_key") String auth_key);

    @Headers({"Accept: application/json","Content-Type: application/json"})
    @POST("/setRating")
    Call<GeneralModel>setRating(@Body HashMap<String, JsonObject> information);

    @GET("/getNearestVendor/{lat}/{lng}/{range}/{id}/{auth_key}")
    Call<GeneralModel>getNearestVendor(@Path("lat") String lat, @Path("lng") String lng,@Path("range") String range,@Path("id") String id,@Path("auth_key") String auth_key);

    @GET("/forgetPassword/{email}/{type}")
    Call<GeneralModel>forgetPassword(@Path("email") String email, @Path("type") String type);

    @GET("/verifyPhone/{type}/{phone}")
    Call<GeneralModel>verifyPhone(@Path("type") String type, @Path("phone") String phone);

    @GET("/verifyCode/{type}/{phone}/{code}")
    Call<GeneralModel>verifyCode(@Path("type") String type, @Path("phone") String phone, @Path("code") String code);

    @GET("/codeValidation/{email}/{type}/{code}")
    Call<GeneralModel>codeValidation(@Path("email") String email, @Path("type") String type,@Path("code") String code);

    @GET("/updatePassword/{email}/{type}/{password}")
    Call<GeneralModel>updatePassword(@Path("email") String email, @Path("type") String type,@Path("password") String password);

    @Headers({"Accept: application/json","Content-Type: application/json"})
    @POST("/updateTrip")
    Call<GeneralModel>updateService(@Body HashMap<String, JSONObject> service);

    @GET("/getChats/{tripId}")
    Call<GeneralModel>getChatMessages(@Path("tripId") String tripId);

    /* ****************************************** SERVER : GOOGLE APIs ****************************************** */

    @GET("/maps/api/geocode/json")
    Call<JsonObject>getAddress(@Query("latlng") String latlng, @Query("sensor") String sensor);

    @GET("/maps/api/directions/json")
    Call<JsonObject>getPath(@Query("origin") String origin, @Query("destination") String destination,@Query("sensor") boolean sensor,@Query("mode") String mode,@Query("alternatives") boolean alternatives,@Query("key") String key);

    @GET("/maps/api/distancematrix/json")
    Call<JsonObject>getTimeDuration(@Query("origins") String origin, @Query("destinations") String destination,@Query("sensor") boolean sensor,@Query("mode") String mode,@Query("key") String key);

    @Headers({"Accept: application/json","Content-Type: application/json"})
    @POST("/urlshortener/v1/url?key=AIzaSyD09rZjHmYusaOGhweGZ7wvkZOXsThsh00")
    Call<JsonObject>shortenUrl(@Body JsonObject service);

    /*
    @FormUrlEncoded
    @POST("register")
    Call<RegisterResponse> registration(@FieldMap HashMap<String, String> authData);

    @FormUrlEncoded
    @POST("login")
    Call<RegisterResponse> login(@FieldMap HashMap<String, String> authData);

    @Multipart
    @POST("imageUpload")
    Call<UploadFileResponce> uploadImage(@Part(Constants.USER_ID) RequestBody userId, @Part(Constants.ACCESS_KEY) RequestBody accessKey, @Part(Constants.API_KEY) RequestBody api_key, @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("updateDeviceToken")
    Call<GenericResponce> updateDeviceToken(@FieldMap HashMap<String, String> authData);

    @GET("getBanners")
    Call<BannerResponce> getBanner();
    */


}


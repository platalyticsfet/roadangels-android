package com.platalytics.roadangles.Interfaces;

/**
 * Created by kamilfaheem on 08/11/2017.
 */

public interface ResponceCallback {
    public void onSuccessResponce(Object obj);
    public void onFailureResponce(Object obj,int type);

}
package com.platalytics.roadangles.Interfaces;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by kamilfaheem on 16/11/2017.
 */

public interface LocationUpdator {
    public void onLocationUpdate(LatLng latlng);
}

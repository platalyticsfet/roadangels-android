package com.platalytics.roadangles.Interfaces;

import com.platalytics.roadangles.Models.ChatModel;

/**
 * Created by kamil on 15/02/2018.
 */

public interface ChatListerner
{
    public void getNewMessage(ChatModel chatModel);
}

package com.platalytics.roadangles.Activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.identity.intents.model.UserAddress;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.CardInfo;
import com.google.android.gms.wallet.CardRequirements;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentMethodTokenizationParameters;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.TransactionInfo;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.Utilities.Utility;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;
import com.stripe.android.view.CardMultilineWidget;

import java.util.Arrays;

public class PaymentMethodActivity extends BaseActivity {

    CardInputWidget card_input_widget;
    private PaymentsClient mPaymentsClient;
    private static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 53;
    boolean testGooglePay = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(PaymentMethodActivity.this);
        card_input_widget = (CardInputWidget)findViewById(R.id.card_multiline_widget);
        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.submitBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Card card = card_input_widget.getCard();
                // Card object will be null if the user inputs invalid data.
                if(card!=null)
                {
                    /*if(card.validateNumber())
                    {
                        Utility.ShowFailurDialog(PaymentMethodActivity.this,"Invalid Card Number",null);
                    }
                    else if(card.validateCVC())
                    {
                        Utility.ShowFailurDialog(PaymentMethodActivity.this,"Invalid CVC Number",null);
                    }
                    else if(card.validateExpMonth())
                    {
                        Utility.ShowFailurDialog(PaymentMethodActivity.this,"Invalid Expiry Month",null);
                    }
                    else if(card.validateExpiryDate())
                    {
                        Utility.ShowFailurDialog(PaymentMethodActivity.this,"Invalid Expiry Date",null);
                    }
                    else if(card.validateCard())
                    {
                        Utility.ShowFailurDialog(PaymentMethodActivity.this,"Invalid Card",null);
                    }
                    else
                    {
                        getCardToken(card);
                    }*/
                    getCardToken(card);
                }
            }
        });
        findViewById(R.id.payGoogleBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payWithGoogle();
            }
        });

        if(testGooglePay)
        {
            findViewById(R.id.stripStandardView).setVisibility(View.GONE);
            findViewById(R.id.googlePayView).setVisibility(View.VISIBLE);
            initGooglePay();
            isReadyToPay();
        }
        else
        {
            findViewById(R.id.googlePayView).setVisibility(View.GONE);
            findViewById(R.id.stripStandardView).setVisibility(View.VISIBLE);
        }
    }

    // Google Pay Functions - START
    private void initGooglePay()
    {
        mPaymentsClient =
                Wallet.getPaymentsClient(this,
                        new Wallet.WalletOptions.Builder()
                                .setEnvironment(WalletConstants.ENVIRONMENT_TEST)
                                .build());

    }

    private void isReadyToPay() {
        IsReadyToPayRequest request = IsReadyToPayRequest.newBuilder()
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
                .build();
        Task<Boolean> task = mPaymentsClient.isReadyToPay(request);
        task.addOnCompleteListener(
                new OnCompleteListener<Boolean>() {
                    public void onComplete(@NonNull Task<Boolean> task) {
                        try {
                            boolean result =
                                    task.getResult(ApiException.class);
                            if(result) {
                                Toast.makeText(PaymentMethodActivity.this, "Ready", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(PaymentMethodActivity.this, "No PWG", Toast.LENGTH_SHORT).show();
                                //hide Google as payment option
                            }
                        } catch (ApiException exception) {
                            Toast.makeText(PaymentMethodActivity.this,
                                    "Exception: " + exception.getLocalizedMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }

    private void payWithGoogle() {
        PaymentDataRequest request = createPaymentDataRequest();
        if (request != null) {
            AutoResolveHelper.resolveTask(
                    mPaymentsClient.loadPaymentData(request),
                    PaymentMethodActivity.this,
                    LOAD_PAYMENT_DATA_REQUEST_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case LOAD_PAYMENT_DATA_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        PaymentData paymentData = PaymentData.getFromIntent(data);
                        // You can get some data on the user's card, such as the brand and last 4 digits
                        CardInfo info = paymentData.getCardInfo();
                        // You can also pull the user address from the PaymentData object.
                        UserAddress address = paymentData.getShippingAddress();
                        // This is the raw string version of your Stripe token.
                        String rawToken = paymentData.getPaymentMethodToken().getToken();

                        // Now that you have a Stripe token object, charge that by using the id
                        Token stripeToken = Token.fromString(rawToken);
                        if (stripeToken != null) {
                            // This chargeToken function is a call to your own server, which should then connect
                            // to Stripe's API to finish the charge.
                            // chargeToken(stripeToken.getId());
                            Toast.makeText(PaymentMethodActivity.this,
                                    "Got token " + stripeToken.toString(), Toast.LENGTH_LONG).show();
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(PaymentMethodActivity.this,
                                "Canceled", Toast.LENGTH_LONG).show();

                        break;
                    case AutoResolveHelper.RESULT_ERROR:
                        Status status = AutoResolveHelper.getStatusFromIntent(data);
                        Toast.makeText(PaymentMethodActivity.this,
                                "Got error " + status.getStatusMessage(), Toast.LENGTH_SHORT).show();

                        // Log the status for debugging
                        // Generally there is no need to show an error to
                        // the user as the Google Payment API will do that
                        break;
                    default:
                        // Do nothing.
                }
                break; // Breaks the case LOAD_PAYMENT_DATA_REQUEST_CODE
            default:
                // Do nothing.
        }
    }

    private PaymentMethodTokenizationParameters createTokenizationParameters() {
        return PaymentMethodTokenizationParameters.newBuilder()
                .setPaymentMethodTokenizationType(WalletConstants.PAYMENT_METHOD_TOKENIZATION_TYPE_PAYMENT_GATEWAY)
                .addParameter("gateway", "stripe")
                .addParameter("stripe:publishableKey", PaymentConfiguration.getInstance().getPublishableKey())
                .addParameter("stripe:version", "5.1.1")
                .build();
    }

    private PaymentDataRequest createPaymentDataRequest() {
        PaymentDataRequest.Builder request =
                PaymentDataRequest.newBuilder()
                        .setTransactionInfo(
                                TransactionInfo.newBuilder()
                                        .setTotalPriceStatus(WalletConstants.TOTAL_PRICE_STATUS_FINAL)
                                        .setTotalPrice("10.00")
                                        .setCurrencyCode("USD")
                                        .build())
                        .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
                        .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
                        .setCardRequirements(
                                CardRequirements.newBuilder()
                                        .addAllowedCardNetworks(Arrays.asList(
                                                WalletConstants.CARD_NETWORK_AMEX,
                                                WalletConstants.CARD_NETWORK_DISCOVER,
                                                WalletConstants.CARD_NETWORK_VISA,
                                                WalletConstants.CARD_NETWORK_MASTERCARD))
                                        .build());

        request.setPaymentMethodTokenizationParameters(createTokenizationParameters());
        return request.build();
    }
    // Google Pay Functions - END

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }

    private void getCardToken(Card card)
    {
        Utility.ShowProgressDialog(PaymentMethodActivity.this,getString(R.string.please_wait));
        Stripe stripe = new Stripe(PaymentMethodActivity.this, getString(R.string.strip_test_apiKey));
        stripe.createToken(
                card,
                new TokenCallback() {
                    public void onSuccess(Token token) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utility.HideProgressDialog();
                            }
                        });
                        Log.d("Token ID",token.getId());
                        Log.d("Token Type ",token.getType());
                        Log.d("Token Created",token.getCreated().toString());
                        Log.d("Token LiveMode",token.getLivemode()+"");
                        Log.d("Token Used",token.getUsed()+"");
                        if(token.getBankAccount()!=null)
                        {
                            Log.d("Token Bank Name",token.getBankAccount().getBankName());
                            Log.d("Token Holder Name",token.getBankAccount().getAccountHolderName());
                            Log.d("Token Holder Type",token.getBankAccount().getAccountHolderType());
                            Log.d("Token Account Number",token.getBankAccount().getAccountNumber());
                            Log.d("Token Country Code",token.getBankAccount().getCountryCode());
                            Log.d("Token Currency",token.getBankAccount().getCurrency());
                            Log.d("Token Fingure Print",token.getBankAccount().getFingerprint());
                            Log.d("Token Last4",token.getBankAccount().getLast4());
                            Log.d("Token Rounting",token.getBankAccount().getRoutingNumber());
                        }
                    }
                    public void onError(Exception error) {
                        // Show localized error message
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utility.HideProgressDialog();
                            }
                        });
                        Utility.ShowGenericToast(PaymentMethodActivity.this,error.getLocalizedMessage());
                    }
                }
        );
    }

}

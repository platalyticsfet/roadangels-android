package com.platalytics.roadangles.Activities.User;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.platalytics.roadangles.Activities.SplashActivity;
import com.platalytics.roadangles.Adapters.ServicesAdapter;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.Models.GoogleDistanceModel;
import com.platalytics.roadangles.Models.NearByVendorModel;
import com.platalytics.roadangles.Models.ServicesModel;
import com.platalytics.roadangles.Models.TripModel;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.Utility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.support.v7.appcompat.R.id.message;

public class ServicesActivity extends BaseActivity implements ResponceCallback {

    RecyclerView serviceRV;
    ServicesAdapter servicesAdapter;
    SwipeRefreshLayout swipeView;

    NearByVendorModel nearestVendor = new NearByVendorModel();
    NearByVendorModel farestVendor = new NearByVendorModel();
    String nearEstimateTime=null;
    String farEstimateTime=null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(ServicesActivity.this);
        nearestVendor = (NearByVendorModel) getIntent().getSerializableExtra("nearestVendor");
        farestVendor = (NearByVendorModel) getIntent().getSerializableExtra("farestVendor");
        serviceRV = findViewById(R.id.serviceRV);
        swipeView = (SwipeRefreshLayout)findViewById(R.id.swipeView);
        swipeView.setColorSchemeResources(R.color.colorPrimary, R.color.black);
        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        String origion = SharedClass.latitude+","+SharedClass.longitude;
        String destination = nearestVendor.latitude+","+nearestVendor.longitude;
        Utility.ShowProgressDialog(ServicesActivity.this,getString(R.string.please_wait));
        getEstimatedTime(origion,destination);
        if(SharedClass.allServices.size()==0)
        {
            swipeView.setRefreshing(true);
            ServerCalls.getAllServices(ServicesActivity.this);
        }
        else
        {
            setAdapter();
        }
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(false);
            }
        });
    }

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }

    private void setAdapter()
    {
        LinearLayoutManager layout = new LinearLayoutManager(ServicesActivity.this, LinearLayoutManager.VERTICAL, false);
        serviceRV.setLayoutManager(layout);
        servicesAdapter = new ServicesAdapter(ServicesActivity.this,SharedClass.allServices);
        serviceRV.setAdapter(servicesAdapter);
    }

    public void ShowConfirmDialog(final ServicesModel servicesModel, String image, String serviceName, String serviceDetail, String estFare)
    {
        if(nearEstimateTime==null || nearEstimateTime.equals(""))
        {
            nearEstimateTime = "N/A";
        }
        if(farEstimateTime==null || farEstimateTime.equals(""))
        {
            farEstimateTime = "N/A";
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(ServicesActivity.this);
        final FrameLayout frameView = new FrameLayout(ServicesActivity.this);
        builder.setView(frameView);
        final AlertDialog alertDialog = builder.create();
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        final View dialoglayout = inflater.inflate(R.layout.dialog_service, frameView);
        Picasso.with(ServicesActivity.this).load(image).into(((ImageView)dialoglayout.findViewById(R.id.serviceIcon)));
        ((TextView)dialoglayout.findViewById(R.id.serviceName)).setText(serviceName);
        ((TextView)dialoglayout.findViewById(R.id.serviceDetail)).setText(serviceDetail);
        ((TextView)dialoglayout.findViewById(R.id.firstestTime)).setText(nearEstimateTime+" - ");
        ((TextView)dialoglayout.findViewById(R.id.secondestTime)).setText(farEstimateTime);
        ((TextView)dialoglayout.findViewById(R.id.estFare)).setText(estFare+"$");

        dialoglayout.findViewById(R.id.confirmBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String comment = ((EditText)dialoglayout.findViewById(R.id.commnentBox)).getText().toString().trim();
                if(comment.isEmpty())
                {
                    comment = "";
                }
                alertDialog.dismiss();
                Intent intent = new Intent();
                intent.putExtra("service",servicesModel);
                intent.putExtra("note",comment);
                setResult(RESULT_OK,intent);
                finish();
            }
        });
        dialoglayout.findViewById(R.id.cancelBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(dialoglayout);
        alertDialog.setCancelable(false);
        //alertDialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
        alertDialog.show();
    }

    private void getEstimatedTime(String origion,String destination)
    {
        ServerCalls.getTimeDuration(origion, destination, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                try
                {
                    JsonObject timeResponse = ((Response<JsonObject>)obj).body();
                    GoogleDistanceModel googleDistanceModel = new Gson().fromJson(timeResponse,GoogleDistanceModel.class);
                    if(nearEstimateTime==null)
                    {
                        nearEstimateTime = googleDistanceModel.rows.get(0).elements.get(0).duration.text;
                        if(nearEstimateTime.contains("hours"))
                        {
                            nearEstimateTime = nearEstimateTime.replace("hours","h");
                            int hIndex = nearEstimateTime.indexOf("h")-1;
                            StringBuilder myName = new StringBuilder(nearEstimateTime);
                            nearEstimateTime = myName.deleteCharAt(hIndex).toString();
                        }
                        if(nearEstimateTime.contains("mins"))
                        {
                            nearEstimateTime = nearEstimateTime.replace("mins","m");
                            int mIndex = nearEstimateTime.indexOf("m")-1;
                            StringBuilder myName = new StringBuilder(nearEstimateTime);
                            nearEstimateTime = myName.deleteCharAt(mIndex).toString();
                        }
                        String origion = SharedClass.latitude+","+SharedClass.longitude;
                        String destination = farestVendor.latitude+","+farestVendor.longitude;
                        getEstimatedTime(origion,destination);
                    }
                    else
                    {
                        Utility.HideProgressDialog();
                        farEstimateTime = googleDistanceModel.rows.get(0).elements.get(0).duration.text;
                        if(farEstimateTime.contains("hours"))
                        {
                            farEstimateTime = farEstimateTime.replace("hours","h");
                            int hIndex = farEstimateTime.indexOf("h")-1;
                            StringBuilder myName = new StringBuilder(farEstimateTime);
                            farEstimateTime = myName.deleteCharAt(hIndex).toString();
                        }
                        if(farEstimateTime.contains("mins"))
                        {
                            farEstimateTime = farEstimateTime.replace("mins","m");
                            int mIndex = farEstimateTime.indexOf("m")-1;
                            StringBuilder myName = new StringBuilder(farEstimateTime);
                            farEstimateTime = myName.deleteCharAt(mIndex).toString();
                        }
                    }
                }
                catch (Exception exp)
                {
                    Utility.HideProgressDialog();
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    Utility.HideProgressDialog();
                    if(type== Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(ServicesActivity.this,message);
                    }
                    else if(type==Constants.THROWABLE)
                    {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                        {
                            Utility.ShowErrorSneakbar(ServicesActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                        }
                        else {
                            Utility.ShowErrorSneakbar(ServicesActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(ServicesActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(ServicesActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp){}
            }
        });
    }

    @Override
    public void onSuccessResponce(Object obj) {
        swipeView.setRefreshing(false);
        JsonObject response = (JsonObject)obj;
        JsonArray serviceResponse = response.getAsJsonArray("array");
        for(int i=0;i<serviceResponse.size();i++)
        {
            ServicesModel servicesModel = new Gson().fromJson(serviceResponse.get(i).getAsJsonObject(),ServicesModel.class);
            SharedClass.allServices.add(servicesModel);
        }
        setAdapter();
    }

    @Override
    public void onFailureResponce(Object obj, int type) {
        try {
            swipeView.setRefreshing(false);
            if (type == Constants.STRING) {
                String message = obj.toString();
                Utility.ShowErrorToast(ServicesActivity.this,message);
            } else if (type == Constants.THROWABLE) {
                Throwable throwable = (Throwable) obj;
                String exceptionClassName = throwable.getClass().getName();
                if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                    Utility.ShowErrorSneakbar(ServicesActivity.this,findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                } else {
                    Utility.ShowErrorSneakbar(ServicesActivity.this,findViewById(android.R.id.content), throwable.getMessage());
                }
            }
            else if(type==Constants.AUTHENTICATION)
            {
                Utility.ShowGenericToast(ServicesActivity.this,Constants.AUTHENTICATION_ERROR);
                SharedPrefrences.clearAll();
                SharedClass.clearAll();
                startActivity(new Intent(ServicesActivity.this, SplashActivity.class));
                finishAffinity();
            }
        }
        catch (Exception exp)
        {
        }

    }
}

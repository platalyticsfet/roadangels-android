package com.platalytics.roadangles.Activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.Models.ColorsModel;
import com.platalytics.roadangles.Models.User;
import com.platalytics.roadangles.R;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;

import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.ServerUtils.Urls;
import com.platalytics.roadangles.Services.LocationService;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.SingleShotLocationProvider;
import com.platalytics.roadangles.Utilities.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Response;

public class SplashActivity extends BaseActivity{

    private int GPS_ENABLE = 1001;
    private FusedLocationProviderClient mFusedLocationClient;


    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(SplashActivity.this);
        //Utility.getKeyHash(SplashActivity.this);
        generateColors();
        if(SharedClass.isInTestingMode)
        {
            //Urls.CLOUD_BASE_URL = "http://192.168.23.222:" + Urls.SOCKET_PORT;      // Local Server - Usman's Machine
            Urls.CLOUD_BASE_URL = "http://192.168.23.41:" + Urls.SOCKET_PORT;       // Local Server - Ali's Machine
            //Urls.CLOUD_BASE_URL = "http://192.168.23.183:" + Urls.SOCKET_PORT;        // Local Server - MAC
        }
        else
        {
            Urls.CLOUD_BASE_URL = "http://122.129.79.68:" + Urls.SOCKET_PORT;        // Cloud 8 Server URL 1 - Public
        }
        if(SharedClass.isNetConnected)
        {
            checkPermissions();
        }
        else
        {
            Utility.ShowFailurDialog(SplashActivity.this,"Internet Connection not found", new ResponceCallback() {
                @Override
                public void onSuccessResponce(Object obj) {
                    finish();
                }

                @Override
                public void onFailureResponce(Object obj, int type) {

                }
            });
        }
    }

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }


    private void generateColors() {
        InputStream inputStream = getResources().openRawResource(R.raw.colors);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int ctr;
        try {
            ctr = inputStream.read();
            while (ctr != -1) {
                byteArrayOutputStream.write(ctr);
                ctr = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.v("Text Data", byteArrayOutputStream.toString());
        try {
            // Parse the data into jsonobject to get original data in form of json.
            JSONObject jObject = new JSONObject(byteArrayOutputStream.toString());
            JSONArray jArray = jObject.getJSONArray("color");
            for(int i=0;i<jArray.length();i++)
            {
                JSONObject tempObj = jArray.getJSONObject(i);
                ColorsModel model = new Gson().fromJson(tempObj.toString(),ColorsModel.class);
                SharedClass.colors.add(model);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void checkPermissions()
    {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted()
            {
                checkGPS();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(SplashActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
        TedPermission.with(SplashActivity.this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION)
                .check();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==GPS_ENABLE)
        {
            checkGPS();
        }
    }

    private void checkGPS() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            //Utility.turnGPSOn(SplashActivity.this);
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Your GPS seems to be disabled, Enable your GPS Service")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),GPS_ENABLE);
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
        } else {
            checkLocation();
        }
    }

    private void checkLocation() {
        Intent intent = new Intent(getApplicationContext(), LocationService.class);
        startService(intent);
        if(SharedClass.latitude==0.0 && SharedClass.longitude==0.0)
        {
            getLocatoin();
        }
        else
        {
            GoToNextScreen();
        }

    }

    private void GoToNextScreen() {
        if(!SharedPrefrences.getEmailAddress().equals(""))
        {
            AuthenticateUser(SharedPrefrences.getEmailAddress(),SharedPrefrences.getPassword());
        }
        else
        {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this,UserSelectionActivity.class));
                    finish();
                }
            },2000);
        }

    }

    private void AuthenticateUser(String email,String password) {
        SharedClass.accountType = SharedPrefrences.getLoginAs();
        ServerCalls.signIn(email, password,getString(R.string.authenticate_user), new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                JsonObject loginResponse = (JsonObject)obj;
                if(loginResponse.size()>0)
                {
                    SharedClass.user = new Gson().fromJson(loginResponse, User.class);
                    if(SharedClass.accountType== Constants.END_USER)
                    {
                        startActivity(new Intent(SplashActivity.this,com.platalytics.roadangles.Activities.User.HomeActivity.class));
                    }
                    else
                    {
                        startActivity(new Intent(SplashActivity.this,com.platalytics.roadangles.Activities.Vendor.HomeActivity.class));
                    }
                    finishAffinity();
                }
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    startActivity(new Intent(SplashActivity.this,UserSelectionActivity.class));
                    finish();
                }
                catch (Exception exp){}
            }
        });
    }

    private void getLocatoin() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                SharedClass.latitude = location.getLatitude();
                                SharedClass.longitude = location.getLongitude();
                                SharedPrefrences.saveUserLocation(SharedClass.latitude+"",SharedClass.longitude+"");
                            }
                            else
                            {
                                SingleShotLocationProvider.requestSingleUpdate(SplashActivity.this,
                                        new SingleShotLocationProvider.LocationCallback() {
                                            @Override public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                                                if(location!=null)
                                                {
                                                    SharedClass.latitude = location.latitude;
                                                    SharedClass.longitude = location.longitude;
                                                    SharedPrefrences.saveUserLocation(SharedClass.latitude+"",SharedClass.longitude+"");

                                                }
                                            }
                                        });
                            }
                            GoToNextScreen();
                        }
                    });
        }
    }

}

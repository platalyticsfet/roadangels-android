package com.platalytics.roadangles.Activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.Models.User;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.ServerUtils.Urls;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    EditText emailEt;
    TextInputEditText passwordEt;
    private CallbackManager callbackManager;
    List<String> permissionNeeds;
    LoginButton login_button;
    private String email;
    private String password;
    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 1001;
    private String TAG = "LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(LoginActivity.this);
        callbackManager = CallbackManager.Factory.create();

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        login_button=(LoginButton) findViewById(R.id.login_button);
        login_button.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));

        emailEt = (EditText)findViewById(R.id.emailEt);
        passwordEt = (TextInputEditText)findViewById(R.id.passwordEt);
        emailEt.setText(SharedPrefrences.getEmailAddress());
        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.forgotPassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,ForgetPasswordActivity.class));
            }
        });
        findViewById(R.id.fbBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login_button.performClick();
            }
        });
        findViewById(R.id.googleBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
        findViewById(R.id.loginBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = emailEt.getText().toString().trim();
                password = passwordEt.getText().toString().trim();
                login();
            }
        });
        FbCallBacks();
    }

    private void login() {
        if(email.isEmpty())
        {
            Utility.ShowErrorSneakbar(LoginActivity.this,findViewById(android.R.id.content),"Enter Email please");
        }
        else if(password.isEmpty())
        {
            Utility.ShowErrorSneakbar(LoginActivity.this,findViewById(android.R.id.content),"Enter Password please");
        }
        else
        {
            ServerCalls.signIn(email, password,getString(R.string.please_wait), new ResponceCallback() {
                @Override
                public void onSuccessResponce(Object obj) {
                    JsonObject loginResponse = (JsonObject)obj;
                    if(loginResponse.size()>0)
                    {
                        SharedClass.user = new Gson().fromJson(loginResponse, User.class);
                        if(SharedClass.accountType== Constants.END_USER)
                        {
                            if(SharedClass.user.isVerified())
                            {
                                SharedPrefrences.loginAs();
                                SharedPrefrences.saveEmailAddress(email);
                                SharedPrefrences.savePassword(password);
                                startActivity(new Intent(LoginActivity.this,com.platalytics.roadangles.Activities.User.HomeActivity.class));
                            }
                            else
                            {
                                startActivity(new Intent(LoginActivity.this, PhoneVerificationActivity.class));
                            }
                        }
                        else
                        {
                            if(SharedClass.user.isVerified())
                            {
                                SharedPrefrences.loginAs();
                                SharedPrefrences.saveEmailAddress(email);
                                SharedPrefrences.savePassword(password);
                                startActivity(new Intent(LoginActivity.this,com.platalytics.roadangles.Activities.Vendor.HomeActivity.class));
                            }
                            else
                            {
                                startActivity(new Intent(LoginActivity.this, PhoneVerificationActivity.class));
                            }
                        }
                        finishAffinity();
                    }
                }

                @Override
                public void onFailureResponce(Object obj, int type) {
                    try
                    {
                        if(type==Constants.STRING)
                        {
                            String message = obj.toString();
                            Utility.ShowErrorToast(LoginActivity.this,message);
                        }
                        else if(type==Constants.THROWABLE)
                        {
                            Throwable throwable = (Throwable) obj;
                            String exceptionClassName = throwable.getClass().getName();
                            if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                            {
                                Utility.ShowErrorSneakbar(LoginActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                            }
                            else {
                                Utility.ShowErrorSneakbar(LoginActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                            }
                        }
                        else if(type==Constants.AUTHENTICATION)
                        {
                            Utility.ShowGenericToast(LoginActivity.this,Constants.AUTHENTICATION_ERROR);
                            SharedPrefrences.clearAll();
                            SharedClass.clearAll();
                            startActivity(new Intent(LoginActivity.this, SplashActivity.class));
                            finishAffinity();
                        }
                    }
                    catch (Exception exp){}
                }
            });
        }
    }

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }

    private void FbCallBacks()
    {
        permissionNeeds = Arrays.asList("user_photos", "email", "user_birthday", "public_profile", "AccessToken");
        login_button.registerCallback(callbackManager,
            new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult)
                {
                    Log.e("Status","onSuccess");
                    System.out.println("onSuccess");
                    String accessToken = loginResult.getAccessToken().getToken();
                    Log.i("accessToken", accessToken);
                    GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback()
                            {
                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response)
                                {
                                    Log.i("LoginActivity", response.toString());
                                    try {
                                        password = object.getString("id");
                                        try {
                                            URL profile_pic = new URL("http://graph.facebook.com/" + password + "/picture?type=large");
                                            Log.i("profile_pic", profile_pic + "");
                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                        }
                                        if(object.has("email"))
                                        {
                                            email = object.getString("email");
                                        }
                                        else
                                        {
                                            email = "";
                                        }
                                        if(email.equals(""))
                                        {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    emailEt.setText(email);
                                                    passwordEt.setText(password);
                                                }
                                            });
                                        }
                                        else
                                        {
                                            login();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,name,email,gender, birthday");
                    request.setParameters(parameters);
                    request.executeAsync();
                }

                @Override
                public void onCancel() {
                    Log.e("Status","onCancel");
                }

                @Override
                public void onError(FacebookException exception) {
                    Log.e("Status","onError");
                    Log.v("LoginActivity", exception.getMessage().toString());
                    Utility.ShowFailurDialog(LoginActivity.this,exception.getMessage().toString(),null);
                }
            });
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
        callbackManager.onActivityResult(requestCode, responseCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            email = account.getEmail();
            password = account.getId();
            login();
        } catch (ApiException e) {
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }
}

package com.platalytics.roadangles.Activities.Vendor;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.Image;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.github.ornolfr.ratingview.RatingView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.makeramen.roundedimageview.RoundedImageView;
import com.platalytics.roadangles.Activities.ChatActivity;
import com.platalytics.roadangles.Activities.HistoryActivity;
import com.platalytics.roadangles.Activities.ProfileActivity;
import com.platalytics.roadangles.Activities.RatingReviewsActivity;
import com.platalytics.roadangles.Activities.ReviewServiceActivity;
import com.platalytics.roadangles.Activities.SettingActivity;
import com.platalytics.roadangles.Activities.SplashActivity;
import com.platalytics.roadangles.Activities.User.ServicesActivity;
import com.platalytics.roadangles.Adapters.RequestAdapter;
import com.platalytics.roadangles.Adapters.ServicesAdapter;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Bus.Events;
import com.platalytics.roadangles.Bus.GlobalBus;
import com.platalytics.roadangles.Interfaces.LocationUpdator;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.Interfaces.SocketMessageListerner;
import com.platalytics.roadangles.Models.ChatModel;
import com.platalytics.roadangles.Models.GoogleAddressModel;
import com.platalytics.roadangles.Models.GoogleDistanceModel;
import com.platalytics.roadangles.Models.NearByVendorModel;
import com.platalytics.roadangles.Models.RequestModel;
import com.platalytics.roadangles.Models.ServicesModel;
import com.platalytics.roadangles.Models.TripModel;
import com.platalytics.roadangles.Models.User;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.Reciever.NetworkChangeReceiver;
import com.platalytics.roadangles.RoadAngelsApplication;
import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.Services.LocationService;
import com.platalytics.roadangles.SocketsUtil.SocketManager;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.Utility;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class HomeActivity extends BaseActivity implements
        View.OnClickListener,
        OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMarkerClickListener,
        SocketMessageListerner,
        LocationUpdator {

    private Toolbar toolbar;
    private GoogleMap mMap;
    private CircleImageView profilePic;
    private TextView usernameTv;
    private Switch status_switch;
    private View mapView;
    private int ZOOM_POWER = 10;
    //ImageView mapmarker;
    Marker userMarker = null;
    Marker vendorMarker = null;
    ProgressBar loadingbar;
    Button finishServiceBtn;
    Animation slidedownHideAnim,slideupShowAnim,slidedownShowAnim,slideupHideAnim;
    LocationService mLocationService;
    boolean mServiceBound = false;
    private List<LatLng> mPathPolygonPoints = new ArrayList<>();
    LatLng previousLatLng = null;
    LatLng currentLatLng = null;
    CardView userDetailView;
    TripModel tripModel = null;
    User user = null;
    private SlidingRootNav slidingRootNav;
    private NotificationManager notificationmanager = null;
    LinearLayout nav_history,nav_earning,nav_rating,nav_setting,nav_help,nav_logout;
    HashMap<String,Marker> requestMarkersMap = null;
    List<RequestModel> requestLists = null;
    String selectedRequestId = null;
    RecyclerView requestRV;
    RequestAdapter requestAdapter = null;
    int maxColourIndex;
    static TextView tv_check_connection;
    boolean tripStarted = false;
    FloatingActionMenu fabMenu;

    //Testing Variables
    Handler handler = null;
    Runnable runnable = null;
    int index=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_vendor);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(HomeActivity.this);
        initSocketManager();
        setToolbarAndNavDrawer(savedInstanceState);
        initObject();
        inflateGoogleMap();
    }

    public void dialog(boolean value){

        if(value){
            tv_check_connection.setText("Connection Established");
            tv_check_connection.setBackgroundColor(getResources().getColor(R.color.success_green));
            tv_check_connection.setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                if(SharedClass.user.getTripId()!=null && SharedClass.user.getTripId().length()>0 && !tripStarted)
                {
                    tripStarted = true;
                    index = SharedPrefrences.getIndex();
                    getTrip();
                }
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            tv_check_connection.setVisibility(View.VISIBLE);
            tv_check_connection.setText("Internet Connection Lost");
            tv_check_connection.setBackgroundColor(getResources().getColor(R.color.error_red));
            tv_check_connection.setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }

    public void passNewChatMessage(ChatModel chat) {
        try
        {
            Events.GetNewMessage getNewMessage =
                    new Events.GetNewMessage(chat);
            GlobalBus.getBus().post(getNewMessage);
        }
        catch (Exception exp)
        {
            exp.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (SharedClass.user != null) {
            if (SharedClass.user.getProfilePicture() != null) {
                Picasso.with(HomeActivity.this).load(SharedClass.user.getProfilePicture()).error(R.drawable.user_placeholder).placeholder(R.drawable.user_placeholder).into(profilePic);
            }
            usernameTv.setText(SharedClass.user.getFirstName() + " " + SharedClass.user.getLastName());
            ((TextView)findViewById(R.id.ratingTv)).setText(SharedClass.user.getRating());
            if(SharedClass.user.getRating()==null || SharedClass.user.getRating().equals(""))
            {
                findViewById(R.id.ratingstar).setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(handler!=null && SharedClass.isInTestingMode)
        {
            handler.removeCallbacks(runnable);
        }
        GlobalBus.getBus().unregister(this);
        socketManager.socketDisconnect();
        unbindLocationService();
        if(requestLists!=null && requestLists.size()>0)
        {
            StopGettingRequest();
        }
    }

    private void initSocketManager() {
        RoadAngelsApplication app = (RoadAngelsApplication) getApplication();
        socketManager = app.getSocketManager();
        socketManager.connectSocketManager();
        socketManager.initSocketListener(HomeActivity.this);
    }

    private void setToolbarAndNavDrawer(Bundle savedInstanceState) {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        slidingRootNav = new SlidingRootNavBuilder(this)
                .withToolbarMenuToggle(toolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.vendor_menu_left_drawer)
                .inject();
    }

    private void inflateGoogleMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);
    }

    private void initObject() {
        tv_check_connection = (TextView)findViewById(R.id.tv_check_connection);
        requestRV = (RecyclerView)findViewById(R.id.requestRV);
        status_switch = (Switch)findViewById(R.id.status_switch);
        userDetailView = (CardView) findViewById(R.id.userDetailView);
        finishServiceBtn = (Button) findViewById(R.id.finishServiceBtn);
        loadingbar = (ProgressBar) findViewById(R.id.loadingbar);

        nav_history = (LinearLayout) findViewById(R.id.nav_history);
        nav_earning = (LinearLayout) findViewById(R.id.nav_earning);
        nav_rating = (LinearLayout) findViewById(R.id.nav_rating);
        nav_setting = (LinearLayout) findViewById(R.id.nav_setting);
        nav_help = (LinearLayout) findViewById(R.id.nav_help);
        nav_logout = (LinearLayout) findViewById(R.id.nav_logout);
        fabMenu = (FloatingActionMenu)findViewById(R.id.fabMenu);
        fabMenu.setVisibility(View.GONE);
        findViewById(R.id.menu_navigation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabMenu.close(true);
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+tripModel.userLatLng);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
        findViewById(R.id.menu_call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabMenu.close(true);
                String phone = "+92344023457";
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null)));
            }
        });
        findViewById(R.id.menu_sms).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabMenu.close(true);
                /*if(vendor.getPhoneNumber()!=null && !vendor.getPhoneNumber().equals(""))
                {
                    Uri smsUri = Uri.parse("tel:123456");
                    intent.putExtra("sms_body", "Enter Some Text");
                    startActivity(new Intent(Intent.ACTION_VIEW, smsUri).setType("vnd.android-dir/mms-sms"));
                }*/
            }
        });
        findViewById(R.id.menu_chat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabMenu.close(true);
                Intent intent = new Intent(HomeActivity.this, ChatActivity.class);
                if(user!=null)
                {
                    intent.putExtra("otherUserId",user.getId());
                    intent.putExtra("otherUserName",user.getFirstName() +" "+user.getLastName());
                }
                intent.putExtra("tripId",SharedClass.user.getTripId());
                startActivity(intent);
            }
        });

        nav_history.setOnClickListener(this);
        nav_earning.setOnClickListener(this);
        nav_rating.setOnClickListener(this);
        nav_setting.setOnClickListener(this);
        nav_help.setOnClickListener(this);
        nav_logout.setOnClickListener(this);

        profilePic = (CircleImageView) findViewById(R.id.profilePic);
        usernameTv = (TextView) findViewById(R.id.usernameTv);
        profilePic.setOnClickListener(this);
        slidedownHideAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down_hide);
        slideupShowAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up_visible);
        slidedownShowAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down_visible);
        slideupHideAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up_hide);
        finishServiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishService();
            }
        });
        if(SharedClass.user!=null && SharedClass.user.getStatus())
        {
            status_switch.setChecked(true);
            //toolbar.setBackgroundColor(getResources().getColor(R.color.success_green));
        }
        else
        {
            status_switch.setChecked(false);
            //toolbar.setBackgroundColor(getResources().getColor(R.color.error_red));
        }
        status_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                ServerCalls.changeVendorStatus(SharedClass.user.getId(), isChecked, new ResponceCallback() {
                    @Override
                    public void onSuccessResponce(Object obj) {
                        if(SharedClass.user.getStatus())
                        {
                            SharedClass.user.setStatus(false);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(requestLists!=null && requestLists.size()>0)
                                    {
                                        StopGettingRequest();
                                    }
                                }
                            });
                            //toolbar.setBackgroundColor(getResources().getColor(R.color.error_red));
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),"You are Offline now");
                        }
                        else
                        {
                            SharedClass.user.setStatus(true);
                            Utility.ShowSuccessSneakbar(HomeActivity.this,findViewById(android.R.id.content),"You are Online now");
                            //toolbar.setBackgroundColor(getResources().getColor(R.color.success_green));
                        }
                    }

                    @Override
                    public void onFailureResponce(Object obj, int type) {
                        try
                        {
                            if (type == Constants.STRING) {
                                String message = obj.toString();
                                Utility.ShowErrorToast(HomeActivity.this,message);
                            } else if (type == Constants.THROWABLE) {
                                Throwable throwable = (Throwable) obj;
                                String exceptionClassName = throwable.getClass().getName();
                                if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                                    Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                                } else {
                                    Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content), throwable.getMessage());
                                }
                            }
                            else if(type==Constants.AUTHENTICATION)
                            {
                                Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                                SharedPrefrences.clearAll();
                                SharedClass.clearAll();
                                startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                                finishAffinity();
                            }
                        } catch (Exception exp) {
                        }
                    }
                });
            }
        });
    }

    @Subscribe
    public void sendMessage(Events.SendNewMessage newMessage)
    {
        socketManager.SendChatMessage(newMessage.senderId,newMessage.recieverId,newMessage.authKey,newMessage.message,newMessage.date,newMessage.tripId);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.profilePic:
                slidingRootNav.closeMenu();
                startActivity(new Intent(HomeActivity.this, ProfileActivity.class));
                break;
            case R.id.nav_history:
                startActivity(new Intent(HomeActivity.this, HistoryActivity.class));
                slidingRootNav.closeMenu();
                break;
            case R.id.nav_earning:
                slidingRootNav.closeMenu();
                break;
            case R.id.nav_rating:
                startActivity(new Intent(HomeActivity.this, RatingReviewsActivity.class));
                slidingRootNav.closeMenu();
                break;
            case R.id.nav_setting:
                slidingRootNav.closeMenu();
                startActivity(new Intent(HomeActivity.this, SettingActivity.class));
                break;
            case R.id.nav_help:
                slidingRootNav.closeMenu();
                break;
            case R.id.nav_logout:
                slidingRootNav.closeMenu();
                Utility.ShowYesNoDialog(HomeActivity.this,"Logout", "You really want to logout ?", "Yes", "No", new ResponceCallback() {
                    @Override
                    public void onSuccessResponce(Object obj) {
                        logout();
                    }

                    @Override
                    public void onFailureResponce(Object obj, int type) {

                    }
                });
                break;
        }
    }

    private void hideRequestListView() {
        requestRV.setVisibility(View.GONE);
        requestAdapter = null;
    }

    private void finishService() {
        unbindLocationService();
        mMap.clear();
        status_switch.setEnabled(true);
        index = 0;
        user = null;
        requestLists = null;
        requestMarkersMap = null;
        tripStarted = false;
        hideRequestListView();
        userMarker = null;
        vendorMarker = null;
        mPathPolygonPoints.clear();
        previousLatLng = null;
        currentLatLng = null;
        //mapmarker.setVisibility(View.VISIBLE);
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }
        finishServiceBtn.startAnimation(slidedownHideAnim);
        slidedownHideAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                finishServiceBtn.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        finishTrip();
    }

    @Override
    public void onBackPressed() {
        if (slidingRootNav.isMenuOpened()) {
            slidingRootNav.closeMenu();
        } /*else if(SharedClass.user.getTripId()!=null && SharedClass.user.getTripId().length()>0) {
            Utility.ShowErrorSneakbar(findViewById(android.R.id.content),"Can not go back during trip");
        }*/ else {
            super.onBackPressed();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            SharedClass.latitude = (SharedPrefrences.getUserLatitude().equals("") ? 0.0 : Double.parseDouble(SharedPrefrences.getUserLatitude()));
            SharedClass.longitude = (SharedPrefrences.getUserLongitude().equals("") ? 0.0 : Double.parseDouble(SharedPrefrences.getUserLongitude()));
            updateVendorLocation();
            mMap = googleMap;
            mMap.setMyLocationEnabled(true);
            mMap.setOnMyLocationButtonClickListener(this);
            mMap.setOnMarkerClickListener(this);
            MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style);
            //mMap.setMapStyle(style);
            //googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            LatLng myLoc = new LatLng(SharedClass.latitude, SharedClass.longitude);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLoc, ZOOM_POWER+3));

            if (mapView != null && mapView.findViewById(Integer.parseInt("1")) != null) {
                View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                layoutParams.setMargins(0, 0, Utility.dpToPx(HomeActivity.this,10), 30);
            }
            onMyLocationButtonClick();
            if(SharedClass.user.getTripId()!=null && SharedClass.user.getTripId().length()>0)
            {
                index = SharedPrefrences.getIndex();
                getTrip();
            }
        } else {
            return;
        }
    }

    private void updateVendorLocation() {
        getAdress(SharedClass.latitude, SharedClass.longitude );
    }

    private void getAdress(final double lat, final double lng)
    {
        ServerCalls.getAddressFromGoogle(lat + "", lng + "", new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                try
                {
                    JsonObject addressResponse = ((Response<JsonObject>)obj).body();
                    GoogleAddressModel addressObj = new Gson().fromJson(addressResponse, GoogleAddressModel.class);
                    if(addressObj.status.equals("OK"))
                    {
                        SharedClass.address = addressObj.results.get(0).formattedAddress;
                        ServerCalls.setVendorLocation(SharedClass.user.getId(), SharedClass.latitude + "", SharedClass.longitude + "",SharedClass.address, new ResponceCallback() {
                            @Override
                            public void onSuccessResponce(Object obj) {
                                JsonObject vendorLoc = (JsonObject) obj;
                                String str = "asd";
                            }

                            @Override
                            public void onFailureResponce(Object obj, int type) {
                                try {
                                    if (type == Constants.STRING) {
                                        String message = obj.toString();
                                        Utility.ShowErrorToast(HomeActivity.this,message);
                                    } else if (type == Constants.THROWABLE) {
                                        Throwable throwable = (Throwable) obj;
                                        String exceptionClassName = throwable.getClass().getName();
                                        if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                                        } else {
                                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content), throwable.getMessage());
                                        }
                                    }
                                    else if(type==Constants.AUTHENTICATION)
                                    {
                                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                                        SharedPrefrences.clearAll();
                                        SharedClass.clearAll();
                                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                                        finishAffinity();
                                    }
                                } catch (Exception exp) {
                                }
                            }
                        });
                    }
                    else
                    {
                        getAdress(lat,lng);
                    }
                }
                catch (Exception exp)
                {
                    exp.printStackTrace();
                }

            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                //getAdress(lat,lng);
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if(requestMarkersMap!=null)
        {
            Set<String> keys = requestMarkersMap.keySet();
            for (String key : keys) {
                Marker tempMarker = requestMarkersMap.get(key);
                if (marker.equals(tempMarker)) {
                    for(int i=0;i<requestLists.size();i++)
                    {
                        if(requestLists.get(i).requestId.equals(key))
                        {
                            requestRV.smoothScrollToPosition(i);
                            //View view = requestRV.findViewHolderForAdapterPosition(i).itemView;
                            //requestAdapter.scaleView(view,1.1f,1);

                            break;
                        }
                    }
                    break;
                }
            }
        }
        return false;
    }

    @Override
    public boolean onMyLocationButtonClick() {
        if(mMap!=null) {
            Location location = mMap.getMyLocation();
            if (location!=null && SharedClass.latitude == 0.0) {
                SharedClass.latitude = location.getLatitude();
                SharedClass.longitude = location.getLongitude();
                updateVendorLocation();
                SharedPrefrences.saveUserLocation(SharedClass.latitude + "", SharedClass.longitude + "");
            }
        }
        return false;
    }

    @Override
    public void onGetMessageAck(String event,int objectType, Object... obj)
    {
        if(event.equals(Constants.SERVICE_REQUEST) && SharedClass.user.getStatus() && SharedClass.user.getService_status().equals("Free"))
        {
            onGetServiceRequest(obj);
        }
        else if(event.equals(Constants.CANCEL_REQUEST) && SharedClass.user.getStatus() && SharedClass.user.getService_status().equals("Free"))
        {
            cancelRequest((String) obj[0]);
        }
        else if(event.equals(Constants.ACCEPT_REQUEST) && SharedClass.user.getStatus() && SharedClass.user.getService_status().equals("Free"))
        {
            JSONArray array = (JSONArray) obj[0];
            if (array.length() > 0) {
                try
                {
                    String requestID = array.getString(0);
                    cancelRequest(requestID);
                }
                catch (Exception exp)
                {
                    exp.printStackTrace();
                }
            }
        }
        else if(event.equals(Constants.RECIEVE_NEW_MESSAGE))
        {
            try
            {
                final JSONObject jsonObject = (JSONObject)obj[0];
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ChatModel chat = new Gson().fromJson(jsonObject.toString(),ChatModel.class);
                        passNewChatMessage(chat);
                    }
                });

            }
            catch (Exception exp)
            {
                Log.d("ChatParsing",exp.getMessage());
            }
        }
    }

    private void onGetServiceRequest(Object... obj) {
        RequestModel requestModel = null;
        try
        {
            JSONObject object = (JSONObject) obj[0];
            JSONObject userJsonObj = object.getJSONObject("user");
            JSONObject serviceJsonObj = object.getJSONObject("services");
            JSONObject requestJsonObj = object.getJSONObject("serviceRequests");
            JSONArray vendorIdsJsonObj = object.getJSONArray("vendorIds");

            User user = new User(userJsonObj);

            requestModel = new RequestModel(requestJsonObj);
            user.setLatitude(Double.parseDouble(requestModel.user_lat));
            user.setLongitude(Double.parseDouble(requestModel.user_lng));
            user.setAddress(requestModel.user_address);
            requestModel.user = user;
            for(int i=0;i<vendorIdsJsonObj.length();i++)
            {

                String id = vendorIdsJsonObj.getString(i);
                requestModel.vendorsId.add(id);
            }

            ServicesModel servicesModel = new ServicesModel(serviceJsonObj);
            requestModel.service = servicesModel;

        }catch (Exception e) {
            e.printStackTrace();
        }
        if (isRangedVendorForRequest(requestModel))
        {
            if(requestLists==null)
            {
                maxColourIndex = 0;
                requestLists = new ArrayList<>();
            }
            requestModel.colorIndex = maxColourIndex;
            requestModel.distanceTime = "Fetching...";
            requestLists.add(requestModel);
            setRequestInList();
            final RequestModel finalRequestModel = requestModel;
            runOnUiThread(new Runnable() {
                @Override
                public void run()
                {
                    MediaPlayer mPlayer = MediaPlayer.create(HomeActivity.this, R.raw.request_tune);
                    mPlayer.start();
                    //final String destination = requestModel.user_lat + "," + requestModel.user_lng;
                    //String mapUrl = Utility.makeGoogleImageURL(destination);
                    showRequestMarker(finalRequestModel.requestId, finalRequestModel.service.serviceImage,new LatLng(Double.parseDouble(finalRequestModel.user_lat),Double.parseDouble(finalRequestModel.user_lng)));
                    //showRequestDialog(requestModel.requestId, servicesModel.serviceName, servicesModel.servicePrice+"$", user.getAddress(), mapUrl);
                    getEstimatedTime(finalRequestModel.requestId,finalRequestModel.user_lat, finalRequestModel.user_lng);
                }
            });
        }
    }

    private void cancelRequest(final String requestId)
    {
        if(SharedClass.user.getStatus()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (requestMarkersMap != null)
                    {
                        if (requestMarkersMap.containsKey(requestId)) {
                            requestMarkersMap.get(requestId).remove();
                            requestMarkersMap.remove(requestId);
                            for(int i=0;i<requestLists.size();i++)
                            {
                                if(requestLists.get(i).requestId.equals(requestId))
                                {
                                    requestLists.remove(i);
                                    requestAdapter.notifyDataSetChanged();
                                    break;
                                }
                            }
                        }
                        if(selectedRequestId!=null && selectedRequestId.equals(requestId))
                        {
                            selectedRequestId = null;
                            hideRequestListView();
                        }
                        if (requestMarkersMap.size() == 0)
                        {
                            requestRV.setVisibility(View.GONE);
                            requestAdapter = null;
                            requestMarkersMap = null;
                            requestLists = null;
                            hideRequestListView();
                        }
                    }
                }
            });
        }
    }

    private void updateRequestStatus(final String requestId, String vendorId, String requestStatus) {
        ServerCalls.updateRequestStatus(requestId, vendorId, requestStatus, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj)
            {
                JsonObject requestStatus = (JsonObject)obj;
                JsonObject userJsonObj = requestStatus.getAsJsonObject("user");
                JsonObject requestJsonObj = requestStatus.getAsJsonObject("serviceRequests");
                JsonObject serviceJsonObj = requestStatus.getAsJsonObject("service");
                user = null;
                user = new User();
                user.setId(userJsonObj.get("_id").getAsString());
                user.setFirstName(userJsonObj.get("first_name").getAsString());
                user.setLastName(userJsonObj.get("last_name").getAsString());
                user.setEmail(userJsonObj.get("email").getAsString());
                user.setPhoneNumber(userJsonObj.get("phone").getAsString());
                user.setProfilePicture(userJsonObj.get("profile_pic").getAsString());
                user.setRating(userJsonObj.get("rating").getAsString());

                final RequestModel tempModel = new RequestModel();
                tempModel.vendorsId.add(requestJsonObj.get("vendor_id").getAsString());
                tempModel.request_status = requestJsonObj.get("request_status").getAsString();
                tempModel.request_time = requestJsonObj.get("request_time").getAsString();
                tempModel.user_lng = requestJsonObj.get("user_lng").getAsString();
                tempModel.user_lat = requestJsonObj.get("user_lat").getAsString();
                tempModel.user_address = requestJsonObj.get("address").getAsString();
                tempModel.user_id = requestJsonObj.get("user_id").getAsString();
                tempModel.requestId = requestJsonObj.get("_id").getAsString();
                user.setLatitude(Double.parseDouble(tempModel.user_lat));
                user.setLongitude(Double.parseDouble(tempModel.user_lng));
                user.setAddress(tempModel.user_address);
                tempModel.user = user;

                ServicesModel servicesModel = new ServicesModel();
                servicesModel.serviceId = serviceJsonObj.get("_id").getAsString();
                servicesModel.serviceName = serviceJsonObj.get("service_name").getAsString();
                servicesModel.serviceDetail = serviceJsonObj.get("service_detail").getAsString();
                servicesModel.serviceImage = serviceJsonObj.get("service_image").getAsString();
                servicesModel.servicePrice = serviceJsonObj.get("service_price").getAsString();
                tempModel.service = servicesModel;
                SharedClass.user.setService_status("Busy");
                if(requestLists==null)
                {
                    requestLists = new ArrayList<>();
                }
                else {
                    requestLists.clear();
                }
                tempModel.colorIndex = 0;
                tempModel.distanceTime = "Fetching...";
                requestLists.add(tempModel);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        status_switch.setEnabled(false);
                        removeAllRequestMarker();
                        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        LatLng userLatLng = new LatLng(Double.parseDouble(tempModel.user_lat), Double.parseDouble(tempModel.user_lng));
                        LatLng destinationLatLng = new LatLng(SharedClass.latitude, SharedClass.longitude);
                        drawPathBetweenUserVendor(null,userLatLng, destinationLatLng, false);
                    }
                });
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try {
                    if (type == Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    } else if (type == Constants.THROWABLE) {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                        } else {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content), throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                } catch (Exception exp) {
                }
            }
        });
    }

    private void StopGettingRequest()
    {
        // Call ! When vendor go Offline line while getting requests or App goes to destory
        mMap.clear();
        if(requestMarkersMap!=null) {
            requestMarkersMap.clear();
            requestMarkersMap = null;
        }
        if(requestLists!=null)
        {
            requestLists.clear();
            requestLists = null;
        }
        hideRequestListView();
    }

    private void removeAllRequestMarker() {
        mMap.clear();
        if(requestMarkersMap!=null) {
            requestMarkersMap.clear();
            requestMarkersMap = null;
        }
        hideRequestListView();
    }

    private void getEstimatedTime(final String reqId, String desLet, String desLng) {
        String origion = SharedClass.latitude + "," + SharedClass.longitude;
        final String destination = desLet + "," + desLng;
        ServerCalls.getTimeDuration(origion, destination, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                try {
                    JsonObject timeResponse = ((Response<JsonObject>) obj).body();
                    GoogleDistanceModel googleDistanceModel = new Gson().fromJson(timeResponse, GoogleDistanceModel.class);
                    String estimateTime = googleDistanceModel.rows.get(0).elements.get(0).duration.text;
                    String estimateDistance = googleDistanceModel.rows.get(0).elements.get(0).distance.text;
                    final String disTime = estimateDistance+" / "+estimateTime;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            requestAdapter.updateDisTime(reqId,disTime);
                        }
                    });
                } catch (Exception exp) {
                    Utility.HideProgressDialog();
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try {
                    Utility.HideProgressDialog();
                    if (type == Constants.STRING) {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    } else if (type == Constants.THROWABLE) {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                        } else {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content), throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                } catch (Exception exp) {
                }
            }
        });
    }

    private void drawPathBetweenUserVendor(String tripPath, final LatLng userLac, LatLng vendorLoc, boolean showAlternativePath) {
        userDetailView.setVisibility(View.VISIBLE);
        userDetailView.startAnimation(slidedownShowAnim);
        String firstName = user.getFirstName() == null ? "N/A" : user.getFirstName();
        String lastName = user.getLastName() == null ? "N/A" : user.getLastName();
        ((TextView)findViewById(R.id.userName)).setText(firstName+" "+lastName);
        if(requestLists!=null && requestLists.size()>0)
        {
            ((TextView)findViewById(R.id.userDate)).setText(Utility.getFormatedData_yyyy_MM_dd_HH_mm_ss(Long.parseLong(requestLists.get(0).request_time)));
        }
        else
        {
            ((TextView)findViewById(R.id.userDate)).setText(Utility.getFormatedData_yyyy_MM_dd_HH_mm_ss(Long.parseLong(tripModel.startTime)));
        }
        ((TextView)findViewById(R.id.userAddress)).setText(user.getAddress());
        Picasso.with(HomeActivity.this).load(user.getProfilePicture()).placeholder(R.drawable.user_placeholder).into(  ((CircleImageView)findViewById(R.id.userProfilePic))   );
        if(user.getRating()!=null && !user.getRating().equals(""))
        {
            ((TextView)findViewById(R.id.rating)).setText(user.getRating());
            ((RatingView)findViewById(R.id.ratingbar)).setRating(Float.parseFloat(user.getRating()));
        }
        else
        {
            ((TextView)findViewById(R.id.rating)).setText("N/A");
            ((RatingView)findViewById(R.id.ratingbar)).setRating(0.0f);
        }
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        }
        if (userMarker == null) {
            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.user_icon_view, null);
            userMarker = mMap.addMarker(new MarkerOptions()
                    .position(userLac)
                    .icon(BitmapDescriptorFactory.fromBitmap(Utility.createDrawableFromView(HomeActivity.this,marker))));
        } else {
            userMarker.setPosition(userLac);
        }
        if (vendorMarker == null) {
            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.vendor_icon_view, null);
            vendorMarker = mMap.addMarker(new MarkerOptions()
                    .position(vendorLoc)
                    .icon(BitmapDescriptorFactory.fromBitmap(Utility.createDrawableFromView(HomeActivity.this,marker))));
        } else {
            vendorMarker.setPosition(vendorLoc);
        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(vendorLoc, ZOOM_POWER+3));
        //mapmarker.setVisibility(View.GONE);
        String origion = vendorLoc.latitude + "," + vendorLoc.longitude;
        String destination = userLac.latitude + "," + userLac.longitude;
        mPathPolygonPoints.clear();
        if(tripPath!=null)
        {
            mPathPolygonPoints.addAll(Utility.decodePoly(tripPath));
            mMap.addPolyline(new PolylineOptions()
                    .addAll(mPathPolygonPoints)
                    .width(7)
                    .color(getResources().getColor(R.color.success_green))
                    .geodesic(true));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //setCameraAutoZooming();
                    showNotificationOnStartTrip(user.getFirstName()+" "+user.getLastName(),Utility.getFormatedData_yyyy_MM_dd_HH_mm_ss(System.currentTimeMillis()),user.getProfilePicture());
                    bindLocationService();
                    finishServiceBtn.setVisibility(View.VISIBLE);
                    finishServiceBtn.startAnimation(slideupShowAnim);
                    slideupShowAnim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            fabMenu.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    Utility.ShowGenericToast(HomeActivity.this,"Trip Resumed");
                    if(SharedClass.isInTestingMode)
                    {
                        launchSandboxTesting();
                    }
                }
            });
        }
        else
        {
            loadingbar.setVisibility(View.VISIBLE);
            ServerCalls.getPathFromGoogle(origion, destination, showAlternativePath, new ResponceCallback() {
                @Override
                public void onSuccessResponce(Object obj) {
                    try {
                        final JsonObject pathResponse = ((Response<JsonObject>) obj).body();
                        loadingbar.setVisibility(View.GONE);
                        //Tranform the string into a json object
                        JsonArray routeArray = pathResponse.getAsJsonArray("routes");
                        JsonObject routes = routeArray.get(0).getAsJsonObject();
                        JsonObject overviewPolylines = routes.getAsJsonObject("overview_polyline");
                        final String encodedString = overviewPolylines.get("points").getAsString();
                        mPathPolygonPoints.addAll(Utility.decodePoly(encodedString));
                        mMap.addPolyline(new PolylineOptions()
                                .addAll(mPathPolygonPoints)
                                .width(7)
                                .color(getResources().getColor(R.color.success_green))
                                .geodesic(true));
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //setCameraAutoZooming();
                                showNotificationOnStartTrip(user.getFirstName()+" "+user.getLastName(),Utility.getFormatedData_yyyy_MM_dd_HH_mm_ss(System.currentTimeMillis()),user.getProfilePicture());
                                creatTrip(pathResponse,encodedString);
                                bindLocationService();
                                finishServiceBtn.setVisibility(View.VISIBLE);
                                finishServiceBtn.startAnimation(slideupShowAnim);
                                slideupShowAnim.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        fabMenu.setVisibility(View.VISIBLE);
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });
                            }
                        });
                    }
                    catch (JsonIOException e) {
                        e.printStackTrace();
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailureResponce(Object obj, int type) {

                }
            });
        }

    }

    private void setCameraAutoZooming() {
        LatLngBounds.Builder boundsBuilder = LatLngBounds.builder();
        for (LatLng point : mPathPolygonPoints) {
            boundsBuilder.include(point);
        }
        LatLngBounds bounds = boundsBuilder.build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, ZOOM_POWER);
        mMap.moveCamera(cameraUpdate);
    }

    private void bindLocationService()
    {
        Intent intent = new Intent(this, LocationService.class);
        startService(intent);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void unbindLocationService()
    {
        if (mServiceBound)
        {
            previousLatLng = null;
            currentLatLng = null;
            fabMenu.setVisibility(View.GONE);
            ((TextView)findViewById(R.id.testLatLngTV)).setText("");
            userDetailView.startAnimation(slideupHideAnim);
            slideupHideAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    userDetailView.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            if(mLocationService!=null) {
                mLocationService.initLocationListener(null);
            }
            unbindService(mServiceConnection);
            mServiceBound = false;
        }
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mServiceBound = false;
            if(mLocationService!=null) {
                mLocationService.initLocationListener(null);
            }
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationService.MyBinder myBinder = (LocationService.MyBinder) service;
            mLocationService = myBinder.getService();
            mServiceBound = true;
            mLocationService.initLocationListener(HomeActivity.this);
        }
    };

    @Override
    public void onLocationUpdate(LatLng latlng) {
        if(!SharedClass.isInTestingMode)
        {
            if (vendorMarker != null) {
                String text = Utility.getFormatedData_HH_mm_ss(System.currentTimeMillis() + "");
                text = text + "\n" + latlng.latitude + " , " + latlng.longitude;
                ((TextView) findViewById(R.id.testLatLngTV)).setText(text);
                if (previousLatLng == null) {
                    previousLatLng = new LatLng(latlng.latitude, latlng.longitude);
                    currentLatLng = new LatLng(latlng.latitude, latlng.longitude);
                } else {
                    previousLatLng = currentLatLng;
                    currentLatLng = new LatLng(latlng.latitude, latlng.longitude);
                }
                animateVendorMove(previousLatLng, currentLatLng);
                socketManager.SendVendorPosition(SharedClass.user.getId(),latlng.latitude+"",latlng.longitude+"",HomeActivity.this);
            }
        }
    }

    private void animateVendorMove(final LatLng startPosition, final LatLng finalPosition) {
        if(vendorMarker!=null) {
            final long start = SystemClock.uptimeMillis();
            final Interpolator interpolator = new AccelerateDecelerateInterpolator();
            long elapsed = SystemClock.uptimeMillis() - start;
            float t = elapsed / 3000;
            interpolator.getInterpolation(t);
            LatLng currentPosition = new LatLng(startPosition.latitude * (1 - t) + finalPosition.latitude * t, startPosition.longitude * (1 - t) + finalPosition.longitude * t);
            vendorMarker.setPosition(currentPosition);
        }

        /*final long startTime = SystemClock.uptimeMillis();
        final Interpolator interpolator = new LinearInterpolator();
        final long duration = 1000;
        long elapsed = SystemClock.uptimeMillis() - startTime;
        float t = interpolator.getInterpolation((float) elapsed / duration);
        double lat = (endLatLng.latitude - beginLatLng.latitude) * t + beginLatLng.latitude;
        double lngDelta = endLatLng.longitude - beginLatLng.longitude;
        if (Math.abs(lngDelta) > 180) {
            lngDelta -= Math.signum(lngDelta) * 360;
        }
        double lng = lngDelta * t + beginLatLng.longitude;
        vendorMarker.setPosition(new LatLng(lat, lng));*/
    }

    private void creatTrip(final JsonObject path, final String pathString)
    {
        TripModel model = new TripModel();
        model.v = "";
        model.endTime = "";
        model.startTime = System.currentTimeMillis()+"";
        model.vendorId = SharedClass.user.getId();
        model.vendorLatLng = SharedClass.latitude+","+SharedClass.longitude;
        model.vendorAddress = SharedClass.address;
        model.vendorName = SharedClass.user.getFirstName()+" "+SharedClass.user.getLastName();
        model.vendorProfilePic = SharedClass.user.getProfilePicture();
        model.userId = requestLists.get(0).user.getId();
        model.userLatLng = requestLists.get(0).user.getLatitude()+","+requestLists.get(0).user.getLongitude();
        model.userAddress = requestLists.get(0).user.getAddress();
        model.userName = requestLists.get(0).user.getFirstName()+" "+requestLists.get(0).user.getLastName();
        model.userProfilePic = requestLists.get(0).user.getProfilePicture();
        model.serviceId = requestLists.get(0).service.serviceId;
        model.serviceName = requestLists.get(0).service.serviceName;
        model.servicePrice = requestLists.get(0).service.servicePrice;
        model.serviceIcon = requestLists.get(0).service.serviceImage;
        model.tripPath = pathString;
        model.vendorRating = "";
        model.userRating = "";
        model.mapImage = "";
        ServerCalls.createTrip(model, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                JsonObject tripResponse = (JsonObject) obj;
                tripModel = new Gson().fromJson(tripResponse,TripModel.class);
                sendTripPathToUser(tripModel.id,path,pathString);
                Utility.ShowGenericToast(HomeActivity.this,"Trip Created");
                SharedClass.user.setTripId(tripModel.id);
                if(SharedClass.isInTestingMode)
                {
                    launchSandboxTesting();
                }
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try {
                    if (type == Constants.STRING) {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    } else if (type == Constants.THROWABLE) {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                        } else {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content), throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                } catch (Exception exp) {
                }
            }
        });
    }

    private void finishTrip()
    {
        Utility.ShowProgressDialog(HomeActivity.this,getResources().getString(R.string.please_wait));
        if(handler!=null && SharedClass.isInTestingMode)
        {
            handler.removeCallbacks(runnable);
        }
        SharedClass.user.setService_status("Free");
        SharedClass.user.setTripId("");
        notificationmanager.cancelAll();
        notificationmanager = null;
        ServerCalls.finishTrip(tripModel.id, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj)
            {
                JsonObject tripResponse = (JsonObject) obj;
                tripModel = new Gson().fromJson(tripResponse,TripModel.class);
                createStaticMapImageLink();
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try {
                    Utility.HideProgressDialog();
                    if (type == Constants.STRING) {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    } else if (type == Constants.THROWABLE) {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                        } else {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content), throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp)
                {
                }
            }
        });
    }

    private void sendTripPathToUser(String tripId,JsonObject path, final String pathString)
    {
        ServerCalls.setTripRoute(tripId,path, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                Utility.ShowGenericToast(HomeActivity.this,"Route sent to user");
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    if(type== Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    }
                    else if(type==Constants.THROWABLE)
                    {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                        {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                        }
                        else
                            {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp){}
            }
        });
    }

    private void showNotificationOnStartTrip(String name,String dateTime,String pic)
    {
        final RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.notification_view);
        Intent notificationIntent = new Intent(this, SplashActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(getString(R.string.app_name))
                .setAutoCancel(true)
                .setOngoing(true)
                .setContentIntent(pIntent)
                .setContent(remoteViews);

        Picasso.with(this).load(pic).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                remoteViews.setImageViewBitmap(R.id.profilePic,bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
        remoteViews.setTextViewText(R.id.name,name);
        remoteViews.setTextViewText(R.id.dateTime,dateTime);
        remoteViews.setTextViewText(R.id.status,"Waiting");
        notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationmanager.notify(0, builder.build());
    }

    private void launchSandboxTesting()
    {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run()
            {
                SharedPrefrences.saveIndex(index);
                double latitude = mPathPolygonPoints.get(index).latitude;
                double longitude = mPathPolygonPoints.get(index).longitude;
                if(previousLatLng==null)
                {
                    previousLatLng = new LatLng(latitude,longitude);
                    currentLatLng = new LatLng(latitude,longitude);
                }
                else
                {
                    previousLatLng = currentLatLng;
                    currentLatLng = new LatLng(latitude,longitude);
                }
                index++;
                animateVendorMove(previousLatLng,currentLatLng);
                socketManager.SendVendorPosition(SharedClass.user.getId(),latitude+"",longitude+"",HomeActivity.this);
                if(index<mPathPolygonPoints.size())
                {
                    handler.postDelayed(runnable, 1000);
                }
                else
                {
                    handler.removeCallbacks(runnable);
                    index = mPathPolygonPoints.size()-1;
                    latitude = mPathPolygonPoints.get(index).latitude;
                    longitude = mPathPolygonPoints.get(index).longitude;
                    previousLatLng = currentLatLng;
                    currentLatLng = new LatLng(latitude,longitude);
                    animateVendorMove(previousLatLng,currentLatLng);
                    finishService();
                }
            }
        };
        handler.postDelayed(runnable,1000);
    }

    private boolean isRangedVendorForRequest(RequestModel requestModel)
    {
        for(int i=0;i<requestModel.vendorsId.size();i++)
        {
            if(requestModel.vendorsId.get(i).equals(SharedClass.user.getId()))
            {
                return true;
            }
        }
        return false;
    }

    private void getTrip()
    {
        ServerCalls.getPreviousTrips(Constants.TRIP + "", SharedClass.user.getTripId(),"0", new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                try
                {
                    JsonObject response = (JsonObject)obj;
                    JsonArray tripObject = response.getAsJsonArray("array");
                    if(tripObject.size()>0)
                    {
                        tripStarted = true;
                        tripModel = new Gson().fromJson(tripObject.get(0).getAsJsonObject(),TripModel.class);
                        getUser(tripModel.userId);
                    }
                }
                catch (Exception exp)
                {
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    if(type== Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    }
                    else if(type==Constants.THROWABLE)
                    {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                        {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                        }
                        else {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp){}
            }
        });
    }

    private void getUser(String userId)
    {
        ServerCalls.getUser(userId, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                try
                {
                    JsonObject userObj = (JsonObject)obj;
                    user = null;
                    user = new User();
                    user.setId(userObj.get("_id").getAsString());
                    user.setFirstName(userObj.get("first_name").getAsString());
                    user.setLastName(userObj.get("last_name").getAsString());
                    user.setEmail(userObj.get("email").getAsString());
                    user.setPhoneNumber(userObj.get("phone").getAsString());
                    user.setProfilePicture(userObj.get("profile_pic").getAsString());
                    user.setRating(userObj.get("rating").getAsString());
                    user.setLatitude(Double.parseDouble(tripModel.userLatLng.split(",")[0]));
                    user.setLongitude(Double.parseDouble(tripModel.userLatLng.split(",")[1]));
                    user.setAddress(tripModel.userAddress);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run()
                        {
                            LatLng userLatLng = new LatLng(user.getLatitude(), user.getLongitude());
                            SharedClass.latitude = Double.parseDouble(tripModel.vendorLatLng.split(",")[0]);
                            SharedClass.longitude = Double.parseDouble(tripModel.vendorLatLng.split(",")[1]);
                            LatLng vendorLatLng = new LatLng(SharedClass.latitude, SharedClass.longitude);
                            drawPathBetweenUserVendor(tripModel.tripPath,userLatLng, vendorLatLng, false);
                        }
                    });
                }
                catch (Exception exp)
                {
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    if(type== Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    }
                    else if(type==Constants.THROWABLE)
                    {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                        {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                        }
                        else {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp){}
            }
        });
    }

    private void showRequestMarker(final String requestId, final String serviceicon, final LatLng pos)
    {
        final View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.request_icon_view, null);
        ImageView imageView = (ImageView)marker.findViewById(R.id.marker);
        imageView.setColorFilter(Color.parseColor(SharedClass.colors.get(maxColourIndex).hexString));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos,ZOOM_POWER+3));
        Marker reqMarker = mMap.addMarker(new MarkerOptions()
                .position(pos)
                .icon(BitmapDescriptorFactory.fromBitmap(Utility.createDrawableFromView(HomeActivity.this,marker))));

        if(requestMarkersMap==null)
        {
            requestMarkersMap = new HashMap<>();
        }
        requestMarkersMap.put(requestId,reqMarker);
        maxColourIndex++;

    }

    private void setRequestInList()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                requestRV.setVisibility(View.VISIBLE);
                if(requestAdapter==null)
                {
                    LinearLayoutManager layout = new LinearLayoutManager(HomeActivity.this, LinearLayoutManager.HORIZONTAL, false);
                    requestRV.setLayoutManager(layout);
                    requestAdapter = new RequestAdapter(HomeActivity.this,requestLists);
                    requestRV.setAdapter(requestAdapter);
                }
                else
                {
                    requestAdapter.notifyDataSetChanged();
                }
            }
        });

    }

    public void onAcceptRequest(String requestId)
    {
        selectedRequestId = requestId;
        //hideRequestListView();
        Utility.ShowGenericToast(HomeActivity.this,"Accepted");
        updateRequestStatus(selectedRequestId, SharedClass.user.getId(), "true");
    }

    private void createStaticMapImageLink()
    {
        String url = "https://maps.googleapis.com/maps/api/staticmap?size=600x250&format=png&sensor=false&markers=color:0xfe9f00%7C"+tripModel.userLatLng+"&markers=color:0x3CB371%7C"+tripModel.vendorLatLng+"&maptype=roadmap&style="+SharedClass.mapStyle+"&path=weight:4%7Ccolor:0xd68c0e%7Cenc:"+tripModel.tripPath;
        ServerCalls.shortenUrl(url, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                Utility.HideProgressDialog();
                JsonObject staticMapResponse = ((Response<JsonObject>)obj).body();
                String shortUrl = staticMapResponse.get("id").getAsString();
                tripModel.mapImage = shortUrl;
                startActivity(new Intent(HomeActivity.this, ReviewServiceActivity.class).putExtra("tripmodel",tripModel));
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    Utility.HideProgressDialog();
                    if(type== Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    }
                    else if(type==Constants.THROWABLE)
                    {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                        {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                        }
                        else {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp){}
            }
        });
    }

    private void logout()
    {
        Utility.ShowProgressDialog(HomeActivity.this,getResources().getString(R.string.please_wait));
        ServerCalls.logout(SharedClass.accountType + "", SharedClass.user.getId(), new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                SharedPrefrences.clearAll();
                SharedClass.clearAll();
                startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                finishAffinity();
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    if(type == Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    }
                    else if(type==Constants.THROWABLE)
                    {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                        {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                        }
                        else {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp){}
            }
        });
    }

}

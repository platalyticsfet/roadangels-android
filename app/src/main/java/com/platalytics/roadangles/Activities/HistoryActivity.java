package com.platalytics.roadangles.Activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.platalytics.roadangles.Activities.User.ServicesActivity;
import com.platalytics.roadangles.Adapters.HistoryAdapter;
import com.platalytics.roadangles.Adapters.ServicesAdapter;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.Models.TripModel;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class HistoryActivity extends BaseActivity implements ResponceCallback{

    RecyclerView historyRV;
    HistoryAdapter adapter;
    List<TripModel> tripModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(HistoryActivity.this);
        historyRV = (RecyclerView)findViewById(R.id.historyRV);
        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ServerCalls.getPreviousTrips(SharedClass.accountType+"",SharedClass.user.getId(),SharedClass.user.getAuthKey(),HistoryActivity.this);
    }

    public void dialog(boolean value){

        if(value)
        {
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }

    @Override
    public void onSuccessResponce(Object obj) {
        try
        {
            JsonObject response = (JsonObject)obj;
            JsonArray historyStatus = response.getAsJsonArray("array");
            if(historyStatus.size()>0)
            {
                for(int i=0;i<historyStatus.size();i++)
                {
                    TripModel model = new Gson().fromJson(historyStatus.get(i).getAsJsonObject(),TripModel.class);
                    tripModelList.add(model);
                }
                setAdapter();
            }
        }
        catch (Exception exp)
        {
            exp.printStackTrace();
        }
    }

    @Override
    public void onFailureResponce(Object obj, int type) {
        try
        {
            if(type== Constants.STRING)
            {
                String message = obj.toString();
                Utility.ShowErrorToast(HistoryActivity.this,message);
            }
            else if(type==Constants.THROWABLE)
            {
                Throwable throwable = (Throwable) obj;
                String exceptionClassName = throwable.getClass().getName();
                if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                {
                    Utility.ShowErrorSneakbar(HistoryActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                }
                else {
                    Utility.ShowErrorSneakbar(HistoryActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                }
            }
            else if(type==Constants.AUTHENTICATION)
            {
                Utility.ShowGenericToast(HistoryActivity.this,Constants.AUTHENTICATION_ERROR);
                SharedPrefrences.clearAll();
                SharedClass.clearAll();
                startActivity(new Intent(HistoryActivity.this, SplashActivity.class));
                finishAffinity();
            }
        }
        catch (Exception exp){}
    }

    private void setAdapter()
    {
        LinearLayoutManager layout = new LinearLayoutManager(HistoryActivity.this, LinearLayoutManager.VERTICAL, false);
        historyRV.setLayoutManager(layout);
        adapter = new HistoryAdapter(HistoryActivity.this,tripModelList);
        historyRV.setAdapter(adapter);
    }
}

package com.platalytics.roadangles.Activities.User;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.github.ornolfr.ratingview.RatingView;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.platalytics.roadangles.Activities.ChatActivity;
import com.platalytics.roadangles.Activities.HistoryActivity;
import com.platalytics.roadangles.Activities.ProfileActivity;
import com.platalytics.roadangles.Activities.ReviewServiceActivity;
import com.platalytics.roadangles.Activities.SettingActivity;
import com.platalytics.roadangles.Activities.SplashActivity;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Bus.Events;
import com.platalytics.roadangles.Bus.GlobalBus;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.Interfaces.SocketMessageListerner;
import com.platalytics.roadangles.Models.ChatModel;
import com.platalytics.roadangles.Models.GoogleAddressModel;
import com.platalytics.roadangles.Models.GoogleDistanceModel;
import com.platalytics.roadangles.Models.NearByVendorModel;
import com.platalytics.roadangles.Models.RequestModel;
import com.platalytics.roadangles.Models.ServicesModel;
import com.platalytics.roadangles.Models.TripModel;
import com.platalytics.roadangles.Models.User;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.Reciever.NetworkChangeReceiver;
import com.platalytics.roadangles.RoadAngelsApplication;
import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.Utility;
import com.rodolfonavalon.shaperipplelibrary.ShapeRipple;
import com.rodolfonavalon.shaperipplelibrary.model.Circle;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class HomeActivity extends BaseActivity implements
        OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMarkerClickListener,
        SocketMessageListerner,
        View.OnClickListener {

    private Toolbar toolbar;
    private GoogleMap mMap;
    private CircleImageView profilePic;
    private TextView usernameTv,locationTv;
    Button chooseServiceBtn,cancleRequest;
    private View mapView;
    ImageView searchBtn;
    LinearLayout pickLocationBtn;
    private static final String TAG = "HomeActivity";
    private int ZOOM_POWER = 10;
    private int LOCATION_REQUEST_CODE = 1001;
    private int SERVICE_CODE = 1002;
    private int FINISH_SERVICE = 1003;
    private ShapeRipple ripple;
    List<NearByVendorModel> nearByVendors = new ArrayList<>();
    int Range; // in KM
    List<Marker> nearByVendorsMarker = new ArrayList<>();
    private boolean wasCameraMoving = false;
    Animation slideupHideAnim,slidedownHideAnim,slideupShowAnim,slidedownShowAnim;
    RelativeLayout progressView;
    RequestModel requestModel = null;
    Marker userMarker = null;
    Marker vendorMarker = null;
    private List<LatLng> mPathPolygonPoints = new ArrayList<>();
    User vendor = null;
    CardView vendorDetailView;
    private TripModel tripModel = null;
    LatLng previousLatLng = null;
    LatLng currentLatLng = null;
    private NotificationManager notificationmanager = null;
    private SlidingRootNav slidingRootNav;
    LinearLayout nav_payment,nav_history,nav_notification,nav_setting,nav_help,nav_logout;
    Handler handler = null;
    Runnable runnable = null;
    int REQUEST_WAITING_TIME = 30*1000;    // 30 seconds
    boolean requestAccepted;
    private String tripRoute;
    static TextView tv_check_connection;
    FloatingActionMenu fabMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_enduser);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(HomeActivity.this);
        requestAccepted = false;
        initSocketManager();
        slideupHideAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up_hide);
        slidedownHideAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down_hide);
        slideupShowAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up_visible);
        slidedownShowAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down_visible);
        Range = 10; // in KM
        notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        setToolbarAndNavDrawer(savedInstanceState);
        initObject();
        inflateGoogleMap();
    }

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                if(SharedClass.user.getTripId()!=null && SharedClass.user.getTripId().length()>0 && !requestAccepted)
                {
                    requestAccepted = true;
                    getTrip();
                }
                else if(!requestAccepted)
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(nearByVendors==null || nearByVendors.size()==0)
                            {
                                getNearByVendors(SharedClass.latitude+"",SharedClass.longitude+"");
                            }
                        }
                    });
                }
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }

    public void passNewChatMessage(ChatModel chat) {
        Events.GetNewMessage getNewMessage =
                new Events.GetNewMessage(chat);
        GlobalBus.getBus().post(getNewMessage);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (SharedClass.user != null) {
            if (SharedClass.user.getProfilePicture() != null) {
                Picasso.with(HomeActivity.this).load(SharedClass.user.getProfilePicture()).error(R.drawable.user_placeholder).placeholder(R.drawable.user_placeholder).into(profilePic);
            }
            usernameTv.setText(SharedClass.user.getFirstName() + " " + SharedClass.user.getLastName());
            ((TextView)findViewById(R.id.ratingTv)).setText(SharedClass.user.getRating());
            if(SharedClass.user.getRating()==null || SharedClass.user.getRating().equals(""))
            {
                findViewById(R.id.ratingstar).setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GlobalBus.getBus().unregister(this);
        socketManager.socketDisconnect();
        if(cancleRequest.getVisibility()==View.VISIBLE)
        {
            if(requestModel!=null)
            {
                cancelRequest();
            }
        }
    }

    private void initSocketManager() {
        RoadAngelsApplication app = (RoadAngelsApplication) getApplication();
        socketManager = app.getSocketManager();
        socketManager.connectSocketManager();
        socketManager.initSocketListener(HomeActivity.this);
    }

    private void setToolbarAndNavDrawer(Bundle savedInstanceState) {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        slidingRootNav = new SlidingRootNavBuilder(this)
                .withToolbarMenuToggle(toolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.user_menu_left_drawer)
                .inject();
    }

    private void inflateGoogleMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);
    }

    private void initObject() {
        ripple = (ShapeRipple) findViewById(R.id.ripple);
        ripple.setRippleShape(new Circle());
        ripple.setRippleColor(getResources().getColor(R.color.colorPrimary));
        ripple.setRippleFromColor(getResources().getColor(R.color.colorPrimaryDark));
        ripple.setRippleDuration(5000);
        ripple.setVisibility(View.GONE);

        tv_check_connection = (TextView)findViewById(R.id.tv_check_connection);
        vendorDetailView = (CardView) findViewById(R.id.vendorDetailView);
        chooseServiceBtn = (Button) findViewById(R.id.chooseServiceBtn);
        cancleRequest = (Button) findViewById(R.id.cancleRequest);
        searchBtn = (ImageView)findViewById(R.id.searchBtn);
        locationTv = (TextView)findViewById(R.id.locationTv);
        pickLocationBtn = (LinearLayout)findViewById(R.id.pickLocationBtn);
        progressView = (RelativeLayout) findViewById(R.id.progressView);

        fabMenu = (FloatingActionMenu)findViewById(R.id.fabMenu);
        fabMenu.setVisibility(View.GONE);
        findViewById(R.id.menu_call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabMenu.close(true);
                if(vendor.getPhoneNumber()!=null && !vendor.getPhoneNumber().equals(""))
                {
                    String phone = vendor.getPhoneNumber();
                    startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null)));
                }
            }
        });
        findViewById(R.id.menu_sms).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabMenu.close(true);
                if(vendor.getPhoneNumber()!=null && !vendor.getPhoneNumber().equals(""))
                {
                    Uri smsUri = Uri.parse("tel:123456");
                    //intent.putExtra("sms_body", "Enter Some Text");
                    startActivity(new Intent(Intent.ACTION_VIEW, smsUri).setType("vnd.android-dir/mms-sms"));
                }
            }
        });
        findViewById(R.id.menu_chat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabMenu.close(true);
                Intent intent = new Intent(HomeActivity.this, ChatActivity.class);
                if(vendor!=null)
                {
                    intent.putExtra("otherUserId",vendor.getId());
                    intent.putExtra("otherUserName",vendor.getFirstName() +" "+vendor.getLastName());
                }
                intent.putExtra("tripId",SharedClass.user.getTripId());
                startActivity(intent);
            }
        });

        nav_payment = (LinearLayout) findViewById(R.id.nav_payment);
        nav_history = (LinearLayout) findViewById(R.id.nav_history);
        nav_notification = (LinearLayout) findViewById(R.id.nav_notification);
        nav_setting = (LinearLayout) findViewById(R.id.nav_setting);
        nav_help = (LinearLayout) findViewById(R.id.nav_help);
        nav_logout = (LinearLayout) findViewById(R.id.nav_logout);

        nav_payment.setOnClickListener(this);
        nav_history.setOnClickListener(this);
        nav_notification.setOnClickListener(this);
        nav_setting.setOnClickListener(this);
        nav_help.setOnClickListener(this);
        nav_logout.setOnClickListener(this);


        profilePic = (CircleImageView) findViewById(R.id.profilePic);
        usernameTv = (TextView) findViewById(R.id.usernameTv);
        searchBtn.setOnClickListener(this);
        chooseServiceBtn.setOnClickListener(this);
        cancleRequest.setOnClickListener(this);
        progressView.setOnClickListener(this);

        profilePic.setOnClickListener(this);
    }

    @Subscribe
    public void sendMessage(Events.SendNewMessage newMessage)
    {
        socketManager.SendChatMessage(newMessage.senderId,newMessage.recieverId,newMessage.authKey,newMessage.message,newMessage.date,newMessage.tripId);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.profilePic:
                slidingRootNav.closeMenu();
                startActivity(new Intent(HomeActivity.this, ProfileActivity.class));
                break;
            case R.id.searchBtn:
                findPlace(LOCATION_REQUEST_CODE);
                //startActivity(new Intent(HomeActivity.this, RequestsActivity.class));
                break;
            case R.id.chooseServiceBtn:
                if(SharedClass.longitude==0.0 || SharedClass.latitude==0.0)
                {
                    ShowAlertDialog("Please click circular aim button in bottom right for getting correct location");
                }
                else {
                    NearByVendorModel nearestVendor = new NearByVendorModel();
                    NearByVendorModel farestVendor = new NearByVendorModel();
                    double minDistance = 0.0f;
                    double maxDistance = 0.0f;
                    for (int i = 0; i < nearByVendors.size(); i++) {
                        LatLng source = new LatLng(SharedClass.latitude, SharedClass.longitude);
                        LatLng vendor = new LatLng(Double.parseDouble(nearByVendors.get(i).latitude), Double.parseDouble(nearByVendors.get(i).longitude));
                        double tempDistance = Utility.getDistance(source, vendor);
                        if (i == 0) {
                            minDistance = Utility.getDistance(source, vendor);
                            maxDistance = minDistance;
                            nearestVendor = nearByVendors.get(0);
                            farestVendor = nearByVendors.get(0);
                        }
                        if (tempDistance < minDistance) {
                            minDistance = tempDistance;
                            nearestVendor = nearByVendors.get(i);
                        }
                        if (tempDistance > maxDistance) {
                            maxDistance = tempDistance;
                            farestVendor = nearByVendors.get(i);
                        }
                    }
                    startActivityForResult(new Intent(HomeActivity.this, ServicesActivity.class).putExtra("nearestVendor", nearestVendor).putExtra("farestVendor", farestVendor), SERVICE_CODE);
                }
                break;
            case R.id.cancleRequest:
                Utility.ShowYesNoDialog(HomeActivity.this,"Cancle Request", "You really want to cancle the request ?", "Yes", "No", new ResponceCallback() {
                    @Override
                    public void onSuccessResponce(Object obj) {
                        cancelRequest();
                    }

                    @Override
                    public void onFailureResponce(Object obj, int type) {

                    }
                });
                break;
            case R.id.progressView:
                break;
            case R.id.nav_payment:
                slidingRootNav.closeMenu();
                break;
            case R.id.nav_history:
                startActivity(new Intent(HomeActivity.this, HistoryActivity.class));
                slidingRootNav.closeMenu();
                break;
            case R.id.nav_notification:
                slidingRootNav.closeMenu();
                break;
            case R.id.nav_setting:
                slidingRootNav.closeMenu();
                startActivity(new Intent(HomeActivity.this, SettingActivity.class));
                break;
            case R.id.nav_help:
                slidingRootNav.closeMenu();
                break;
            case R.id.nav_logout:
                slidingRootNav.closeMenu();
                Utility.ShowYesNoDialog(HomeActivity.this,"Logout", "You really want to logout ?", "Yes", "No", new ResponceCallback() {
                    @Override
                    public void onSuccessResponce(Object obj) {
                        logout();
                    }

                    @Override
                    public void onFailureResponce(Object obj, int type) {

                    }
                });
                break;
        }
    }

    private void cancelRequest() {
        try
        {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(handler!=null && runnable!=null)
                    {
                        handler.removeCallbacks(runnable);
                        handler = null;
                        runnable = null;
                    }
                }
            });
        }
        catch (Exception exp)
        {
            exp.printStackTrace();
        }
        if(requestModel!=null && requestModel.requestId!=null)
        {
            ServerCalls.cancelRequest(requestModel.requestId, new ResponceCallback() {
                @Override
                public void onSuccessResponce(Object obj) {
                    try
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                UpdateButtonViewonRequestCancel();
                                Utility.ShowGenericToast(HomeActivity.this,"Request Cancelled");
                            }
                        });
                    }
                    catch (Exception exp)
                    {
                        exp.printStackTrace();
                    }
                }

                @Override
                public void onFailureResponce(Object obj, int type) {
                    try
                    {
                        if(type== Constants.STRING)
                        {
                            String message = obj.toString();
                            Utility.ShowErrorToast(HomeActivity.this,message);
                        }
                        else if(type==Constants.THROWABLE)
                        {
                            Throwable throwable = (Throwable) obj;
                            String exceptionClassName = throwable.getClass().getName();
                            if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                            {
                                Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                            }
                            else {
                                Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                            }
                        }
                        else if(type==Constants.AUTHENTICATION)
                        {
                            Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                            SharedPrefrences.clearAll();
                            SharedClass.clearAll();
                            startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                            finishAffinity();
                        }
                    }
                    catch (Exception exp){}
                }
            });
        }
        else
        {
            UpdateButtonViewonRequestCancel();
        }
    }

    @Override
    public void onBackPressed() {
        if (slidingRootNav.isMenuOpened()) {
            slidingRootNav.closeMenu();
        }/* else if(SharedClass.user.getTripId()!=null && SharedClass.user.getTripId().length()>0) {
            Utility.ShowErrorSneakbar(findViewById(android.R.id.content),"Can not go back during trip");
        }*/
        else {
            super.onBackPressed();
        }
    }

    public void findPlace(int RequestCode) {
        try {
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setFilter(typeFilter).build(this);
            startActivityForResult(intent, RequestCode);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            SharedClass.latitude = (SharedPrefrences.getUserLatitude().equals("") ? 0.0 : Double.parseDouble(SharedPrefrences.getUserLatitude()));
            SharedClass.longitude = (SharedPrefrences.getUserLongitude().equals("") ? 0.0 : Double.parseDouble(SharedPrefrences.getUserLongitude()));
            getAdress(SharedClass.latitude,SharedClass.longitude);
            mMap = googleMap;
            mMap.setMyLocationEnabled(true);
            mMap.setOnMyLocationButtonClickListener(this);
            MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style);
            //mMap.setMapStyle(style);

            //mMap.setOnMarkerClickListener(this);
            LatLng myLoc = new LatLng(SharedClass.latitude,SharedClass.longitude);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLoc,ZOOM_POWER+3));

            if (mapView != null && mapView.findViewById(Integer.parseInt("1")) != null) {
                View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                layoutParams.setMargins(0, 0, Utility.dpToPx(HomeActivity.this,10), Utility.dpToPx(HomeActivity.this,11));
            }
            mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {

                @Override
                public void onCameraMove() {
                    wasCameraMoving = true;
                }
            });
            mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    if(wasCameraMoving)
                    {
                        wasCameraMoving = false;
                        CameraPosition cameraPosition = mMap.getCameraPosition();
                        SharedClass.latitude = cameraPosition.target.latitude;
                        SharedClass.longitude = cameraPosition.target.longitude;
                        findViewById(R.id.timerProgress).setVisibility(View.VISIBLE);
                        getNearestVendor();
                        getAdress(cameraPosition.target.latitude,cameraPosition.target.longitude);
                    }
                }
            });
            onMyLocationButtonClick();
            if(SharedClass.user.getTripId()!=null && SharedClass.user.getTripId().length()>0)
            {
                getTrip();
            }
            else
            {
                notificationmanager.cancelAll();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getNearByVendors(SharedClass.latitude+"",SharedClass.longitude+"");
                    }
                });
            }
        }
        else
        {
            return;
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        if(mMap!=null) {
            Location location = mMap.getMyLocation();
            if (location!=null && SharedClass.latitude == 0.0) {
                SharedClass.latitude = location.getLatitude();
                SharedClass.longitude = location.getLongitude();
                getAdress(SharedClass.latitude,SharedClass.longitude);
                SharedPrefrences.saveUserLocation(SharedClass.latitude + "", SharedClass.longitude + "");
            }
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK)
        {
            if (requestCode == LOCATION_REQUEST_CODE)
            {
                Place place = PlaceAutocomplete.getPlace(this, data);
                LatLng myLoc = place.getLatLng();
                SharedClass.latitude = myLoc.latitude;
                SharedClass.longitude = myLoc.longitude;
                locationTv.setText(place.getAddress().toString());
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLoc,ZOOM_POWER+3));
            }
            else if(requestCode==SERVICE_CODE)
            {
                UpdateButtonViewonRequest();
                ServicesModel servicesModel = (ServicesModel) data.getSerializableExtra("service");
                String note = data.getStringExtra("note");
                sendServiceRequest(servicesModel,note);
            }
            else if(requestCode==FINISH_SERVICE)
            {
                UpdateButtonViewonFinishTrip();
                getNearByVendors(SharedClass.latitude+"",SharedClass.longitude+"");
            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(this, data);
            Log.i(TAG, status.getStatusMessage());
        } else if (resultCode == RESULT_CANCELED) {
            // The user canceled the operation.
        }
    }

    private void UpdateButtonViewonRequest() {
        progressView.setVisibility(View.VISIBLE);
        ((CardView)findViewById(R.id.locationCV)).startAnimation(slideupHideAnim);
        slideupHideAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                ((CardView)findViewById(R.id.locationCV)).setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        chooseServiceBtn.startAnimation(slidedownHideAnim);
        slidedownHideAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                chooseServiceBtn.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        cancleRequest.setVisibility(View.VISIBLE);
        cancleRequest.startAnimation(slideupShowAnim);
    }

    private void UpdateButtonViewonRequestCancel() {
        requestModel = null;
        progressView.setVisibility(View.GONE);
        ((CardView)findViewById(R.id.locationCV)).setVisibility(View.VISIBLE);
        ((CardView)findViewById(R.id.locationCV)).startAnimation(slidedownShowAnim);

        chooseServiceBtn.setVisibility(View.VISIBLE);
        chooseServiceBtn.startAnimation(slideupShowAnim);

        cancleRequest.startAnimation(slidedownHideAnim);
        slidedownHideAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                cancleRequest.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void UpdateButtonViewonFinishTrip() {
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }
        mMap.clear();
        notificationmanager.cancelAll();
        vendor = null;
        tripModel = null;
        previousLatLng = null;
        currentLatLng = null;
        vendorMarker = null;
        userMarker = null;
        mPathPolygonPoints.clear();
        findViewById(R.id.mapmarker).setVisibility(View.VISIBLE);
        ((CardView)findViewById(R.id.locationCV)).setVisibility(View.VISIBLE);
        ((CardView)findViewById(R.id.locationCV)).startAnimation(slidedownShowAnim);

        chooseServiceBtn.setVisibility(View.VISIBLE);
        chooseServiceBtn.startAnimation(slideupShowAnim);

        vendorDetailView.startAnimation(slidedownHideAnim);
        slidedownHideAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                vendorDetailView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void sendServiceRequest(final ServicesModel servicesModel,String serviceNote)
    {
        handler = null;
        final String time = System.currentTimeMillis()+"";
        ServerCalls.sendRequest(servicesModel.serviceId,serviceNote,SharedClass.latitude + "", SharedClass.longitude + "",SharedClass.address,time, Range+"", new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                try
                {
                    JsonObject requestStatus = (JsonObject)obj;
                    if(requestModel==null) {
                        requestModel = new RequestModel();
                    }
                    requestModel.vendorsId.add(requestStatus.get("vendor_id").getAsString());
                    requestModel.request_status = requestStatus.get("request_status").getAsString();
                    requestModel.service.serviceId = requestStatus.get("service_id").getAsString();
                    requestModel.serviceNote = requestStatus.get("service_note").getAsString();
                    requestModel.request_time = requestStatus.get("request_time").getAsString();
                    requestModel.user_lng = requestStatus.get("user_lng").getAsString();
                    requestModel.user_lat = requestStatus.get("user_lat").getAsString();
                    requestModel.user_id = requestStatus.get("user_id").getAsString();
                    requestModel.requestId = requestStatus.get("_id").getAsString();
                    socketManager.SendRequestToVendor(requestModel.requestId,servicesModel.serviceId,requestModel.serviceNote,SharedClass.user.getId(),SharedClass.latitude + "", SharedClass.longitude + "",SharedClass.address,time, Range+"");
                    if(handler==null)
                    {
                        handler = new Handler();
                        runnable = new Runnable() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(requestModel!=null)
                                        {
                                            if(handler!=null && runnable!=null) {
                                                handler.removeCallbacks(runnable);
                                                ShowAlertDialog(null);
                                                cancelRequest();
                                                handler = null;
                                                runnable = null;
                                            }
                                        }
                                    }
                                });
                            }
                        };
                        handler.postDelayed(runnable,REQUEST_WAITING_TIME);
                    }
                }
                catch (Exception exp)
                {
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    if(type== Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    }
                    else if(type==Constants.THROWABLE)
                    {
                        /*Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                        {
                            Utility.ShowErrorSneakbar(findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                        }
                        else {
                            Utility.ShowErrorSneakbar(findViewById(android.R.id.content),throwable.getMessage());
                        }*/
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp){}
            }
        });
    }

    private void getAdress(final double lat, final double lng)
    {
        ServerCalls.getAddressFromGoogle(lat + "", lng + "", new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                try
                {
                    JsonObject addressResponse = ((Response<JsonObject>)obj).body();
                    GoogleAddressModel addressObj = new Gson().fromJson(addressResponse, GoogleAddressModel.class);
                    if(addressObj.status.equals("OK"))
                    {
                        final String address = addressObj.results.get(0).formattedAddress;
                        locationTv.setText(address);
                        SharedClass.address = address;
                    }
                    else
                    {
                        getAdress(lat,lng);
                    }
                }
                catch (Exception exp)
                {
                    exp.printStackTrace();
                }

            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                //getAdress(lat,lng);
            }
        });
    }

    private void drawPathBetweenUserVendor(String encodingPath,LatLng userLoc,LatLng vendorLoc) {

        cancleRequest.startAnimation(slidedownHideAnim);
        slidedownHideAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                cancleRequest.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        vendorDetailView.setVisibility(View.VISIBLE);
        vendorDetailView.startAnimation(slideupShowAnim);
        fabMenu.setVisibility(View.VISIBLE);
        ((TextView)findViewById(R.id.vendorName)).setText(vendor.getFirstName()+" "+vendor.getLastName());
        String time = "";
        if(requestModel==null)
        {
            time = tripModel.startTime == null ? "" : tripModel.startTime;
        }
        else
        {
            time = requestModel.request_time == null ? "" : requestModel.request_time;
        }

        if(time.equals(""))
        {
            ((TextView)findViewById(R.id.vendorDate)).setText("N/A");
        }
        else
        {
            ((TextView)findViewById(R.id.vendorDate)).setText(Utility.getFormatedData_yyyy_MM_dd_HH_mm_ss(Long.parseLong(time)));
        }
        ((TextView)findViewById(R.id.vendorAddress)).setText(vendor.getAddress());
        Picasso.with(HomeActivity.this).load(vendor.getProfilePicture()).placeholder(R.drawable.user_placeholder).into(((CircleImageView)findViewById(R.id.vendorProfilePic)));
        if(vendor.getRating()!=null && !vendor.getRating().equals(""))
        {
            ((TextView)findViewById(R.id.rating)).setText(vendor.getRating());
            ((RatingView)findViewById(R.id.ratingbar)).setRating(Float.parseFloat(vendor.getRating()));
        }
        else
        {
            ((TextView)findViewById(R.id.rating)).setText("N/A");
            ((RatingView)findViewById(R.id.ratingbar)).setRating(0.0f);
        }

        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.clear();
            mMap.setMyLocationEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            progressView.setVisibility(View.GONE);
        }
        if (userMarker == null) {
            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.user_icon_view, null);
            userMarker = mMap.addMarker(new MarkerOptions()
                    .position(userLoc)
                    .icon(BitmapDescriptorFactory.fromBitmap(Utility.createDrawableFromView(HomeActivity.this,marker))));
        } else {
            userMarker.setPosition(userLoc);
        }
        if (vendorMarker == null) {
            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.vendor_icon_view, null);
            vendorMarker = mMap.addMarker(new MarkerOptions()
                    .position(vendorLoc)
                    .icon(BitmapDescriptorFactory.fromBitmap(Utility.createDrawableFromView(HomeActivity.this,marker))));
        } else {
            vendorMarker.setPosition(vendorLoc);
        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLoc, 13));
        findViewById(R.id.mapmarker).setVisibility(View.GONE);
        try
        {
            mPathPolygonPoints.clear();
            tripRoute = encodingPath;
            mPathPolygonPoints.addAll(Utility.decodePoly(encodingPath));
            mMap.addPolyline(new PolylineOptions()
                    .addAll(mPathPolygonPoints)
                    .width(7)
                    .color(getResources().getColor(R.color.success_green))
                    .geodesic(true));
            //setCameraAutoZooming();
        }
        catch (Exception exp)
        {
            exp.printStackTrace();
        }
        showNotificationOnStartTrip(vendor.getFirstName()+" "+vendor.getLastName(),Utility.getFormatedData_yyyy_MM_dd_HH_mm_ss(System.currentTimeMillis()),vendor.getProfilePicture());
    }

    private void setCameraAutoZooming() {
        LatLngBounds.Builder boundsBuilder = LatLngBounds.builder();
        for (LatLng point : mPathPolygonPoints) {
            boundsBuilder.include(point);
        }
        LatLngBounds bounds = boundsBuilder.build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, ZOOM_POWER);
        mMap.moveCamera(cameraUpdate);
    }

    private void getNearByVendors(String lat,String lng)
    {
        ripple.setVisibility(View.VISIBLE);
        ServerCalls.getNearByVendors(lat,lng,Range+"",new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                try
                {
                    ripple.setVisibility(View.GONE);

                    JsonObject response = (JsonObject)obj;
                    JsonArray vendorsResponse = response.getAsJsonArray("array");
                    if(vendorsResponse.size()>0)
                    {
                        nearByVendors.clear();
                        nearByVendorsMarker.clear();
                        for(int i=0;i<vendorsResponse.size();i++)
                        {
                            JsonObject jsonObject = vendorsResponse.get(i).getAsJsonObject();
                            nearByVendors.add(new Gson().fromJson(jsonObject, NearByVendorModel.class));
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                for(int i=0;i<nearByVendors.size();i++)
                                {
                                    View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.vendors_marker, null);
                                    nearByVendorsMarker.add(mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(Double.parseDouble(nearByVendors.get(i).latitude), Double.parseDouble(nearByVendors.get(i).longitude)))
                                            .icon(BitmapDescriptorFactory.fromBitmap(Utility.createDrawableFromView(HomeActivity.this,marker)))));
                                }
                            }
                        });
                    }

                }
                catch (Exception exp)
                {
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    ripple.setVisibility(View.GONE);
                    if(type== Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    }
                    else if(type==Constants.THROWABLE)
                    {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                        {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                        }
                        else
                        {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp){}
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onGetMessageAck(String event,int objectType,Object... obj) {
        if(event.equals(Constants.ACCEPT_REQUEST))
        {
            //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            JSONArray array = (JSONArray) obj[0];
            if (array.length() > 0)
            {
                try
                {
                    String requestID = array.getString(0);
                    if(requestModel!= null && requestID.equals(requestModel.requestId))
                    {
                        if(vendor==null)
                        {
                            vendor = new User();
                        }
                        if(handler!=null)
                        {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Utility.ShowGenericToast(HomeActivity.this,"Waiting Timer Closed");
                                }
                            });
                            handler.removeCallbacks(runnable);
                            handler = null;
                            runnable = null;
                        }
                        requestAccepted = true;
                        JSONObject jsonObject = array.getJSONObject(1);
                        vendor.setId(jsonObject.getString("_id"));
                        vendor.setFirstName(jsonObject.getString("first_name"));
                        vendor.setLastName(jsonObject.getString("last_name"));
                        vendor.setEmail(jsonObject.getString("email"));
                        vendor.setPhoneNumber(jsonObject.getString("phone"));
                        vendor.setProfilePicture(jsonObject.getString("profile_pic"));
                        vendor.setRating(jsonObject.getString("rating"));

                        jsonObject = array.getJSONObject(2);
                        vendor.setLatitude(Double.parseDouble(jsonObject.getString("latitude")));
                        vendor.setLongitude(Double.parseDouble(jsonObject.getString("longitude")));
                        vendor.setAddress(jsonObject.getString("address"));
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }
        else if(event.equals(Constants.FINISH_TRIP))
        {
            if(requestAccepted)
            {
                finishService(obj);
            }
        }
        else if(event.equals(Constants.TRIP_ROUTE))
        {
            if(requestAccepted)
            {
                try
                {
                    JSONObject jsonObj = (JSONObject) obj[0];
                    String pathString = jsonObj.getString("object");
                    JSONObject routeJson = new JSONObject(pathString);
                    JSONArray routeArray = routeJson.getJSONArray("routes");
                    JSONObject routes = routeArray.getJSONObject(0);
                    JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
                    tripRoute = overviewPolylines.getString("points");
                    String tripId = jsonObj.getString("tripId");
                    String vendorId = jsonObj.getString("vendor_id");
                    if(vendor.getId().equals(vendorId)) {
                        SharedClass.user.setTripId(tripId);
                        final LatLng userLatLng = new LatLng(SharedClass.latitude, SharedClass.longitude);
                        final LatLng vendorLatLng = new LatLng(vendor.getLatitude(), vendor.getLongitude());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                drawPathBetweenUserVendor(tripRoute, userLatLng, vendorLatLng);
                            }
                        });
                    }

                }
                catch (Exception exp)
                {
                    exp.printStackTrace();
                }
            }
        }
        else if(event.equals(Constants.GET_VENDOR_LOCATION))
        {
            try
            {
                JSONObject jsonObject = (JSONObject) obj[0];
                String vendorId = jsonObject.getString("vendor_id");
                if(vendor!=null && vendor.getId().equals(vendorId)) {
                    //jsonObject = jsonObject.getJSONObject("venLoc");
                    String lat = jsonObject.getString("latitude");
                    String lng = jsonObject.getString("longitude");
                    if (previousLatLng == null) {
                        previousLatLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                        currentLatLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                    } else {
                        previousLatLng = currentLatLng;
                        currentLatLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            animateVendorMove(previousLatLng, currentLatLng);
                        }
                    });
                }
            }
            catch (Exception exp)
            {
                exp.printStackTrace();
            }
        }
        else if(event.equals(Constants.RECIEVE_NEW_MESSAGE))
        {
            try
            {
                final JSONObject jsonObject = (JSONObject)obj[0];
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ChatModel chat = new Gson().fromJson(jsonObject.toString(),ChatModel.class);
                        passNewChatMessage(chat);
                    }
                });

            }
            catch (Exception exp)
            {
                Log.d("ChatParsing",exp.getMessage());
            }
        }
    }

    private void finishService(Object... obj)
    {
        try {
            JSONObject jsonObject = (JSONObject) obj[0];
            tripModel = null;
            tripModel = new TripModel(jsonObject);
            tripModel.tripPath = tripRoute;
            if (tripModel.vendorId.equals(vendor.getId())) {
                vendor = null;
                requestAccepted = false;
                SharedClass.user.setTripId("");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fabMenu.setVisibility(View.GONE);
                    }
                });
                createStaticMapImageLink();
            }
            else
            {
                tripModel = null;
            }
        }
        catch (Exception exp)
        {
            exp.printStackTrace();
        }
    }

    public void showFinishServiceDialog(String serviceName, String userAddress, String vendorAddress, String startTime, String endTime, String charges, String mapImg, final ResponceCallback responceCallback)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        final FrameLayout frameView = new FrameLayout(HomeActivity.this);
        builder.setView(frameView);
        final AlertDialog alertDialog = builder.create();
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_finishservice, frameView);
        ((TextView)dialoglayout.findViewById(R.id.serviceName)).setText(serviceName);
        ((TextView)dialoglayout.findViewById(R.id.sourceTV)).setText(userAddress);
        ((TextView)dialoglayout.findViewById(R.id.destinationTV)).setText(vendorAddress);
        ((TextView)dialoglayout.findViewById(R.id.startTime)).setText(startTime);
        ((TextView)dialoglayout.findViewById(R.id.endTime)).setText(endTime);
        ((TextView)dialoglayout.findViewById(R.id.charges)).setText(charges);
        Picasso.with(HomeActivity.this).load(mapImg).placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder).into(((ImageView)dialoglayout.findViewById(R.id.map)));
        dialoglayout.findViewById(R.id.okBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if(responceCallback!=null)
                    responceCallback.onSuccessResponce(null);
            }
        });
        alertDialog.setView(dialoglayout);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
        alertDialog.show();
    }

    private void animateVendorMove(final LatLng startPosition, final LatLng finalPosition) {
        if(vendorMarker!=null)
        {
            Log.d("VendorPos",finalPosition.latitude +" , "+finalPosition.longitude);
            final long start = SystemClock.uptimeMillis();
            final Interpolator interpolator = new AccelerateDecelerateInterpolator();
            long elapsed = SystemClock.uptimeMillis() - start;
            float t = elapsed / 1000;
            interpolator.getInterpolation(t);
            LatLng currentPosition = new LatLng(startPosition.latitude * (1 - t) + finalPosition.latitude * t, startPosition.longitude * (1 - t) + finalPosition.longitude * t);
            vendorMarker.setPosition(currentPosition);
        }
    }

    private void showNotificationOnStartTrip(String name,String dateTime,String pic)
    {
        final RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.notification_view);
        Intent notificationIntent = new Intent(this, SplashActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(getString(R.string.app_name))
                .setAutoCancel(true)
                .setOngoing(true)
                .setContentIntent(pIntent)
                .setContent(remoteViews);
        Picasso.with(this).load(pic).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                remoteViews.setImageViewBitmap(R.id.profilePic,bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
        remoteViews.setTextViewText(R.id.name,name);
        remoteViews.setTextViewText(R.id.dateTime,dateTime);
        remoteViews.setTextViewText(R.id.status,"On the way");
        notificationmanager.notify(0, builder.build());
    }

    public void ShowAlertDialog(String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        final FrameLayout frameView = new FrameLayout(HomeActivity.this);
        builder.setView(frameView);
        final AlertDialog alertDialog = builder.create();
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_alert, frameView);
        if(msg!=null)
        {
            ((TextView)dialoglayout.findViewById(R.id.error_msg)).setText(msg);
        }
        dialoglayout.findViewById(R.id.okButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(dialoglayout);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
        alertDialog.show();
    }

    private void getTrip()
    {
        ServerCalls.getPreviousTrips(Constants.TRIP + "", SharedClass.user.getTripId(),"0", new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                try
                {
                    JsonObject response = (JsonObject)obj;
                    JsonArray tripObject = response.getAsJsonArray("array");
                    if(tripObject.size()>0)
                    {
                        tripModel = new Gson().fromJson(tripObject.get(0).getAsJsonObject(),TripModel.class);
                        getVendor(tripModel.vendorId);
                        requestAccepted = true;
                    }
                }
                catch (Exception exp)
                {
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    if(type== Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    }
                    else if(type==Constants.THROWABLE)
                    {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                        {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                        }
                        else {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp){}
            }
        });
    }

    private void getVendor(String vendorId)
    {
        ServerCalls.getVendor(vendorId, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                try
                {
                    JsonObject response = (JsonObject)obj;
                    JsonArray vendorObj = response.getAsJsonArray("array");

                    if(vendorObj.size()>0)
                    {
                        vendor = null;
                        vendor = new User();
                        JsonObject jsonObject = vendorObj.get(0).getAsJsonObject();

                        vendor.setId(jsonObject.get("_id").getAsString());
                        vendor.setFirstName(jsonObject.get("first_name").getAsString());
                        vendor.setLastName(jsonObject.get("last_name").getAsString());
                        vendor.setEmail(jsonObject.get("email").getAsString());
                        vendor.setPhoneNumber(jsonObject.get("phone").getAsString());
                        vendor.setProfilePicture(jsonObject.get("profile_pic").getAsString());
                        vendor.setRating(jsonObject.get("rating").getAsString());
                        jsonObject = vendorObj.get(1).getAsJsonObject();
                        JsonObject locationJson = jsonObject.get("location").getAsJsonObject();
                        vendor.setLatitude(Double.parseDouble(locationJson.get("lat").getAsString()));
                        vendor.setLongitude(Double.parseDouble(locationJson.get("lon").getAsString()));
                        vendor.setAddress(jsonObject.get("address").getAsString());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run()
                            {
                                //UpdateButtonViewonRequest();
                                chooseServiceBtn.setVisibility(View.GONE);
                                SharedClass.latitude = Double.parseDouble(tripModel.userLatLng.split(",")[0]);
                                SharedClass.longitude = Double.parseDouble(tripModel.userLatLng.split(",")[1]);
                                final LatLng userLatLng = new LatLng(SharedClass.latitude,SharedClass.longitude);
                                final LatLng vendorLatLng = new LatLng(vendor.getLatitude(),vendor.getLongitude());
                                drawPathBetweenUserVendor(tripModel.tripPath,userLatLng,vendorLatLng);
                            }
                        });
                    }
                }
                catch (Exception exp)
                {
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    if(type== Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    }
                    else if(type==Constants.THROWABLE)
                    {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                        {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                        }
                        else {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp){}
            }
        });
    }

    private void getNearestVendor()
    {
        ServerCalls.getNearestVendor(SharedClass.latitude + "", SharedClass.longitude + "", Range + "", new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                try
                {
                    JsonObject vendorsResponse = (JsonObject)obj;
                    NearByVendorModel nearestVendor = new Gson().fromJson(vendorsResponse, NearByVendorModel.class);
                    getEstimatedTime(nearestVendor.latitude,nearestVendor.longitude);
                }
                catch (Exception exp)
                {
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            findViewById(R.id.timerProgress).setVisibility(View.GONE);
                            ((TextView)findViewById(R.id.markerTimer)).setText("N/A");
                        }
                    });
                    if(type== Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    }
                    else if(type==Constants.THROWABLE)
                    {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                        {
                            //Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                        }
                        else
                        {
                            //Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp){}
            }
        });
    }

    private void getEstimatedTime(String desLet, String desLng) {
        String origion = SharedClass.latitude + "," + SharedClass.longitude;
        final String destination = desLet + "," + desLng;
        ServerCalls.getTimeDuration(origion, destination, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                try {
                    JsonObject timeResponse = ((Response<JsonObject>) obj).body();
                    final GoogleDistanceModel googleDistanceModel = new Gson().fromJson(timeResponse, GoogleDistanceModel.class);
                    //String estimateDistance = googleDistanceModel.rows.get(0).elements.get(0).distance.text;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String estimateTime = googleDistanceModel.rows.get(0).elements.get(0).duration.text;
                            estimateTime = estimateTime.replace("mins","min").toString();
                            estimateTime = estimateTime.replace(" ","\n").toString();
                            findViewById(R.id.timerProgress).setVisibility(View.GONE);
                            ((TextView)findViewById(R.id.markerTimer)).setText(estimateTime);
                        }
                    });
                } catch (Exception exp) {
                    Utility.HideProgressDialog();
                    exp.printStackTrace();
                }
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            findViewById(R.id.timerProgress).setVisibility(View.GONE);
                            ((TextView)findViewById(R.id.markerTimer)).setText("N/A");
                        }
                    });
                    if (type == Constants.STRING) {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    } else if (type == Constants.THROWABLE) {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                        } else {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content), throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                } catch (Exception exp) {
                }
            }
        });
    }

    private void createStaticMapImageLink()
    {
        String url = "https://maps.googleapis.com/maps/api/staticmap?size=600x250&format=png&sensor=false&markers=color:0xfe9f00%7C"+tripModel.userLatLng+"&markers=color:0x3CB371%7C"+tripModel.vendorLatLng+"&maptype=roadmap&style="+SharedClass.mapStyle+"&path=weight:4%7Ccolor:0xd68c0e%7Cenc:"+tripModel.tripPath;
        ServerCalls.shortenUrl(url, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                JsonObject staticMapResponse = ((Response<JsonObject>)obj).body();
                String shortUrl = staticMapResponse.get("id").getAsString();
                tripModel.mapImage = shortUrl;
                startActivityForResult(new Intent(HomeActivity.this, ReviewServiceActivity.class).putExtra("tripmodel", tripModel), FINISH_SERVICE);
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    if(type== Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    }
                    else if(type==Constants.THROWABLE)
                    {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                        {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                        }
                        else {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp){}
            }
        });
    }

    private void logout()
    {
        Utility.ShowProgressDialog(HomeActivity.this,getResources().getString(R.string.please_wait));
        ServerCalls.logout(SharedClass.accountType + "", SharedClass.user.getId(), new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                SharedPrefrences.clearAll();
                SharedClass.clearAll();
                startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                finishAffinity();
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    if(type == Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(HomeActivity.this,message);
                    }
                    else if(type==Constants.THROWABLE)
                    {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                        {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                        }
                        else {
                            Utility.ShowErrorSneakbar(HomeActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(HomeActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp){}
            }
        });
    }

}

package com.platalytics.roadangles.Activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import com.platalytics.roadangles.Activities.User.ServicesActivity;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.Utility;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class ChangePasswordActivity extends BaseActivity {

    TextInputEditText old_passwordEt,newpasswordEt,confirmpasswordEt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(ChangePasswordActivity.this);
        old_passwordEt = (TextInputEditText)findViewById(R.id.old_passwordEt);
        newpasswordEt = (TextInputEditText)findViewById(R.id.newpasswordEt);
        confirmpasswordEt = (TextInputEditText)findViewById(R.id.confirmpasswordEt);

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.updateBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String oldPass = old_passwordEt.getText().toString().trim();
                String newPass = newpasswordEt.getText().toString().trim();
                String confirmPass = confirmpasswordEt.getText().toString().trim();
                if(oldPass.isEmpty())
                {
                    Utility.ShowErrorSneakbar(ChangePasswordActivity.this,findViewById(android.R.id.content),"Enter Current Password");
                }
                else if(newPass.isEmpty())
                {
                    Utility.ShowErrorSneakbar(ChangePasswordActivity.this,findViewById(android.R.id.content),"Enter New Password");
                }
                else if(confirmPass.isEmpty())
                {
                    Utility.ShowErrorSneakbar(ChangePasswordActivity.this,findViewById(android.R.id.content),"Enter Confirm Password");
                }
                else if(!newPass.equals(confirmPass))
                {
                    Utility.ShowErrorSneakbar(ChangePasswordActivity.this,findViewById(android.R.id.content),"New & Confirm Password should be same");
                }
                else
                {
                    Utility.ShowProgressDialog(ChangePasswordActivity.this,getResources().getString(R.string.please_wait));

                    ServerCalls.changePassword(oldPass,newPass, new ResponceCallback() {
                        @Override
                        public void onSuccessResponce(Object obj) {
                            Utility.ShowSuccessDialog(ChangePasswordActivity.this,"Password Updated Successfully", new ResponceCallback() {
                                @Override
                                public void onSuccessResponce(Object obj) {
                                    finish();
                                }

                                @Override
                                public void onFailureResponce(Object obj, int type) {

                                }
                            });
                        }

                        @Override
                        public void onFailureResponce(Object obj, int type) {
                            try {
                                if (type == Constants.STRING) {
                                    String message = obj.toString();
                                    Utility.ShowErrorToast(ChangePasswordActivity.this,message);
                                } else if (type == Constants.THROWABLE) {
                                    Throwable throwable = (Throwable) obj;
                                    String exceptionClassName = throwable.getClass().getName();
                                    if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                                        Utility.ShowErrorSneakbar(ChangePasswordActivity.this,findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                                    } else {
                                        Utility.ShowErrorSneakbar(ChangePasswordActivity.this,findViewById(android.R.id.content), throwable.getMessage());
                                    }
                                }
                                else if(type==Constants.AUTHENTICATION)
                                {
                                    Utility.ShowGenericToast(ChangePasswordActivity.this,Constants.AUTHENTICATION_ERROR);
                                    SharedPrefrences.clearAll();
                                    SharedClass.clearAll();
                                    startActivity(new Intent(ChangePasswordActivity.this, SplashActivity.class));
                                    finishAffinity();
                                }
                            }
                            catch (Exception exp)
                            {
                            }
                        }
                    });
                }
            }
        });
    }

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }

}

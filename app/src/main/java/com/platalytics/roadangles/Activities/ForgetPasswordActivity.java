package com.platalytics.roadangles.Activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.Models.User;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.Utility;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class ForgetPasswordActivity extends BaseActivity {

    EditText emailEt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(ForgetPasswordActivity.this);
        emailEt = (EditText)findViewById(R.id.emailEt);
        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.submitBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(emailEt.getText().toString().trim().isEmpty())
                {
                    Utility.ShowErrorSneakbar(ForgetPasswordActivity.this,findViewById(android.R.id.content),"Enter Email Address");
                }
                else
                {
                    ServerCalls.forgetPassword(emailEt.getText().toString().trim(), SharedClass.accountType + "", new ResponceCallback() {
                        @Override
                        public void onSuccessResponce(Object obj) {
                            Utility.ShowSuccessDialog(ForgetPasswordActivity.this, "An Email with Code send to your given email address", new ResponceCallback() {
                                @Override
                                public void onSuccessResponce(Object obj) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run()
                                        {
                                            startActivity(new Intent(ForgetPasswordActivity.this,ResetPasswodCodeActivity.class).putExtra("email",emailEt.getText().toString().trim()));
                                        }
                                    });
                                }

                                @Override
                                public void onFailureResponce(Object obj, int type) {
                                    Utility.ShowFailurDialog(ForgetPasswordActivity.this,"Some error occured for email validation",null);
                                }
                            });
                        }

                        @Override
                        public void onFailureResponce(Object obj, int type) {
                            try
                            {
                                if(type== Constants.STRING)
                                {
                                    String message = obj.toString();
                                    Utility.ShowErrorToast(ForgetPasswordActivity.this,message);
                                }
                                else if(type==Constants.THROWABLE)
                                {
                                    Throwable throwable = (Throwable) obj;
                                    String exceptionClassName = throwable.getClass().getName();
                                    if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                                    {
                                        Utility.ShowErrorSneakbar(ForgetPasswordActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                                    }
                                    else {
                                        Utility.ShowErrorSneakbar(ForgetPasswordActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                                    }
                                }
                                else if(type==Constants.AUTHENTICATION)
                                {
                                    Utility.ShowGenericToast(ForgetPasswordActivity.this,Constants.AUTHENTICATION_ERROR);
                                    SharedPrefrences.clearAll();
                                    SharedClass.clearAll();
                                    startActivity(new Intent(ForgetPasswordActivity.this, SplashActivity.class));
                                    finishAffinity();
                                }
                            }
                            catch (Exception exp){}
                        }
                    });
                }
            }
        });
    }

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }

}

package com.platalytics.roadangles.Activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.ornolfr.ratingview.RatingView;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.makeramen.roundedimageview.RoundedImageView;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.Models.TripModel;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.Utility;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class ReviewServiceActivity extends BaseActivity {

    TextView servicePrice,servicename,date,fromlocation,tolocation,name;
    RoundedImageView mapImage;
    RatingView ratingbar;
    EditText reviewEt;
    CircleImageView profilepic;
    TripModel tripModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_service);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(ReviewServiceActivity.this);
        tripModel = (TripModel) getIntent().getSerializableExtra("tripmodel");
        servicePrice = (TextView)findViewById(R.id.servicePrice);
        servicename = (TextView)findViewById(R.id.servicename);
        date = (TextView)findViewById(R.id.date);
        fromlocation = (TextView)findViewById(R.id.fromlocation);
        tolocation = (TextView)findViewById(R.id.tolocation);
        name = (TextView)findViewById(R.id.name);

        profilepic = (CircleImageView)findViewById(R.id.profilepic);
        mapImage = (RoundedImageView)findViewById(R.id.mapImage);
        ratingbar = (RatingView)findViewById(R.id.ratingbar);
        reviewEt = (EditText)findViewById(R.id.reviewEt);

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                setResult(RESULT_OK,intent);
                finish();
            }
        });
        findViewById(R.id.rateBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final float rating = ratingbar.getRating();
                final String review = (reviewEt.getText().toString().trim().isEmpty() ? "" : reviewEt.getText().toString().trim());
                if(review.isEmpty())
                {
                    Utility.ShowYesNoDialog(ReviewServiceActivity.this,"Alert", "Want to proceed with No Review", "Yes", "No", new ResponceCallback() {
                        @Override
                        public void onSuccessResponce(Object obj) {
                            setRating(rating+"",review);
                        }

                        @Override
                        public void onFailureResponce(Object obj, int type) {

                        }
                    });
                }
                else {
                    setRating(rating+"",review);
                }
            }
        });
        setData();
        if(SharedClass.accountType == Constants.VENDOR)
        {
            updateServiceMapUrl();
        }
    }

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }


    private void setData() {
        servicePrice.setText(tripModel.servicePrice+"$");
        servicename.setText(tripModel.serviceName);
        String time = Utility.getFormatedData_MMMM_DD_YYYY_HH_MM_A(tripModel.endTime);
        date.setText(time);
        if(SharedClass.accountType== Constants.END_USER)
        {
            Picasso.with(ReviewServiceActivity.this).load(tripModel.vendorProfilePic).into(profilepic);
            name.setText(tripModel.vendorName);
        }
        else
        {
            Picasso.with(ReviewServiceActivity.this).load(tripModel.userProfilePic).into(profilepic);
            name.setText(tripModel.userName);
        }
        fromlocation.setText(tripModel.vendorAddress);
        tolocation.setText(tripModel.userAddress);
        String mapImg = Utility.makeGoogleImageURL(tripModel);
        Picasso.with(ReviewServiceActivity.this).load((tripModel.mapImage.equals("") || tripModel.mapImage==null) ? mapImg : tripModel.mapImage).placeholder(R.drawable.image_placeholder).into(mapImage);

    }

    private void setRating(String rating,String review)
    {
        String byId = "";
        String forId = "";
        if (SharedClass.accountType == Constants.END_USER) {
            byId = SharedClass.user.getId();
            forId = tripModel.vendorId;
        } else {
            byId = SharedClass.user.getId();
            forId = tripModel.userId;
        }
        HashMap<String, JsonObject> information = new HashMap<>();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("trip_id", tripModel.id);
        jsonObject.addProperty("by_id", byId);
        jsonObject.addProperty("for_id", forId);
        jsonObject.addProperty("type", SharedClass.accountType + "");
        jsonObject.addProperty("auth_key", SharedClass.user.getAuthKey());
        jsonObject.addProperty("date", System.currentTimeMillis() + "");
        jsonObject.addProperty("rating", rating);
        jsonObject.addProperty("Succe", review);
        information.put("rate", jsonObject);

        ServerCalls.setRating(information, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                Utility.ShowSuccessDialog(ReviewServiceActivity.this,"Review Submitted", null);
                findViewById(R.id.ratingView).setVisibility(View.GONE);
                findViewById(R.id.rateBtn).setVisibility(View.GONE);
                findViewById(R.id.notice).setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try {
                    if (type == Constants.STRING) {
                        String message = obj.toString();
                        Utility.ShowErrorToast(ReviewServiceActivity.this,message);
                    } else if (type == Constants.THROWABLE) {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                            Utility.ShowErrorSneakbar(ReviewServiceActivity.this,findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                        } else {
                            Utility.ShowErrorSneakbar(ReviewServiceActivity.this,findViewById(android.R.id.content), throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(ReviewServiceActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(ReviewServiceActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                } catch (Exception exp) {
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK,intent);
        super.onBackPressed();
    }

    private void updateServiceMapUrl()
    {
        ServerCalls.updateService(tripModel.id, tripModel.mapImage, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                Utility.ShowGenericToast(ReviewServiceActivity.this,"Url Updated");
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    if(type== Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowErrorToast(ReviewServiceActivity.this,message);
                    }
                    else if(type==Constants.THROWABLE)
                    {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                        {
                            Utility.ShowErrorSneakbar(ReviewServiceActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                        }
                        else {
                            Utility.ShowErrorSneakbar(ReviewServiceActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(ReviewServiceActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(ReviewServiceActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp){}
            }
        });
    }
}

package com.platalytics.roadangles.Activities;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.ornolfr.ratingview.RatingView;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Models.TripModel;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.Utility;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ServiceDetailActivity extends BaseActivity {

    ImageView mapImage,serviceIcon;
    TextView dateTime,servicename,price,vendorAddress,userAddress,username;
    CircleImageView profilepic;
    RatingView ratingbar;
    TripModel tripModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(ServiceDetailActivity.this);
        setContentView(R.layout.activity_service_detail);
        initObject();
        tripModel = (TripModel) getIntent().getSerializableExtra("trip");
        setData();
    }

    private void initObject()
    {
        mapImage = (ImageView)findViewById(R.id.mapImage);
        serviceIcon = (ImageView)findViewById(R.id.serviceIcon);
        dateTime = (TextView)findViewById(R.id.dateTime);
        servicename = (TextView)findViewById(R.id.servicename);
        price = (TextView)findViewById(R.id.price);
        vendorAddress = (TextView)findViewById(R.id.vendorAddress);
        userAddress = (TextView)findViewById(R.id.userAddress);
        username = (TextView)findViewById(R.id.username);
        profilepic = (CircleImageView)findViewById(R.id.profilepic);
        ratingbar = (RatingView)findViewById(R.id.ratingbar);

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }


    private void setData() {
        String mapImg = Utility.makeGoogleImageURL(tripModel);
        Picasso.with(ServiceDetailActivity.this).load((tripModel.mapImage==null || tripModel.mapImage.equals("")) ? mapImg : tripModel.mapImage).into(mapImage);
        String time = Utility.getFormatedData_MMMM_DD_YYYY_HH_MM_A(tripModel.endTime);
        dateTime.setText(time);
        price.setText(tripModel.servicePrice+"$");
        Picasso.with(ServiceDetailActivity.this).load(tripModel.serviceIcon).into(serviceIcon);
        servicename.setText(tripModel.serviceName);
        vendorAddress.setText(tripModel.vendorAddress);
        userAddress.setText(tripModel.userAddress);
        if(SharedClass.accountType== Constants.END_USER)
        {
            Picasso.with(ServiceDetailActivity.this).load(tripModel.vendorProfilePic).into(profilepic);

            if(tripModel.vendorRating==null || tripModel.vendorRating.equals(""))
            {
                username.setText("Your Service by "+tripModel.vendorName);
                ratingbar.setVisibility(View.GONE);
            }
            else
            {
                username.setText("You rate "+tripModel.vendorName);
                ratingbar.setVisibility(View.VISIBLE);
                ratingbar.setRating(Float.parseFloat(tripModel.vendorRating));
            }
        }
        else
        {
            Picasso.with(ServiceDetailActivity.this).load(tripModel.userProfilePic).into(profilepic);
            if(tripModel.userRating==null || tripModel.userRating.equals(""))
            {
                username.setText("Your Service for "+tripModel.userName);
                ratingbar.setVisibility(View.GONE);
            }
            else
            {
                username.setText("You rate "+tripModel.userName);
                ratingbar.setVisibility(View.VISIBLE);
                ratingbar.setRating(Float.parseFloat(tripModel.userRating));
            }
        }
    }
}

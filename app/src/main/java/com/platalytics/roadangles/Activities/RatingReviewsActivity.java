package com.platalytics.roadangles.Activities;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.platalytics.roadangles.Activities.User.ServicesActivity;
import com.platalytics.roadangles.Adapters.RatingReviewAdapter;
import com.platalytics.roadangles.Adapters.ServicesAdapter;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Models.ReviewRatingModel;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.Utilities.SharedClass;

import java.util.ArrayList;
import java.util.List;

public class RatingReviewsActivity extends BaseActivity {

    RecyclerView reviewsRV;
    RatingReviewAdapter ratingReviewAdapter;
    List<ReviewRatingModel> allReviews = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_reviews);
        mNetworkReceiver.setContext(RatingReviewsActivity.this);
        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        reviewsRV = (RecyclerView)findViewById(R.id.reviewsRV);
        setAdapter();
    }

    private void setAdapter() {
        LinearLayoutManager layout = new LinearLayoutManager(RatingReviewsActivity.this, LinearLayoutManager.VERTICAL, false);
        reviewsRV.setLayoutManager(layout);
        ratingReviewAdapter = new RatingReviewAdapter(RatingReviewsActivity.this, allReviews);
        reviewsRV.setAdapter(ratingReviewAdapter);
    }

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }
}

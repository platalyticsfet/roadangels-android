package com.platalytics.roadangles.Activities;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.platalytics.roadangles.Activities.User.HomeActivity;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.Models.User;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class RegisterActivity extends BaseActivity {

    EditText firstNameEt, lastNameEt, emailEt, mobileEt;
    TextInputEditText passwordEt;
    LoginButton login_button;
    private CallbackManager callbackManager;
    List<String> permissionNeeds;
    private String firstName, lastName, email, mobile, password;
    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 1001;
    private String TAG = "RegisterActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(RegisterActivity.this);
        callbackManager = CallbackManager.Factory.create();
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        objectsInit();
        findViewById(R.id.facebookBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login_button.performClick();
            }
        });
        findViewById(R.id.googleBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.nextBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstName = firstNameEt.getText().toString().trim();
                lastName = lastNameEt.getText().toString().trim();
                email = emailEt.getText().toString().trim();
                mobile = mobileEt.getText().toString().trim();
                password = passwordEt.getText().toString().trim();
                if (firstName.isEmpty()) {
                    Utility.ShowErrorSneakbar(RegisterActivity.this, findViewById(android.R.id.content), "First Name is empty");
                } else if (lastName.isEmpty()) {
                    Utility.ShowErrorSneakbar(RegisterActivity.this, findViewById(android.R.id.content), "Last Name is empty");
                } else if (email.isEmpty()) {
                    Utility.ShowErrorSneakbar(RegisterActivity.this, findViewById(android.R.id.content), "Email is empty");
                } else if (mobile.isEmpty()) {
                    Utility.ShowErrorSneakbar(RegisterActivity.this, findViewById(android.R.id.content), "Mobile Number is empty");
                } else if (password.isEmpty()) {
                    Utility.ShowErrorSneakbar(RegisterActivity.this, findViewById(android.R.id.content), "Password is empty");
                } else if(!mobile.contains("+")) {
                    Utility.ShowErrorSneakbar(RegisterActivity.this, findViewById(android.R.id.content), "Invalid format of mobile number. Replace first 0 with +92");
                }
                else {
                    Utility.ShowYesNoDialog(RegisterActivity.this, mobile, "Is this correct?", "Yes", "No, let me Change it", new ResponceCallback() {
                        @Override
                        public void onSuccessResponce(Object obj) {
                            HashMap<String, JsonObject> detail = new HashMap<>();
                            JsonObject jsonObject = new JsonObject();
                            jsonObject.addProperty("firstname", firstName);
                            jsonObject.addProperty("lastname", lastName);
                            jsonObject.addProperty("email", email);
                            jsonObject.addProperty("phone", mobile);
                            jsonObject.addProperty("password", password);
                            if (SharedClass.accountType == Constants.END_USER) {
                                jsonObject.addProperty("profilepic", "https://i.imgur.com/NZNb3HG.png");
                            } else {
                                jsonObject.addProperty("profilepic", "https://i.imgur.com/oTqxmAK.png");
                            }
                            jsonObject.addProperty("type", SharedClass.accountType + "");
                            detail.put("details", jsonObject);
                            ServerCalls.Register(detail, new ResponceCallback() {
                                @Override
                                public void onSuccessResponce(Object obj) {
                                    verifyPhone(mobile);
                                    SharedPrefrences.savePassword(password);
                                }

                                @Override
                                public void onFailureResponce(Object obj, int type) {
                                    try {
                                        if (type == Constants.STRING)
                                        {
                                            String message = obj.toString();
                                            Utility.ShowErrorSneakbar(RegisterActivity.this,findViewById(android.R.id.content),message);
                                        }
                                        else if (type == Constants.THROWABLE)
                                        {
                                            Throwable throwable = (Throwable) obj;
                                            String exceptionClassName = throwable.getClass().getName();
                                            if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                                                Utility.ShowErrorSneakbar(RegisterActivity.this, findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                                            } else {
                                                Utility.ShowErrorSneakbar(RegisterActivity.this, findViewById(android.R.id.content), throwable.getMessage());
                                            }
                                        }
                                        else if(type==Constants.AUTHENTICATION)
                                        {
                                            Utility.ShowGenericToast(RegisterActivity.this,Constants.AUTHENTICATION_ERROR);
                                            SharedPrefrences.clearAll();
                                            SharedClass.clearAll();
                                            startActivity(new Intent(RegisterActivity.this, SplashActivity.class));
                                            finishAffinity();
                                        }
                                    } catch (Exception exp) {
                                    }
                                }
                            });
                        }

                        @Override
                        public void onFailureResponce(Object obj, int object) {

                        }
                    });
                }
            }
        });
        FbCallBacks();
    }

    private void objectsInit() {
        login_button = (LoginButton) findViewById(R.id.login_button);
        login_button.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
        firstNameEt = (EditText) findViewById(R.id.firstNameEt);
        lastNameEt = (EditText) findViewById(R.id.lastNameEt);
        emailEt = (EditText) findViewById(R.id.emailEt);
        mobileEt = (EditText) findViewById(R.id.mobileEt);
        passwordEt = (TextInputEditText) findViewById(R.id.passwordEt);
    }

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }

    private void FbCallBacks() {
        permissionNeeds = Arrays.asList("user_photos", "email", "user_birthday", "public_profile", "AccessToken");
        login_button.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.e("Status", "onSuccess");
                        System.out.println("onSuccess");
                        String accessToken = loginResult.getAccessToken().getToken();
                        Log.i("accessToken", accessToken);
                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.i("LoginActivity", response.toString());
                                        try {
                                            password = object.getString("id");
                                            try {
                                                URL profile_pic = new URL("http://graph.facebook.com/" + password + "/picture?type=large");
                                                Log.i("profile_pic", profile_pic + "");
                                            } catch (MalformedURLException e) {
                                                e.printStackTrace();
                                            }
                                            if (object.has("email")) {
                                                email = object.getString("email");
                                            }
                                            else
                                            {
                                                email = "";
                                            }
                                            if(object.has("name")) {
                                                String name = object.getString("name");
                                                if(name.contains(" "))
                                                {
                                                    firstName = name.split(" ")[0];
                                                    lastName = name.split(" ")[1];
                                                }
                                                else
                                                {
                                                    firstName = name;
                                                    lastName = "";
                                                }
                                            }
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    firstNameEt.setText(firstName);
                                                    lastNameEt.setText(lastName);
                                                    emailEt.setText(email);
                                                    passwordEt.setText(password);
                                                    passwordEt.setEnabled(false);
                                                }
                                            });

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender, birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.e("Status", "onCancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.e("Status", "onError");
                        Log.v("LoginActivity", exception.getMessage().toString());
                        Utility.ShowFailurDialog(RegisterActivity.this, exception.getMessage().toString(), null);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
        callbackManager.onActivityResult(requestCode, responseCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            firstName = account.getDisplayName();
            if (firstName.contains(" ")) {
                String[] splits = firstName.split(" ");
                firstName = splits[0];
                lastName = splits[1];
            } else {
                lastName = "";
            }
            email = account.getEmail();
            password = account.getId();
            firstNameEt.setText(firstName);
            lastNameEt.setText(lastName);
            emailEt.setText(email);
            passwordEt.setText(password);
            passwordEt.setEnabled(false);
        } catch (ApiException e) {
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }


    private void verifyPhone(String phone) {
        ServerCalls.verifyNumber(SharedClass.accountType + "", phone, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                startActivity(new Intent(RegisterActivity.this, CodeVerificationActivity.class).putExtra("phone",mobile));
                finishAffinity();
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try
                {
                    if (type == Constants.STRING)
                    {
                        String message = obj.toString();
                        Utility.ShowGenericToast(RegisterActivity.this,"An error occured while sending verification code . Please login to get code");
                        finish();
                    }
                    else if (type == Constants.THROWABLE)
                    {
                        /*Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                            Utility.ShowErrorSneakbar(RegisterActivity.this, findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                        } else {
                            Utility.ShowErrorSneakbar(RegisterActivity.this, findViewById(android.R.id.content), throwable.getMessage());
                        }*/
                        Utility.ShowGenericToast(RegisterActivity.this,"An error occured while sending verification code . Please login to get code");
                        finish();
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(RegisterActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(RegisterActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                } catch (Exception exp) {
                }
            }
        });
    }

}

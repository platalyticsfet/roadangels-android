package com.platalytics.roadangles.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.makeramen.roundedimageview.RoundedImageView;
import com.platalytics.roadangles.Activities.User.ServicesActivity;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.ServerUtils.Urls;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.ImageUtils;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.Utility;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class ProfileActivity extends BaseActivity implements View.OnClickListener {

    ImageView backBtn,editBtn,imageBtn,coverPic;
    TextView title,editCoverBtn;
    CircleImageView profilePic;
    EditText fnameEt,lnameEt,emailEt,phoneEt;
    Button updateBtn;
    File file = null;
    boolean isCameraSelect;
    private Uri cameraImageUri;
    private static final int REQUEST_IMAGE_CAPTURE = 1001;
    private static final int REQUEST_IMAGE_GALLERY = 1002;
    final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};
    int chanPicType;
    private final int COVER_CHANGE = 1;
    private final int PROFILE_PIC_CHANGE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(ProfileActivity.this);
        initObjects();
        setData();
    }

    private void initObjects() {
        isCameraSelect = false;
        backBtn = (ImageView)findViewById(R.id.backBtn);
        editBtn = (ImageView)findViewById(R.id.editBtn);
        imageBtn = (ImageView)findViewById(R.id.imageBtn);
        coverPic = (ImageView)findViewById(R.id.coverPic);
        title = (TextView)findViewById(R.id.title);
        editCoverBtn = (TextView)findViewById(R.id.editCoverBtn);
        profilePic = (CircleImageView)findViewById(R.id.profilePic);
        fnameEt = (EditText)findViewById(R.id.fnameEt);
        lnameEt = (EditText)findViewById(R.id.lnameEt);
        emailEt = (EditText)findViewById(R.id.emailEt);
        phoneEt = (EditText)findViewById(R.id.phoneEt);
        updateBtn = (Button)findViewById(R.id.updateBtn);

        backBtn.setOnClickListener(this);
        editBtn.setOnClickListener(this);
        imageBtn.setOnClickListener(this);
        updateBtn.setOnClickListener(this);
        editCoverBtn.setOnClickListener(this);
    }

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.backBtn:
                finish();
                break;
            case R.id.editBtn:
                imageBtn.setVisibility(View.VISIBLE);
                updateBtn.setVisibility(View.VISIBLE);
                editCoverBtn.setVisibility(View.VISIBLE);
                editBtn.setVisibility(View.GONE);
                title.setText("Edit Profile");
                fnameEt.setEnabled(true);
                lnameEt.setEnabled(true);
                phoneEt.setEnabled(true);
                break;
            case R.id.imageBtn:
                chanPicType = PROFILE_PIC_CHANGE;
                ImageSourceChooser();
                break;
            case R.id.updateBtn:
                imageBtn.setVisibility(View.GONE);
                updateBtn.setVisibility(View.GONE);
                editCoverBtn.setVisibility(View.GONE);
                editBtn.setVisibility(View.VISIBLE);
                title.setText("Profile");
                fnameEt.setEnabled(false);
                lnameEt.setEnabled(false);
                phoneEt.setEnabled(false);
                if(file!=null)
                {
                    uploadImage();
                }
                else
                {
                    updateData(SharedClass.user.getProfilePicture());
                }
                break;
            case R.id.editCoverBtn:
                chanPicType = COVER_CHANGE;
                ImageSourceChooser();
                break;
        }
    }

    private void setData()
    {
        Picasso.with(ProfileActivity.this).load(SharedClass.user.getProfilePicture()).placeholder(R.drawable.user_placeholder).into(profilePic);
        fnameEt.setText(SharedClass.user.getFirstName());
        lnameEt.setText(SharedClass.user.getLastName());
        emailEt.setText(SharedClass.user.getEmail());
        phoneEt.setText(SharedClass.user.getPhoneNumber()==null ? "N/A" : SharedClass.user.getPhoneNumber());
    }

    private void ImageSourceChooser()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (item==0) {
                    isCameraSelect = true;
                    PickImage();
                } else if (item==1) {
                    isCameraSelect = false;
                    PickImage();
                } else{
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void PickImage()
    {
        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted()
            {
                if(isCameraSelect)
                {
                    OpenCamera();
                }
                else
                {
                    OpenGallery();
                }
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Utility.ShowGenericToast(ProfileActivity.this,"Permission Denied\n" + deniedPermissions.toString());
            }
        };
        TedPermission.with(ProfileActivity.this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE)
                .check();
    }

    private void OpenCamera()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File cameraImageOutputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Utility.createCameraImageFileName());
        cameraImageUri = Uri.fromFile(cameraImageOutputFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri);
        startActivityForResult(Intent.createChooser(intent, "Image Source"),REQUEST_IMAGE_CAPTURE);
    }

    private void OpenGallery()
    {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , REQUEST_IMAGE_GALLERY);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case REQUEST_IMAGE_GALLERY:
                if(resultCode == RESULT_OK){
                    try
                    {
                        Uri uri = imageReturnedIntent.getData();
                        file = new File(ImageUtils.compressImage(Utility.getPathFromUri(ProfileActivity.this,uri)));
                        if(chanPicType == COVER_CHANGE)
                        {
                            Picasso.with(ProfileActivity.this).load(uri).into(coverPic);
                        }
                        else
                        {
                            Picasso.with(ProfileActivity.this).load(uri).into(profilePic);
                        }
                    }
                    catch (Exception exp)
                    {

                    }
                }

                break;
            case REQUEST_IMAGE_CAPTURE:
                if(resultCode == RESULT_OK){
                    try
                    {
                        Uri uri = cameraImageUri;
                        Picasso.with(ProfileActivity.this).load(uri).into(profilePic);
                        file = new File(ImageUtils.compressImage(Utility.getPathFromUri(ProfileActivity.this,uri)));
                    }
                    catch (Exception exp)
                    {

                    }
                }
                break;
        }
    }

    private void updateData(final String image)
    {
        final String fname = fnameEt.getText().toString().trim();
        final String lname = lnameEt.getText().toString().trim();
        final String phone = phoneEt.getText().toString().trim();
        if(fname.isEmpty())
        {
            Utility.ShowErrorSneakbar(ProfileActivity.this,findViewById(android.R.id.content),"Enter First Name");
        }
        else if(lname.isEmpty())
        {
            Utility.ShowErrorSneakbar(ProfileActivity.this,findViewById(android.R.id.content),"Enter Last Name");
        }
        else if(phone.isEmpty())
        {
            Utility.ShowErrorSneakbar(ProfileActivity.this,findViewById(android.R.id.content),"Enter Phone Number");
        }
        else
        {
            Utility.ShowProgressDialog(ProfileActivity.this,getString(R.string.please_wait));
            ServerCalls.editProfile(fname, lname, phone, image, new ResponceCallback() {
                @Override
                public void onSuccessResponce(Object obj) {
                    SharedClass.user.setFirstName(fname);
                    SharedClass.user.setLastName(lname);
                    SharedClass.user.setProfilePicture(image);
                    SharedClass.user.setPhoneNumber(phone);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utility.ShowSuccessDialog(ProfileActivity.this,"Profile Updated Successfully", new ResponceCallback() {
                                @Override
                                public void onSuccessResponce(Object obj) {
                                    finish();
                                }

                                @Override
                                public void onFailureResponce(Object obj, int type) {

                                }
                            });
                        }
                    });
                }

                @Override
                public void onFailureResponce(Object obj, int type) {
                    try {
                        if (type == Constants.STRING) {
                            String message = obj.toString();
                            Utility.ShowErrorToast(ProfileActivity.this,message);
                        } else if (type == Constants.THROWABLE) {
                            Throwable throwable = (Throwable) obj;
                            String exceptionClassName = throwable.getClass().getName();
                            if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                                Utility.ShowErrorSneakbar(ProfileActivity.this,findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                            } else {
                                Utility.ShowErrorSneakbar(ProfileActivity.this,findViewById(android.R.id.content), throwable.getMessage());
                            }
                        }
                        else if(type==Constants.AUTHENTICATION)
                        {
                            Utility.ShowGenericToast(ProfileActivity.this,Constants.AUTHENTICATION_ERROR);
                            SharedPrefrences.clearAll();
                            SharedClass.clearAll();
                            startActivity(new Intent(ProfileActivity.this, SplashActivity.class));
                            finishAffinity();
                        }
                    }
                    catch (Exception exp)
                    {
                    }
                }
            });
        }
    }

    private void uploadImage()
    {
        findViewById(R.id.uploadingView).setVisibility(View.VISIBLE);
        ServerCalls.uploadImage(SharedClass.user.getId(), file, new ResponceCallback() {
            @Override
            public void onSuccessResponce(Object obj) {
                findViewById(R.id.uploadingView).setVisibility(View.GONE);
                JsonObject uploadingResponse = (JsonObject) obj;
                String imagePath = uploadingResponse.get("image").getAsString();
                updateData(Urls.CLOUD_BASE_URL+imagePath);
            }

            @Override
            public void onFailureResponce(Object obj, int type) {
                try {
                    findViewById(R.id.uploadingView).setVisibility(View.GONE);
                    if (type == Constants.STRING) {
                        String message = obj.toString();
                        Utility.ShowErrorToast(ProfileActivity.this,message);
                    }
                    else if (type == Constants.THROWABLE)
                    {
                        Throwable throwable = (Throwable) obj;
                        String exceptionClassName = throwable.getClass().getName();
                        if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                            Utility.ShowErrorSneakbar(ProfileActivity.this,findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                        } else {
                            Utility.ShowErrorSneakbar(ProfileActivity.this,findViewById(android.R.id.content), throwable.getMessage());
                        }
                    }
                    else if(type==Constants.AUTHENTICATION)
                    {
                        Utility.ShowGenericToast(ProfileActivity.this,Constants.AUTHENTICATION_ERROR);
                        SharedPrefrences.clearAll();
                        SharedClass.clearAll();
                        startActivity(new Intent(ProfileActivity.this, SplashActivity.class));
                        finishAffinity();
                    }
                }
                catch (Exception exp)
                {
                }
            }
        });
    }

}

package com.platalytics.roadangles.Activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.Models.User;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.Utility;

public class CodeVerificationActivity extends BaseActivity {

    private String phoneNumber;
    EditText codeEt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_verification);
        codeEt = findViewById(R.id.codeEt);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(CodeVerificationActivity.this);
        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        phoneNumber = getIntent().getStringExtra("phone");
        findViewById(R.id.submitBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(codeEt.getText().toString().trim().isEmpty())
                {
                    Utility.ShowErrorSneakbar(CodeVerificationActivity.this, findViewById(android.R.id.content), "Code is empty");
                }
                else
                {
                    ServerCalls.verifyCode(SharedClass.accountType + "", phoneNumber, codeEt.getText().toString().trim(), new ResponceCallback() {
                        @Override
                        public void onSuccessResponce(Object obj) {
                            JsonObject registerResponse = (JsonObject)obj;
                            SharedClass.user = new Gson().fromJson(registerResponse, User.class);
                            SharedPrefrences.loginAs();
                            SharedPrefrences.saveEmailAddress(SharedClass.user.getEmail());
                            if (SharedClass.accountType == Constants.END_USER) {
                                startActivity(new Intent(CodeVerificationActivity.this, com.platalytics.roadangles.Activities.User.HomeActivity.class));
                                finishAffinity();
                            } else {
                                startActivity(new Intent(CodeVerificationActivity.this, com.platalytics.roadangles.Activities.Vendor.HomeActivity.class));
                                finishAffinity();
                            }
                        }

                        @Override
                        public void onFailureResponce(Object obj, int type) {
                            try {
                                if (type == Constants.STRING)
                                {
                                    String message = obj.toString();
                                    Utility.ShowErrorSneakbar(CodeVerificationActivity.this,findViewById(android.R.id.content),message);
                                }
                                else if (type == Constants.THROWABLE)
                                {
                                    Throwable throwable = (Throwable) obj;
                                    String exceptionClassName = throwable.getClass().getName();
                                    if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                                        Utility.ShowErrorSneakbar(CodeVerificationActivity.this, findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                                    } else {
                                        Utility.ShowErrorSneakbar(CodeVerificationActivity.this, findViewById(android.R.id.content), throwable.getMessage());
                                    }
                                }
                                else if(type==Constants.AUTHENTICATION)
                                {
                                    Utility.ShowGenericToast(CodeVerificationActivity.this,Constants.AUTHENTICATION_ERROR);
                                    SharedPrefrences.clearAll();
                                    SharedClass.clearAll();
                                    startActivity(new Intent(CodeVerificationActivity.this, SplashActivity.class));
                                    finishAffinity();
                                }
                            } catch (Exception exp) {
                            }
                        }
                    });
                }
            }
        });
    }

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }
}

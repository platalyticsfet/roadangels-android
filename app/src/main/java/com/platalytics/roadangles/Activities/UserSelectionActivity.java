package com.platalytics.roadangles.Activities;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.Utility;

public class UserSelectionActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(UserSelectionActivity.this);
        setContentView(R.layout.activity_user_selection);
        SharedClass.accountType = Constants.END_USER;
        findViewById(R.id.userBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedClass.accountType = Constants.END_USER;
                ((CardView)findViewById(R.id.userBtn)).setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
                ((CardView)findViewById(R.id.vendorBtn)).setCardBackgroundColor(getResources().getColor(R.color.white));
                ((TextView)findViewById(R.id.usertxt)).setTextColor(getResources().getColor(R.color.white));
                ((TextView)findViewById(R.id.vendortxt)).setTextColor(getResources().getColor(R.color.black));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((ImageView)findViewById(R.id.userImg)).setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                    ((ImageView)findViewById(R.id.vendorImg)).setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
                }
                else
                {
                    ((ImageView)findViewById(R.id.vendorImg)).setColorFilter(getResources().getColor(R.color.black));
                    ((ImageView)findViewById(R.id.userImg)).setColorFilter(getResources().getColor(R.color.white));
                }
            }
        });
        findViewById(R.id.vendorBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedClass.accountType = Constants.VENDOR;
                ((CardView)findViewById(R.id.vendorBtn)).setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
                ((CardView)findViewById(R.id.userBtn)).setCardBackgroundColor(getResources().getColor(R.color.white));
                ((TextView)findViewById(R.id.vendortxt)).setTextColor(getResources().getColor(R.color.white));
                ((TextView)findViewById(R.id.usertxt)).setTextColor(getResources().getColor(R.color.black));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((ImageView)findViewById(R.id.vendorImg)).setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
                    ((ImageView)findViewById(R.id.userImg)).setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.black)));
                }
                else
                {
                    ((ImageView)findViewById(R.id.vendorImg)).setColorFilter(getResources().getColor(R.color.white));
                    ((ImageView)findViewById(R.id.userImg)).setColorFilter(getResources().getColor(R.color.black));
                }
            }
        });
        findViewById(R.id.loginBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserSelectionActivity.this,LoginActivity.class));
            }
        });
        findViewById(R.id.registerBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserSelectionActivity.this,RegisterActivity.class));
            }
        });
    }

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }
}

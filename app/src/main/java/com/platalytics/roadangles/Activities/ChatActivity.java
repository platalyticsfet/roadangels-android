package com.platalytics.roadangles.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.platalytics.roadangles.Activities.User.HomeActivity;
import com.platalytics.roadangles.Adapters.ChatAdapter;
import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Bus.Events;
import com.platalytics.roadangles.Bus.GlobalBus;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.Interfaces.SocketMessageListerner;
import com.platalytics.roadangles.Models.ChatModel;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.RoadAngelsApplication;
import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.Utility;
import com.squareup.otto.Subscribe;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ChatActivity extends BaseActivity implements ResponceCallback {

    ChatAdapter chatAdapter;
    RecyclerView chatRv;
    private ArrayList<ChatModel> chats = new ArrayList<>();
    private HashMap<String,ChatModel> chatMap = new HashMap<>();
    EditText messageText;
    String otherUserId;
    String otherUserName;
    String tripId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        if(getIntent().hasExtra("otherUserId"))
        {
            otherUserId = getIntent().getStringExtra("otherUserId");
            otherUserName = getIntent().getStringExtra("otherUserName");
        }
        tripId = getIntent().getStringExtra("tripId");
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(ChatActivity.this);
        chatRv = (RecyclerView)findViewById(R.id.chatRv);
        messageText = (EditText)findViewById(R.id.messageText);
        if(SharedClass.accountType== Constants.END_USER)
        {
            ((TextView)findViewById(R.id.title)).setText(otherUserName);
            messageText.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        else
        {
            ((TextView)findViewById(R.id.title)).setText(otherUserName);
            messageText.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.sendBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!messageText.getText().toString().isEmpty())
                {
                    String time = System.currentTimeMillis()+"";
                    ChatModel chat = new ChatModel(time,messageText.getText().toString().trim(),time,SharedClass.user.getFirstName()+" "+SharedClass.user.getLastName(),SharedClass.user.getId(),SharedClass.user.getProfilePicture(),tripId);
                    chatMap.put(time,chat);
                    chats.add(chat);
                    chatAdapter.notifyDataSetChanged();
                    chatRv.scrollToPosition(chats.size()-1);
                    sendNewChatMessage(SharedClass.user.getId(),otherUserId,SharedClass.user.getAuthKey(),messageText.getText().toString().trim(),time);
                    messageText.setText("");
                    Utility.hideKeyPad(ChatActivity.this);
                }
            }
        });
        setAdapter();
        getPreviousChatMessages();
    }

    public void sendNewChatMessage(String senderId,String recieverId,String authKey, String message,String date) {
        Events.SendNewMessage sendNewMessage =
                new Events.SendNewMessage(senderId,recieverId,authKey,message,date,tripId);
        GlobalBus.getBus().post(sendNewMessage);
    }

    @Subscribe
    public void getMessage(Events.GetNewMessage getNewMessage)
    {
        if(getNewMessage.getChatModel().from.id.equals(otherUserId) && !chatMap.containsKey(getNewMessage.getChatModel().id))// && !chats.get(chats.size()-1).message.equals(getNewMessage.getChatModel().message))
        {
            chatMap.put(getNewMessage.getChatModel().id,getNewMessage.getChatModel());
            chats.add(getNewMessage.getChatModel());
            try
            {
                chatAdapter.notifyItemInserted(chats.size()-1);
                chatRv.scrollToPosition(chats.size()-1);
            }
            catch (Exception exp)
            {
                Toast.makeText(this, "Error Updating Chat", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setAdapter()
    {
        LinearLayoutManager layout = new LinearLayoutManager(ChatActivity.this, LinearLayoutManager.VERTICAL, false);
        //layout.setReverseLayout(true);
        chatRv.setLayoutManager(layout);
        chatAdapter = new ChatAdapter(ChatActivity.this,chats);
        chatRv.setAdapter(chatAdapter);
    }

    public void dialog(boolean value){
        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()== View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }

    private void getPreviousChatMessages() {
        //Utility.ShowProgressDialog(ChatActivity.this,getResources().getString(R.string.please_wait));
        ServerCalls.getChatMessages(tripId,ChatActivity.this);
    }

    @Override
    public void onSuccessResponce(Object obj) {
        try
        {
            findViewById(R.id.progress_bar).setVisibility(View.GONE);
            JsonObject response = (JsonObject)obj;
            JsonArray chatResponse = response.getAsJsonArray("array");
            for(int i=0;i<chatResponse.size();i++)
            {
                JsonObject json = chatResponse.get(i).getAsJsonObject();
                ChatModel chat = new Gson().fromJson(json,ChatModel.class);
                chats.add(chat);
                chatMap.put(chat.id,chat);
            }
            if(chats.size()>0)
            {
                chatAdapter.notifyDataSetChanged();
            }
            chatRv.scrollToPosition(chats.size()-1);
        }
        catch (Exception exp){
            exp.printStackTrace();
        }
    }

    @Override
    public void onFailureResponce(Object obj, int type) {
        try
        {
            findViewById(R.id.progress_bar).setVisibility(View.GONE);
            if(type== Constants.STRING)
            {
                String message = obj.toString();
                Utility.ShowErrorToast(ChatActivity.this,message);
            }
            else if(type==Constants.THROWABLE)
            {
                Throwable throwable = (Throwable) obj;
                String exceptionClassName = throwable.getClass().getName();
                if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                {
                    Utility.ShowErrorSneakbar(ChatActivity.this,findViewById(android.R.id.content),"Internet/Socket Connection lost. Please try again");
                }
                else
                {
                    Utility.ShowErrorSneakbar(ChatActivity.this,findViewById(android.R.id.content),throwable.getMessage());
                }
            }
            else if(type==Constants.AUTHENTICATION)
            {
                Utility.ShowGenericToast(ChatActivity.this,Constants.AUTHENTICATION_ERROR);
                SharedPrefrences.clearAll();
                SharedClass.clearAll();
                startActivity(new Intent(ChatActivity.this, SplashActivity.class));
                finishAffinity();
            }
        }
        catch (Exception exp){}
    }
}

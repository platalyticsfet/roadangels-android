package com.platalytics.roadangles.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.platalytics.roadangles.BaseActivity;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.R;
import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.Utility;

public class PhoneVerificationActivity extends BaseActivity {

    EditText phoneEt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification);
        phoneEt = findViewById(R.id.phoneEt);
        registerNetworkBroadcast();
        mNetworkReceiver.setContext(PhoneVerificationActivity.this);
        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.submitBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(phoneEt.getText().toString().trim().isEmpty())
                {
                    Utility.ShowErrorSneakbar(PhoneVerificationActivity.this, findViewById(android.R.id.content), "Number is empty");
                } else if(!phoneEt.getText().toString().trim().contains("+")) {
                    Utility.ShowErrorSneakbar(PhoneVerificationActivity.this, findViewById(android.R.id.content), "Invalid format of mobile number. Replace first 0 with +92");
                }
                else
                {
                    Utility.ShowYesNoDialog(PhoneVerificationActivity.this, phoneEt.getText().toString().trim(), "Is this correct?", "Yes", "No, let me Change it", new ResponceCallback() {
                        @Override
                        public void onSuccessResponce(Object obj) {
                            ServerCalls.verifyNumber(SharedClass.accountType + "",  phoneEt.getText().toString().trim(), new ResponceCallback() {
                                @Override
                                public void onSuccessResponce(Object obj) {
                                    startActivity(new Intent(PhoneVerificationActivity.this, CodeVerificationActivity.class).putExtra("phone",phoneEt.getText().toString().trim()));
                                    finishAffinity();
                                }

                                @Override
                                public void onFailureResponce(Object obj, int type) {
                                    try {
                                        if (type == Constants.STRING)
                                        {
                                            String message = obj.toString();
                                            Utility.ShowErrorSneakbar(PhoneVerificationActivity.this,findViewById(android.R.id.content),message);
                                        }
                                        else if (type == Constants.THROWABLE)
                                        {
                                            Throwable throwable = (Throwable) obj;
                                            String exceptionClassName = throwable.getClass().getName();
                                            if (exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION)) {
                                                Utility.ShowErrorSneakbar(PhoneVerificationActivity.this, findViewById(android.R.id.content), "Internet/Socket Connection lost. Please try again");
                                            } else {
                                                Utility.ShowErrorSneakbar(PhoneVerificationActivity.this, findViewById(android.R.id.content), throwable.getMessage());
                                            }
                                        }
                                        else if(type==Constants.AUTHENTICATION)
                                        {
                                            Utility.ShowGenericToast(PhoneVerificationActivity.this,Constants.AUTHENTICATION_ERROR);
                                            SharedPrefrences.clearAll();
                                            SharedClass.clearAll();
                                            startActivity(new Intent(PhoneVerificationActivity.this, SplashActivity.class));
                                            finishAffinity();
                                        }
                                    } catch (Exception exp) {
                                    }
                                }
                            });
                        }

                        @Override
                        public void onFailureResponce(Object obj, int type) {

                        }
                    });
                }
            }
        });
    }

    public void dialog(boolean value){

        if(value){
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Connection Established");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.success_green));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.connected_wifi,0,0,0);
            if(((TextView)findViewById(R.id.tv_check_connection)).getVisibility()==View.VISIBLE)
            {
                Handler handler = new Handler();
                Runnable delayrunnable = new Runnable() {
                    @Override
                    public void run() {
                        ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.GONE);
                    }
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        }else {
            ((TextView)findViewById(R.id.tv_check_connection)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_check_connection)).setText("Internet Connection Lost");
            ((TextView)findViewById(R.id.tv_check_connection)).setBackgroundColor(getResources().getColor(R.color.error_red));
            ((TextView)findViewById(R.id.tv_check_connection)).setCompoundDrawablesWithIntrinsicBounds(R.drawable.notconnected_wifi,0,0,0);
        }
    }

}

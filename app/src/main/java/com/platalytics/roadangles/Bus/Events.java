package com.platalytics.roadangles.Bus;

import com.platalytics.roadangles.Models.ChatModel;

/**
 * Created by kamil on 15/02/2018.
 */

public class Events {


    public static class GetNewMessage {

        private ChatModel chatModel;

        public GetNewMessage(ChatModel chatModel) {
            this.chatModel = chatModel;
        }

        public ChatModel getChatModel() {
            return chatModel;
        }
    }

    public static class SendNewMessage {

        public String senderId;
        public String recieverId;
        public String authKey;
        public String message;
        public String date;
        public String tripId;

        public SendNewMessage(String senderId,String recieverId,String authKey, String message,String date,String tripId) {
            this.senderId = senderId;
            this.recieverId = recieverId;
            this.authKey = authKey;
            this.message = message;
            this.date = date;
            this.tripId = tripId;
        }

    }

}

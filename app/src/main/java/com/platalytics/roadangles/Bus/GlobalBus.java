package com.platalytics.roadangles.Bus;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by kamil on 15/02/2018.
 */

public class GlobalBus {
    private static Bus sBus;

    public static Bus getBus() {
        if (sBus == null)
            sBus = new Bus(ThreadEnforcer.MAIN);
        return sBus;
    }
}

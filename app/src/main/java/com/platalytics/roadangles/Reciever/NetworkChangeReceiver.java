package com.platalytics.roadangles.Reciever;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.platalytics.roadangles.Activities.AddPaymentDetailActivity;
import com.platalytics.roadangles.Activities.ChangePasswordActivity;
import com.platalytics.roadangles.Activities.ChatActivity;
import com.platalytics.roadangles.Activities.CodeVerificationActivity;
import com.platalytics.roadangles.Activities.ForgetPasswordActivity;
import com.platalytics.roadangles.Activities.HistoryActivity;
import com.platalytics.roadangles.Activities.LoginActivity;
import com.platalytics.roadangles.Activities.PaymentMethodActivity;
import com.platalytics.roadangles.Activities.PhoneVerificationActivity;
import com.platalytics.roadangles.Activities.ProfileActivity;
import com.platalytics.roadangles.Activities.RatingReviewsActivity;
import com.platalytics.roadangles.Activities.RegisterActivity;
import com.platalytics.roadangles.Activities.ResetPasswodCodeActivity;
import com.platalytics.roadangles.Activities.ResetPasswordActivity;
import com.platalytics.roadangles.Activities.ReviewServiceActivity;
import com.platalytics.roadangles.Activities.ServiceDetailActivity;
import com.platalytics.roadangles.Activities.SettingActivity;
import com.platalytics.roadangles.Activities.SplashActivity;
import com.platalytics.roadangles.Activities.User.ServicesActivity;
import com.platalytics.roadangles.Activities.UserSelectionActivity;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;


/**
 * Created by kamil on 06/02/2018.
 */

public class NetworkChangeReceiver extends BroadcastReceiver
{
    Activity context;
    public void setContext(Activity context)
    {
        this.context = context;
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        try
        {
            if(context instanceof SplashActivity)
            {
                if (isOnline(context)) {
                    ((SplashActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((SplashActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof UserSelectionActivity)
            {
                if (isOnline(context)) {
                    ((UserSelectionActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((UserSelectionActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof LoginActivity)
            {
                if (isOnline(context)) {
                    ((LoginActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((LoginActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof RegisterActivity)
            {
                if (isOnline(context)) {
                    ((RegisterActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((RegisterActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof com.platalytics.roadangles.Activities.User.HomeActivity )
            {
                if (isOnline(context)) {
                    ((com.platalytics.roadangles.Activities.User.HomeActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((com.platalytics.roadangles.Activities.User.HomeActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof com.platalytics.roadangles.Activities.Vendor.HomeActivity)
            {
                if (isOnline(context)) {
                    ((com.platalytics.roadangles.Activities.Vendor.HomeActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((com.platalytics.roadangles.Activities.Vendor.HomeActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof SettingActivity)
            {
                if (isOnline(context)) {
                    ((SettingActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((SettingActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof ServiceDetailActivity)
            {
                if (isOnline(context)) {
                    ((ServiceDetailActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((ServiceDetailActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof ReviewServiceActivity)
            {
                if (isOnline(context)) {
                    ((ReviewServiceActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((ReviewServiceActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof ResetPasswordActivity)
            {
                if (isOnline(context)) {
                    ((ResetPasswordActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((ResetPasswordActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof ResetPasswodCodeActivity)
            {
                if (isOnline(context)) {
                    ((ResetPasswodCodeActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((ResetPasswodCodeActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof ProfileActivity)
            {
                if (isOnline(context)) {
                    ((ProfileActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((ProfileActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof PaymentMethodActivity)
            {
                if (isOnline(context)) {
                    ((PaymentMethodActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((PaymentMethodActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof HistoryActivity)
            {
                if (isOnline(context)) {
                    ((HistoryActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((HistoryActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof ForgetPasswordActivity)
            {
                if (isOnline(context)) {
                    ((ForgetPasswordActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((ForgetPasswordActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof CodeVerificationActivity)
            {
                if (isOnline(context)) {
                    ((CodeVerificationActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((CodeVerificationActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof PhoneVerificationActivity)
            {
                if (isOnline(context)) {
                    ((PhoneVerificationActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((PhoneVerificationActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof ChangePasswordActivity)
            {
                if (isOnline(context)) {
                    ((ChangePasswordActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((ChangePasswordActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof AddPaymentDetailActivity)
            {
                if (isOnline(context)) {
                    ((AddPaymentDetailActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((AddPaymentDetailActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof ServicesActivity)
            {
                if (isOnline(context)) {
                    ((ServicesActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((ServicesActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof ChatActivity)
            {
                if (isOnline(context)) {
                    ((ChatActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((ChatActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }
            else if(context instanceof RatingReviewsActivity)
            {
                if (isOnline(context)) {
                    ((RatingReviewsActivity)context).dialog(true);
                    SharedClass.isNetConnected = true;
                    Log.e("NetConnection", "Online Connect Intenet ");
                } else {
                    ((RatingReviewsActivity)context).dialog(false);
                    SharedClass.isNetConnected = false;
                    Log.e("NetConnection", "Conectivity Failure !!! ");
                }
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private boolean isOnline(Context context) {
        try {

            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            boolean isWifiConn = networkInfo.isConnected();
            networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            boolean isMobileConn = networkInfo.isConnected();
            if(isWifiConn || isMobileConn)
            {
                return true;
            }
            else {
                return false;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
    }
}
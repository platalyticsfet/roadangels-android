package com.platalytics.roadangles.Utilities;

/**
 * Created by kamilfaheem on 08/11/2017.
 */

public class Constants
{
    public static final int TRIP = 0;
    public static final int END_USER = 1;
    public static final int VENDOR = 2;
    public static final int STRING = 3;
    public static final int OBJECT = 4;
    public static final int THROWABLE = 5;
    public static final int JSONARRAY_TYPE = 6;
    public static final int JSONOBJ_TYPE = 7;
    public static final int AUTHENTICATION = 8;
    public static final String ERROR = "error";
    public static final String DATA = "data";
    public static final String SOCKET_EXCEPTION = "java.net.SocketException";
    public static final String SOCKET_TIME_EXCEPTION = "java.net.SocketTimeoutException";
    public static final String SOCKET_EVENT_CONNECTED = "eventConnected";
    public static final String SEND_VENDOR_LOCATION = "sendVendorLocation";
    public static final String SERVICE_REQUEST = "serviceRequest";
    public static final String ACCEPT_REQUEST = "acceptRequest";
    public static final String CANCEL_REQUEST = "cancelRequest";
    public static final String HIDE_REQUEST = "hideRequest";
    public static final String FINISH_TRIP = "finishTrip";
    public static final String TRIP_ROUTE = "tripRoute";
    public static final String GET_VENDOR_LOCATION = "getVendorLocation";
    public static final String VENDOR_LOCATION = "vendorLocation";
    public static final String USER_LOCATION = "userLocation";
    public static final String TEST_SOCKET = "testSocket";
    public static final String PATH = "path";
    public static final String CONNECTION_ALIVE = "connectionAlive";
    public static final String SEND_REQUEST_TO_VENDOR = "sendRequestToVendor";
    public static final String AUTHENTICATION_ERROR = "Authentication Failed";
    public static final String RECIEVE_NEW_MESSAGE = "ReciveNewMessage";
    public static final String SEND_NEW_MESSAGE = "SendNewMessage";
    public static final int STATUS_200 = 200;   // Success
    public static final int STATUS_300 = 300;   // Authentication Error
    public static final int STATUS_400 = 400;   // Something Not Found
    public static final int STATUS_500 = 500;   // failure
}

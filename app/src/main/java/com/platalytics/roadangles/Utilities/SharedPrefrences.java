package com.platalytics.roadangles.Utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kamilfaheem on 08/11/2017.
 */

public class SharedPrefrences {
    private static Context context;
    private static SharedPreferences sharedpreferences;
    private static SharedPreferences.Editor editor;

    public static String PREF_USER="try_pref_user";
    private static String LATITUDE = "latitude";
    private static String LONGITUDE = "longitude";
    private static String EMAIL = "email";
    private static String PASSWORD = "password";
    private static String LOGIN_AS = "loginAs";
    private static String INDEX = "index";
    private static String DEVICE_TOKEN = "deviceToken";

    public static void InitSharedPrefrences(Context mycontext)
    {
        context = mycontext;
        sharedpreferences = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
    }

    public static void saveUserLocation(String lat,String lng)
    {
        try
        {
            editor.putString(LATITUDE, lat);
            editor.putString(LONGITUDE, lng);
            editor.commit();
        }
        catch (Exception exp){}
    }

    public static void saveEmailAddress(String email)
    {
        editor.putString(EMAIL, email);
        editor.commit();
    }

    public static void savePassword(String password)
    {
        editor.putString(PASSWORD, password);
        editor.commit();
    }

    public static void saveDeviceToken(String token)
    {
        editor.putString(DEVICE_TOKEN, token);
        editor.commit();
    }

    public static String getPassword()
    {
        return sharedpreferences.getString(PASSWORD,"");
    }

    public static String getEmailAddress()
    {
        return sharedpreferences.getString(EMAIL,"");
    }

    public static String getUserLatitude()
    {
        return sharedpreferences.getString(LATITUDE,"");
    }

    public static String getUserLongitude()
    {
        return sharedpreferences.getString(LONGITUDE,"");
    }

    public static String getDeviceToken()
    {
        return sharedpreferences.getString(DEVICE_TOKEN,"");
    }

    public static void clearAll() {
        editor.remove(INDEX);
        editor.remove(EMAIL);
        editor.remove(PASSWORD);
        editor.commit();
    }

    public static void loginAs() {
        editor.putInt(LOGIN_AS, SharedClass.accountType);
        editor.commit();
    }

    public static int getLoginAs()
    {
        return sharedpreferences.getInt(LOGIN_AS,Constants.END_USER);
    }

    public static void saveIndex(int index) {
        editor.putInt(INDEX, index);
        editor.commit();
    }

    public static int getIndex()
    {
        return sharedpreferences.getInt(INDEX,0);
    }

}

package com.platalytics.roadangles.Utilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.makeramen.roundedimageview.RoundedImageView;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.platalytics.roadangles.Interfaces.ResponceCallback;
import com.platalytics.roadangles.Models.TripModel;
import com.platalytics.roadangles.R;
import com.squareup.picasso.Picasso;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by laptop on 26/08/2017.
 */

public class Utility {

    public static ProgressDialog myDialog = null;
    private static android.support.v7.app.AlertDialog myalertdialog;
    private static AlertDialog alertDialog;

    public static void ShowErrorToast(Activity context,String string) {
        new StyleableToast
                .Builder(context)
                .text(string)
                .textColor(Color.WHITE)
                .backgroundColor(Color.RED)
                .show();
    }

    public static void ShowGenericToast(Activity context,String string) {
        new StyleableToast
                .Builder(context)
                .text(string)
                .textColor(Color.WHITE)
                .backgroundColor(Color.BLACK)
                .show();
    }

    public static void ShowSuccessToast(Activity context,String string)
    {
        new StyleableToast
                .Builder(context)
                .text(string)
                .textColor(Color.WHITE)
                .backgroundColor(Color.GREEN)
                .show();
    }

    public static void ShowErrorSneakbar(Activity context,View view,String message)
    {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(android.R.color.holo_red_light ));
        snackbar.show();
    }

    public static void ShowSuccessSneakbar(Activity context,View view,String message)
    {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(android.R.color.holo_green_dark ));
        snackbar.show();
    }

    public static void hideKeyPad(Activity context)
    {
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void ShowProgressDialog(final Activity context, final String message) {
        context.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                try {
                    myDialog=new ProgressDialog(context);
                    myDialog.setMessage(message);
                    myDialog.setCancelable(false);
                    myDialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void HideProgressDialog() {
        try {
            if (myDialog != null && myDialog.isShowing()) {
                myDialog.dismiss();
                myDialog = null;
            }
        } catch (Exception e) {

        }
    }

    public static boolean ValidPassword(String password){
        Pattern pattern;
        Matcher matcher;
        String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!]).{8,20})";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();

    }

    public static void ShowAlertDialog(Activity context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final FrameLayout frameView = new FrameLayout(context);
        builder.setView(frameView);
        alertDialog = builder.create();
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_alert, frameView);
        dialoglayout.findViewById(R.id.okButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(dialoglayout);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
        alertDialog.show();
    }

    public static void ShowSuccessDialog(Activity context,String message, final ResponceCallback responceCallback)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final FrameLayout frameView = new FrameLayout(context);
        builder.setView(frameView);
        alertDialog = builder.create();
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_success, frameView);
        ((TextView)dialoglayout.findViewById(R.id.error_msg)).setText(message);
        dialoglayout.findViewById(R.id.okButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if(responceCallback!=null)
                    responceCallback.onSuccessResponce(null);
            }
        });
        alertDialog.setView(dialoglayout);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
        alertDialog.show();
    }

    public static void ShowFailurDialog(Activity context,String msg, final ResponceCallback responceCallback)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final FrameLayout frameView = new FrameLayout(context);
        builder.setView(frameView);
        alertDialog = builder.create();
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_error, frameView);
        ((TextView)dialoglayout.findViewById(R.id.error_msg)).setText(msg);
        dialoglayout.findViewById(R.id.okButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if(responceCallback!=null)
                    responceCallback.onSuccessResponce(null);
            }
        });
        alertDialog.setView(dialoglayout);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
        alertDialog.show();
    }

    public static void ShowYesNoDialog(Activity context,String title,String messge, String positiveBtnText,String negitiveBtnText,final ResponceCallback responceCallback)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
                .setMessage(messge)
                .setCancelable(false)
                .setPositiveButton(positiveBtnText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        responceCallback.onSuccessResponce(null);
                        alertDialog.dismiss();
                    }
                })
                .setNegativeButton(negitiveBtnText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        responceCallback.onFailureResponce(null,-1);
                        alertDialog.dismiss();
                    }
                });
        alertDialog = builder.create();
        alertDialog.show();
    }

    public static String getPathFromUri(Activity context,final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }

                    // TODO handle non-primary volumes
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {

                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    return getDataColumn(context,contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[] {
                            split[1]
                    };

                    return getDataColumn(context,contentUri, selection, selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {

                // Return the remote address
                if (isGooglePhotosUri(uri))
                    return uri.getLastPathSegment();

                return getDataColumn(context,uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
        }

        return null;
    }

    public static String getDataColumn(Activity context,Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static String createCameraImageFileName() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return timeStamp + ".jpg";
    }

    public static void ErrorResponseHandler(Activity activity,Object obj, int objectType)
    {
        /*try
        {
            if(objectType== Constants.STRING)
            {
                String message = obj.toString();
                Utility.ShowErrorToast(activity,message);
            }
            else if(objectType==Constants.THROWABLE)
            {
                Throwable throwable = (Throwable) obj;
                String exceptionClassName = throwable.getClass().getName();
                if(exceptionClassName.equals(Constants.SOCKET_EXCEPTION) || exceptionClassName.equals(Constants.SOCKET_TIME_EXCEPTION))
                {
                    Utility.ShowErrorToast(activity,"Internet/Socket Connection lost. Please try again");
                }
                else {
                    Utility.ShowErrorToast(activity, throwable.getMessage());
                }
            }
        }
        catch (Exception exp){}*/
    }

    public static String getFormatedData_MMMM_DD_YYYY_HH_MM_A(String dateInMilliseconds)
    {
        return DateFormat.format("MMMM dd, yyyy hh:mm AA", Long.parseLong(dateInMilliseconds)).toString();
    }

    public static String getFormatedData_HH_mm_ss(String dateInMilliseconds)
    {
        return DateFormat.format("hh:mm AA", Long.parseLong(dateInMilliseconds)).toString();
    }

    public static String getFormatedData_yyyy_MM_dd_HH_mm_ss(long dateInMilliseconds)
    {
        return DateFormat.format("dd/MM/yyyy hh:mm AA", dateInMilliseconds).toString();
    }

    public static Date getFormatedData_yyyy_MM_dd_HH_mm_ss(String dateString)
    {
        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        try {
            date = format.parse(dateString);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getRemainingTimeFromDealEndTime(String dealEndDate,boolean getHour,boolean getMin,boolean getSec) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String unit = "";
        try {
            Date date = format.parse(dealEndDate);
            long currentTime = System.currentTimeMillis();
            long endTime = date.getTime();
            if(endTime>currentTime)
            {
                long diffInMillisec = endTime - currentTime;
                long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMillisec);
                int seconds = (int) (diffInSec % 60);
                diffInSec /= 60;
                int minutes = (int) (diffInSec % 60);
                diffInSec /= 60;
                int hours = (int) (diffInSec % 24);
                diffInSec /= 24;
                int days = (int) diffInSec;

                if(days<=1)
                {
                    if(getHour)
                    {
                        unit = hours+"";
                    }
                    else if(getMin)
                    {
                        unit = minutes+"";
                    }
                    else if(getSec)
                    {
                        unit = seconds+"";
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return unit;
    }

    public static void ShowImageZoomView(String imageUrl)
    {
        /*AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.CustomDialog);
        final FrameLayout frameView = new FrameLayout(activity);
        builder.setView(frameView);
        alertDialog = builder.create();
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.image_zoom_view, frameView);
        RoundedImageView roundedImageView = (RoundedImageView) dialoglayout.findViewById(R.id.imageview);
        Picasso.with(activity).load(imageUrl).error(R.drawable.detail_img).placeholder(R.drawable.detail_img).into(roundedImageView);
        ImageView cross = dialoglayout.findViewById(R.id.cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(dialoglayout);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
        alertDialog.show();*/
    }

    public static String makeGooglePathURL (Activity context,double sourcelat, double sourcelog, double destlat, double destlog ){
        StringBuilder urlString = new StringBuilder();
        urlString.append("http://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString.append(Double.toString( sourcelog));
        urlString.append("&destination=");
        urlString.append(Double.toString( destlat));
        urlString.append(",");
        urlString.append(Double.toString( destlog));
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        urlString.append("&key="+context.getString(R.string.google_maps_key));
        return urlString.toString();
    }

    public static String makeGoogleImageURL (TripModel tripModel){
        //http://maps.googleapis.com/maps/api/staticmap?
        // center=37.420988,-122.086891&zoom=17&format=png&sensor=false&size=640x480&maptype=roadmap&style=element:labels|visibility:off
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/staticmap");
        urlString.append("?size=600x250&format=png&sensor=false&markers=color:0xfe9f00%7C");
        urlString.append(tripModel.userLatLng);
        urlString.append("&markers=color:0x3CB371%7C");
        urlString.append(tripModel.vendorLatLng);
        urlString.append("&maptype=roadmap&style="+SharedClass.mapStyle+"&path=weight:4%7Ccolor:0xd68c0e%7Cenc:");
        urlString.append(tripModel.tripPath);
        return urlString.toString();
    }

    public static List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng( (((double) lat / 1E5)),
                    (((double) lng / 1E5) ));
            poly.add(p);
        }

        return poly;
    }

    public static Bitmap createDrawableFromView(Activity context,View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    public static double getDistance(LatLng source, LatLng vendor)
    {
        double lat1 = source.latitude;
        double lat2 = vendor.latitude;
        double lon1 = source.longitude;
        double lon2 = vendor.longitude;
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    public static void getKeyHash(Activity context) {
        PackageInfo info;
        try {
            info = context.getPackageManager().getPackageInfo("com.platalytics.roadangles", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("HashKey", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    public static String replace(String str, int index, char replace){
        if(str==null){
            return str;
        }else if(index<0 || index>=str.length()){
            return str;
        }
        char[] chars = str.toCharArray();
        chars[index] = replace;
        return String.valueOf(chars);
    }

    public static int pxToDp(Activity context,int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static int dpToPx(Activity context,int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static void turnGPSOn(Activity contxt){
        Intent intent=new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", true);
        contxt.sendBroadcast(intent);
    }
}

package com.platalytics.roadangles.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kamilfaheem on 09/11/2017.
 */

public class GoogleAddressModel
{

    @SerializedName("results")
    @Expose
    public List<Result> results = null;
    @SerializedName("status")
    @Expose
    public String status;

    public class Result {

        @SerializedName("address_components")
        @Expose
        public List<AddressComponent> addressComponents = null;
        @SerializedName("formatted_address")
        @Expose
        public String formattedAddress;
        @SerializedName("geometry")
        @Expose
        public Geometry geometry;
        @SerializedName("place_id")
        @Expose
        public String placeId;
        @SerializedName("types")
        @Expose
        public List<String> types = null;

        public class AddressComponent {

            @SerializedName("long_name")
            @Expose
            public String longName;
            @SerializedName("short_name")
            @Expose
            public String shortName;
            @SerializedName("types")
            @Expose
            public List<String> types = null;

        }

    }


    public class Bounds {

        @SerializedName("northeast")
        @Expose
        public Northeast northeast;
        @SerializedName("southwest")
        @Expose
        public Southwest southwest;

        public class Southwest {

            @SerializedName("lat")
            @Expose
            public Float lat;
            @SerializedName("lng")
            @Expose
            public Float lng;

        }

    }

    public class Geometry {

        @SerializedName("bounds")
        @Expose
        public Bounds bounds;
        @SerializedName("location")
        @Expose
        public Location location;
        @SerializedName("location_type")
        @Expose
        public String locationType;
        @SerializedName("viewport")
        @Expose
        public Viewport viewport;

        public class Viewport {

            @SerializedName("northeast")
            @Expose
            public Northeast_ northeast;
            @SerializedName("southwest")
            @Expose
            public Southwest_ southwest;

            public class Northeast_ {

                @SerializedName("lat")
                @Expose
                public Float lat;
                @SerializedName("lng")
                @Expose
                public Float lng;

            }


            public class Southwest_ {

                @SerializedName("lat")
                @Expose
                public Float lat;
                @SerializedName("lng")
                @Expose
                public Float lng;

            }

        }

    }

    public class Location {

        @SerializedName("lat")
        @Expose
        public Float lat;
        @SerializedName("lng")
        @Expose
        public Float lng;

    }

    public class Northeast {

        @SerializedName("lat")
        @Expose
        public Float lat;
        @SerializedName("lng")
        @Expose
        public Float lng;

    }

}

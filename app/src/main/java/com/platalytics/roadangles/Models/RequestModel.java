package com.platalytics.roadangles.Models;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kamil on 15/11/2017.
 */

public class RequestModel {
    public List<String> vendorsId = new ArrayList<>();
    public String request_status;
    public String request_time;
    public String user_lng;
    public String user_lat;
    public String user_id;
    public String requestId;
    public String user_address;
    public User user = new User();
    public String distanceTime;
    public int colorIndex;
    public String serviceNote;
    public ServicesModel service = new ServicesModel();

    public RequestModel()
    {

    }

    public RequestModel(JSONObject jsonObject)
    {
        try
        {
            this.request_status = jsonObject.getString("request_status");
            this.request_time = jsonObject.getString("request_time");
            this.user_lng = jsonObject.getString("user_lng");
            this.user_lat = jsonObject.getString("user_lat");
            this.user_id = jsonObject.getString("user_id");
            this.requestId = jsonObject.getString("_id");
            this.user_address = jsonObject.getString("address");
            this.serviceNote = jsonObject.getString("service_note");
        }
        catch (Exception exp)
        {
            exp.printStackTrace();
        }
    }
}

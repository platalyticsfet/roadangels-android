package com.platalytics.roadangles.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kamil on 08/12/2017.
 */

public class ColorsModel {
    @SerializedName("colorId")
    @Expose
    public String colorId;
    @SerializedName("hexString")
    @Expose
    public String hexString;
    @SerializedName("rgb")
    @Expose
    public Rgb rgb;
    @SerializedName("hsl")
    @Expose
    public Hsl hsl;
    @SerializedName("name")
    @Expose
    public String name;

    public class Rgb {

        @SerializedName("r")
        @Expose
        public String r;
        @SerializedName("g")
        @Expose
        public String g;
        @SerializedName("b")
        @Expose
        public String b;

    }

    public class Hsl {

        @SerializedName("h")
        @Expose
        public String h;
        @SerializedName("s")
        @Expose
        public String s;
        @SerializedName("l")
        @Expose
        public String l;

    }
}

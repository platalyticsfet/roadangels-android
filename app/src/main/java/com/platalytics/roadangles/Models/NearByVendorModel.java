package com.platalytics.roadangles.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by kamilfaheem on 08/11/2017.
 */

public class NearByVendorModel implements Serializable
{
    @SerializedName("_id")
    @Expose
    public String id;
    @SerializedName("vendor_id")
    @Expose
    public String vendorId;
    @SerializedName("latitude")
    @Expose
    public String latitude;
    @SerializedName("longitude")
    @Expose
    public String longitude;
    @SerializedName("address")
    @Expose
    public String address;

}

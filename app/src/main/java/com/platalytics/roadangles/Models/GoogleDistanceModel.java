package com.platalytics.roadangles.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kamilfaheem on 14/11/2017.
 */

public class GoogleDistanceModel {
    @SerializedName("destination_addresses")
    @Expose
    public List<String> destinationAddresses = null;
    @SerializedName("origin_addresses")
    @Expose
    public List<String> originAddresses = null;
    @SerializedName("rows")
    @Expose
    public List<Row> rows = null;
    @SerializedName("status")
    @Expose
    public String status;


    public class Row {

        @SerializedName("elements")
        @Expose
        public List<Element> elements = null;
        public class Element {

            @SerializedName("distance")
            @Expose
            public Distance distance;
            @SerializedName("duration")
            @Expose
            public Duration duration;
            @SerializedName("status")
            @Expose
            public String status;

            public class Distance {

                @SerializedName("text")
                @Expose
                public String text;
                @SerializedName("value")
                @Expose
                public Integer value;

            }

            public class Duration {

                @SerializedName("text")
                @Expose
                public String text;
                @SerializedName("value")
                @Expose
                public Integer value;

            }
        }
    }
}

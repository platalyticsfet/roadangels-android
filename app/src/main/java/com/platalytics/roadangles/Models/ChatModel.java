package com.platalytics.roadangles.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kamil on 09/02/2018.
 */

public class ChatModel {
    @SerializedName("_id")
    @Expose
    public String id;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("From")
    @Expose
    public UserInfo from;
    @SerializedName("To")
    @Expose
    public UserInfo to;
    @SerializedName("__v")
    @Expose
    public String v;
    @SerializedName("trip_id")
    @Expose
    public String tripId;

    public ChatModel(String id, String message, String date, String user_name,String user_id,String user_pic,String tripId) {
        this.id = id;
        this.message = message;
        this.date = date;
        from = new UserInfo();
        from.id = user_id;
        from.name = user_name;
        from.pic = user_pic;
        this.tripId = tripId;
    }

    public class UserInfo {

        @SerializedName("pic")
        @Expose
        public String pic;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("id")
        @Expose
        public String id;

    }
}

package com.platalytics.roadangles.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by kamilfaheem on 10/11/2017.
 */

public class ServicesModel implements Serializable{
    @SerializedName("_id")
    @Expose
    public String serviceId;
    @SerializedName("service_name")
    @Expose
    public String serviceName;
    @SerializedName("service_image")
    @Expose
    public String serviceImage;
    @SerializedName("service_detail")
    @Expose
    public String serviceDetail;
    @SerializedName("service_price")
    @Expose
    public String servicePrice;

    public ServicesModel() {
    }

    public ServicesModel(JSONObject jsonObject)
    {
        try
        {
            this.serviceId = jsonObject.getString("_id");
            this.serviceName = jsonObject.getString("service_name");
            this.serviceDetail = jsonObject.getString("service_detail");
            this.serviceImage = jsonObject.getString("service_image");
            this.servicePrice = jsonObject.getString("service_price");
        }
        catch (Exception exp)
        {
            exp.printStackTrace();
        }
    }
}

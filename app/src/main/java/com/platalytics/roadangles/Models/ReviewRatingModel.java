package com.platalytics.roadangles.Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kamil on 05/03/2018.
 */

public class ReviewRatingModel {
    public float averageRating;
    public int totalRating;
    public float totalRating_5;
    public float totalRating_4;
    public float totalRating_3;
    public float totalRating_2;
    public float totalRating_1;
    public List<Reviews> reviews = new ArrayList<>();

    public class Reviews {

        public String profilePic;
        public String userName;
        public float rating;
        public String date;
        public String reviewText;

    }

    public ReviewRatingModel(float averageRating, int totalRating, float totalRating_5, float totalRating_4, float totalRating_3, float totalRating_2, float totalRating_1, List<Reviews> reviews) {
        this.averageRating = averageRating;
        this.totalRating = totalRating;
        this.totalRating_5 = totalRating_5;
        this.totalRating_4 = totalRating_4;
        this.totalRating_3 = totalRating_3;
        this.totalRating_2 = totalRating_2;
        this.totalRating_1 = totalRating_1;
        this.reviews = reviews;
    }
}

package com.platalytics.roadangles.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by kamilfaheem on 17/11/2017.
 */

public class TripModel implements Serializable{

    @SerializedName("__v")
    @Expose
    public String v;
    @SerializedName("end_time")
    @Expose
    public String endTime;
    @SerializedName("start_time")
    @Expose
    public String startTime;


    @SerializedName("destination_lat_lng")
    @Expose
    public String vendorLatLng;
    @SerializedName("vendor_id")
    @Expose
    public String vendorId;
    @SerializedName("destination_address")
    @Expose
    public String vendorAddress;
    @SerializedName("vendor_name")
    @Expose
    public String vendorName = null;
    @SerializedName("vendor_profilepic")
    @Expose
    public String vendorProfilePic = null;


    @SerializedName("source_lat_lng")
    @Expose
    public String userLatLng;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("user_name")
    @Expose
    public String userName = null;
    @SerializedName("source_address")
    @Expose
    public String userAddress = null;
    @SerializedName("user_profilepic")
    @Expose
    public String userProfilePic = null;


    @SerializedName("service_id")
    @Expose
    public String serviceId;
    @SerializedName("service_name")
    @Expose
    public String serviceName;
    @SerializedName("service_price")
    @Expose
    public String servicePrice;
    @SerializedName("service_icon")
    @Expose
    public String serviceIcon;

    @SerializedName("_id")
    @Expose
    public String id;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("trip_path")
    @Expose
    public String tripPath;

    @SerializedName("vendor_rating")
    @Expose
    public String vendorRating;

    @SerializedName("user_rating")
    @Expose
    public String userRating;

    @SerializedName("map_image")
    @Expose
    public String mapImage;


    public TripModel() {
    }

    public TripModel(JSONObject jsonObject) {
        try
        {
            v = jsonObject.getString("__v");
            endTime = jsonObject.getString("end_time");
            startTime = jsonObject.getString("start_time");
            vendorLatLng = jsonObject.getString("destination_lat_lng");
            vendorId = jsonObject.getString("vendor_id");
            vendorAddress = jsonObject.getString("destination_address");
            vendorName = jsonObject.getString("vendor_name");
            vendorProfilePic = jsonObject.getString("vendor_profilepic");
            userLatLng = jsonObject.getString("source_lat_lng");
            userId = jsonObject.getString("user_id");
            userName = jsonObject.getString("user_name");
            userAddress = jsonObject.getString("source_address");
            serviceId = jsonObject.getString("service_id");
            serviceName = jsonObject.getString("service_name");
            servicePrice = jsonObject.getString("service_price");
            serviceIcon = jsonObject.getString("service_icon");
            id = jsonObject.getString("_id");
            userProfilePic = jsonObject.getString("user_profilepic");
            status = jsonObject.getString("status");
            tripPath = jsonObject.getString("trip_path");
            vendorRating = jsonObject.getString("vendor_rating");
            userRating = jsonObject.getString("user_rating");
            mapImage = jsonObject.getString("map_image");
        }catch (Exception exp)
        {
            exp.printStackTrace();
        }
    }
}

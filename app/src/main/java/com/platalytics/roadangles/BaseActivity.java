package com.platalytics.roadangles;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.platalytics.roadangles.Activities.User.HomeActivity;
import com.platalytics.roadangles.Bus.GlobalBus;
import com.platalytics.roadangles.Reciever.NetworkChangeReceiver;
import com.platalytics.roadangles.ServerUtils.ServerCalls;
import com.platalytics.roadangles.Services.LocationService;
import com.platalytics.roadangles.SocketsUtil.SocketManager;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;
import com.platalytics.roadangles.Utilities.SharedPrefrences;
import com.platalytics.roadangles.Utilities.Utility;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by kamilfaheem on 08/11/2017.
 */

public abstract class BaseActivity extends AppCompatActivity
{
    public static boolean isAppWentToBg = false;
    public static boolean isWindowFocused = false;
    public static boolean isMenuOpened = false;
    public static boolean isBackPressed = false;
    public static NetworkChangeReceiver mNetworkReceiver;
    public SocketManager socketManager = null;

    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(socketManager!=null && socketManager.isConnected())
            {
                socketManager.KeepConnectionAlive();
            }
            if(SharedClass.accountType== Constants.VENDOR || (SharedClass.accountType==Constants.END_USER && SharedClass.latitude==0.0))
            {
                SharedClass.latitude = Double.valueOf(intent.getStringExtra("latutide"));
                SharedClass.longitude = Double.valueOf(intent.getStringExtra("longitude"));
                SharedPrefrences.saveUserLocation(SharedClass.latitude+"",SharedClass.longitude+"");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefrences.InitSharedPrefrences(this);
        ServerCalls.initServerCall(this);
        GlobalBus.getBus().register(this);
    }

    public void registerNetworkBroadcast()
    {
        mNetworkReceiver = new NetworkChangeReceiver();
        registerReceiver(mNetworkReceiver,new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    public void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(LocationService.str_receiver));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStart() {
        applicationWillEnterForeground();
        super.onStart();
    }

    private void applicationWillEnterForeground() {
        if (isAppWentToBg) {
            isAppWentToBg = false;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        applicationdidenterbackground();
    }

    public void applicationdidenterbackground() {
        if (!isWindowFocused) {
            isAppWentToBg = true;
        }
    }

    @Override
    public void onBackPressed() {

        if (this instanceof com.platalytics.roadangles.Activities.Vendor.HomeActivity || this instanceof com.platalytics.roadangles.Activities.User.HomeActivity) {

        } else {
            isBackPressed = true;
        }
        super.onBackPressed();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        isWindowFocused = hasFocus;

        if (isBackPressed && !hasFocus) {
            isBackPressed = false;
            isWindowFocused = true;
        }

        super.onWindowFocusChanged(hasFocus);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }
}

package com.platalytics.roadangles.SocketsUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.share.Share;
import com.google.gson.Gson;
import com.platalytics.roadangles.Interfaces.SocketMessageListerner;
import com.platalytics.roadangles.RoadAngelsApplication;
import com.platalytics.roadangles.ServerUtils.Urls;
import com.platalytics.roadangles.Utilities.Constants;
import com.platalytics.roadangles.Utilities.SharedClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.client.SocketIOException;
import io.socket.emitter.Emitter;

/**
 * Created by DeviceBee on 8/22/2017.
 */

public class SocketManager
{
    private static Socket mSocket;
    private RoadAngelsApplication activity;
    private static String TAG = "Socket";
    SocketMessageListerner listerner;
    private static SocketManager mInstance;

    public static synchronized SocketManager getInstance() {
        return mInstance;
    }

    public SocketManager(final RoadAngelsApplication activity) {
        this.activity = activity;
        mInstance = this;
    }

    public void initSocketListener(SocketMessageListerner listerner)
    {
        this.listerner = listerner;
    }

    public void connectSocketManager() {

        try {
            /*String query = String.format(Locale.getDefault(),
                    Urls.SERVER_URL,
                    SharedClass.userModel.getAccessKey(),
                    SharedClass.userModel.getId().toString()
            );*/
            IO.Options opts = new IO.Options();
            opts.forceNew = false;      // default true
            opts.reconnection = true;
            opts.secure = false;
            opts.timeout = 4000;
            opts.port = Urls.SOCKET_PORT;
            opts.reconnectionDelay = 1000;
            //opts.query = query;
            opts.transports = new String[]{"polling"};
            mSocket = IO.socket(Urls.CLOUD_BASE_URL, opts);

            // Socket Connected Event
            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.w(TAG, Socket.EVENT_CONNECT);
                }
            });

            // Socket Event Disconnect
            mSocket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener()
            {
                @Override
                public void call(Object... args)
                {
                    Log.w(TAG, Socket.EVENT_DISCONNECT);
                }
            });

            // Socket Event Connect Error
            mSocket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener()
            {
                @Override
                public void call(Object... args)
                {
                    Log.w(TAG, Socket.EVENT_CONNECT_ERROR);
                    for (Object o : args) {
                        Log.w("TAG", "object: " + o.toString());
                        if (o instanceof SocketIOException)
                            ((SocketIOException) o).printStackTrace();
                    }

                }
            });

            // Socket Event Error
            mSocket.on(Socket.EVENT_ERROR, new Emitter.Listener()
            {
                @Override
                public void call(Object... args)
                {
                    Log.w(TAG, Socket.EVENT_ERROR);
                    for (Object o : args) {
                        Log.w("TAG", "object: " + o.toString());
                        if (o instanceof SocketIOException)
                            ((SocketIOException) o).printStackTrace();
                    }
                }
            });

            mSocket.on(Constants.SOCKET_EVENT_CONNECTED, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.w(TAG, Constants.SOCKET_EVENT_CONNECTED);
                }
            });
            mSocket.on(Constants.CONNECTION_ALIVE, new Emitter.Listener()
            {
                @Override
                public void call(Object... args)
                {
                    //Log.w(TAG, "Yes : "+Constants.CONNECTION_ALIVE);
                }
            });
            mSocket.on(Constants.SERVICE_REQUEST, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.w(TAG, Constants.SERVICE_REQUEST);
                    if (args.length > 0)
                    {
                        try
                        {
                            if(SharedClass.accountType==Constants.VENDOR && listerner!=null)
                            {
                                listerner.onGetMessageAck(Constants.SERVICE_REQUEST,Constants.JSONARRAY_TYPE,args[0]);
                            }
                        }catch (Exception e) {e.printStackTrace();}
                    }
                }
            });
            mSocket.on(Constants.ACCEPT_REQUEST, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if(listerner!=null) {
                        Log.w(TAG, Constants.ACCEPT_REQUEST);
                        listerner.onGetMessageAck(Constants.ACCEPT_REQUEST,Constants.JSONARRAY_TYPE,args[0]);
                    }
                }
            });
            mSocket.on(Constants.CANCEL_REQUEST, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if(SharedClass.accountType==Constants.VENDOR && listerner!=null)
                    {
                        Log.w(TAG, Constants.CANCEL_REQUEST);
                        listerner.onGetMessageAck(Constants.CANCEL_REQUEST,Constants.JSONARRAY_TYPE,args[0]);
                    }
                }
            });
            mSocket.on(Constants.FINISH_TRIP, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if(SharedClass.accountType==Constants.END_USER && listerner!=null)
                    {
                        Log.w(TAG, Constants.FINISH_TRIP);
                        listerner.onGetMessageAck(Constants.FINISH_TRIP,Constants.JSONOBJ_TYPE,args[0]);
                    }
                }
            });
            mSocket.on(Constants.TRIP_ROUTE, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if(SharedClass.accountType==Constants.END_USER && listerner!=null)
                    {
                        Log.w(TAG, Constants.TRIP_ROUTE);
                        listerner.onGetMessageAck(Constants.TRIP_ROUTE,Constants.STRING,args[0]);
                    }
                }
            });
            mSocket.on(Constants.GET_VENDOR_LOCATION, new Emitter.Listener()
            {
                @Override
                public void call(Object... args) {
                    if (args.length > 0)
                    {
                        if(SharedClass.accountType == Constants.END_USER && listerner!=null)
                        {
                            listerner.onGetMessageAck(Constants.GET_VENDOR_LOCATION, Constants.JSONOBJ_TYPE,args);
                        }
                    }
                }
            });
            mSocket.on(Constants.HIDE_REQUEST, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if(SharedClass.accountType==Constants.VENDOR && listerner!=null)
                    {
                        Log.w(TAG, Constants.HIDE_REQUEST);
                        listerner.onGetMessageAck(Constants.HIDE_REQUEST,Constants.JSONOBJ_TYPE,args[0]);
                    }
                }
            });
            mSocket.on(Constants.RECIEVE_NEW_MESSAGE, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if(listerner!=null)
                    {
                        Log.w(TAG, Constants.RECIEVE_NEW_MESSAGE);
                        listerner.onGetMessageAck(Constants.RECIEVE_NEW_MESSAGE,Constants.JSONOBJ_TYPE,args[0]);
                    }
                }
            });
            if(!mSocket.connected())
                mSocket.connect();

        }catch (Exception e) {e.printStackTrace();}
    }

    public void socketDisconnect() {
        if(mSocket!=null)
            mSocket.disconnect();
    }

    public void SendVendorPosition(String vendorId,String latitude, String longitude,final SocketMessageListerner listerner) {
        if (mSocket == null)
            return;
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put("vendor_id",vendorId);
            jsonObject.put("latitude",latitude);
            jsonObject.put("longitude",longitude);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        mSocket.emit(Constants.SEND_VENDOR_LOCATION, jsonObject.toString(), new Ack() {
            @Override
            public void call(Object... data)
            {
            }
        });
    }

    public void SendRequestToVendor(String requestId,String serviceId,String serviceNote, String userId,String lat,String lng,String address,String requestTime,String range) {
        if (mSocket == null)
            return;
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put("request_id",requestId);
            jsonObject.put("service_id",serviceId);
            jsonObject.put("service_note",serviceNote);
            jsonObject.put("user_id",userId);
            jsonObject.put("lat",lat);
            jsonObject.put("lng",lng);
            jsonObject.put("address",address);
            jsonObject.put("request_time",requestTime);
            jsonObject.put("range",range);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        mSocket.emit(Constants.SEND_REQUEST_TO_VENDOR, jsonObject.toString(), new Ack() {
            @Override
            public void call(Object... data)
            {
            }
        });
    }

    public void SendChatMessage(String senderId,String recieverId,String authKey, String message,String date,String tripId) {
        if (mSocket == null)
            return;
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put("to_id",recieverId);
            jsonObject.put("from_id",senderId);
            jsonObject.put("auth_key",authKey);
            jsonObject.put("type",SharedClass.accountType+"");
            jsonObject.put("message",message);
            jsonObject.put("date",date);
            jsonObject.put("trip_id",tripId);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        mSocket.emit(Constants.SEND_NEW_MESSAGE, jsonObject.toString(), new Ack() {
            @Override
            public void call(Object... data)
            {
            }
        });
    }

    public void KeepConnectionAlive()
    {
        if (mSocket == null)
            return;
        JSONObject jsonObject = new JSONObject();
        mSocket.emit(Constants.CONNECTION_ALIVE, jsonObject.toString(), new Ack() {
            @Override
            public void call(Object... data)
            {
                Log.w(TAG, Constants.CONNECTION_ALIVE);
            }
        });
    }

    public boolean isConnected()
    {
        return mSocket.connected();
    }

}
